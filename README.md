# DECam Pipeline Platform on GCP

:warning: This project is a **work in progress** so it is subject to change at any time,
and may have bugs in main.

The DECam Pipeline Platform allows astronomers to perform image reduction and
subtraction on
[DECam](https://noirlab.edu/science/programs/ctio/instruments/Dark-Energy-Camera)
images in a semi-automated way, generating astronomical alerts when there are changes
in the image objects. The platform takes DECam images through a pipeline implemented
with the [LSST Science Pipelines](https://pipelines.lsst.io/) package, that transforms
raw data into detected objects, which are packaged into alerts and then distributed with
a message streaming service.

This repository contains the application and infrastructure code that supports the DECam
Pipeline Platform. Its main interface is a REST API that allows users to trigger
pipeline executions with the desired input. In terms of infrastructure, the platform is
hosted on Google Cloud Platform using a set of Cloud services for computing and storage.

## Context

While the end goal for the DECam Pipeline Platform is to provide real-time image
subtraction on DECam data, first it will focus on pipeline executions on demand,
which means it is not in real-time, but rather based on input given by users. This is
known as the experimental tool, whose main objective is to enable large-scale tests and
it’s meant to be operated by trained experts only (NSF NOIRLab staff).

## Application

The main application of the DECam Pipeline Platform is a wrapper around the LSST Science
Pipelines package, that orchestrates executions of the DECam pipeline (isolating one
particular execution from others). This wrapper is currently accessed through a REST API
by which users can trigger pipeline executions and later analyze results. In the future
there might be a graphical user interface in place for visualizing pipeline executions.

There are other applications that support the main platform. All the applications are
containerized and can be found inside the `/containers` folder.

The main application is defined in the `/containers/pipeline` folder.
See the [README](containers/pipeline/README.md) there for details of its internal
structure, how to access and use the REST API and how the DECam pipeline works.

A secondary application, called `image_processing`, was used as a preliminar proof of
concept and it's not actively supported. It's kept for historical purposes inside the
`/containers/tutorial` folder.
See the [README](containers/tutorial/README.md) there for details.

Any application that supports the execution of the `pipeline` application will be added
inside ther `/containers` folder as well.

### CI/CD configuration

The containers are configured to be built and tested for each Merge Request in case
there are changes inside their respective folder, and also when a tag or commit on
`main` is pushed.

Additionally, the containers can be directly deployed to Google Cloud through a manual
CI/CD job for each case.
This job is available for Merge Requests, tags and commits on `main`.

The following CI variables need to be defined:
```
LSST_VERSION - The version of the LSST Science Pipelines used, in the vX_Y_Z format
```

In order to deploy, the infrastructure must exist and be configured as described in its
[dedicated documentation](#infrastructure).

## Infrastructure

The DECam Pipeline Platform is hosted on Google Cloud Platform. The infrastructure is
provisioned with terraform and integrated with the Gitlab CI/CD pipelines, so any
changes are managed through Gitlab.

More details can be found in the [README](terraform/README.md) inside the `terraform`
folder.

## Releases

To release a new version of the DECam Pipeline Platform (DPP) you need to follow the
steps below:

- Determine the new version number of DPP (`vX.Y.Z`)
- Update `CHANGELOG.md` with the new version number, keeping an empty `Unreleased`
  section at the top of the file, as well as adding the new version number to the
  comparison links at the bottom of the file
- Update `containers/pipeline/pyproject.toml` with the new version number. This can be
  done manually or running `poetry version <rule>` on an environment where the pipeline
  container is running. Check [poetry docs](https://python-poetry.org/docs/cli/#version)
  for more details on the command
- Submit a Merge Request with these changes, with the title `bump DPP version to vX.Y.Z`
  (using the new version number)
- Merge the changes
- Tag the new commit on `main` with the new version number `vX.Y.Z`. This will trigger a
  pipeline that will create new container images with the tag name on it
