provider "google" {
  project = var.google_project_id
  region  = var.region
}

provider "google-beta" {
  project = var.google_project_id
  region  = var.region
}

module "oidc" {
  source            = "../modules/oidc"
  google_project_id = var.google_project_id
  gitlab_project_id = var.gitlab_project_id
}

resource "random_id" "bucket_suffix" {
  byte_length = 8
}

resource "google_storage_bucket" "inputs" {
  name     = "input-bucket-${random_id.bucket_suffix.hex}"
  location = var.region
}

resource "google_storage_bucket_iam_binding" "create_inputs" {
  bucket = google_storage_bucket.inputs.name
  role   = "roles/storage.objectUser"
  members = [
    "domain:noirlab.edu",
  ]
}

resource "google_storage_bucket" "outputs" {
  name     = "output-bucket-${random_id.bucket_suffix.hex}"
  location = var.region
}

resource "google_storage_bucket_iam_binding" "readonly_outputs" {
  bucket = google_storage_bucket.outputs.name
  role   = "roles/storage.objectViewer"
  members = [
    "domain:noirlab.edu",
  ]
}

module "image_processing" {
  source                 = "../modules/image_processing"
  google_project_id      = var.google_project_id
  region                 = var.region
  artifact_registry_name = google_artifact_registry_repository.artifact-registry.name
  docker_image_name      = "container-dev" # choose where this should be defined
  cloud_run_service_name = var.image_processing_service_name
  input_bucket_name      = google_storage_bucket.inputs.name
  output_bucket_name     = google_storage_bucket.outputs.name
}

module "pipeline" {
  source                 = "../modules/pipeline"
  google_project_id      = var.google_project_id
  region                 = var.region
  artifact_registry_name = google_artifact_registry_repository.artifact-registry.name
  docker_image_name      = "pipeline-dev" # choose where this should be defined
  cloud_run_service_name = var.pipeline_service_name
  input_bucket_name      = google_storage_bucket.inputs.name
}

resource "google_artifact_registry_repository" "artifact-registry" {
  location      = var.region
  repository_id = "decam-rubin-pipeline-dev"
  description   = "repo for pipeline containers"
  format        = "DOCKER"
}
