variable "region" {
  default = "us-west1"
}

variable "google_project_id" {
  default = "insert_google_project_id"
}

variable "gitlab_project_id" {
  default = 9999
}

variable "image_processing_service_name" {
  default = "image-processing-service"
}

variable "pipeline_service_name" {
  default = "pipeline-service"
}
