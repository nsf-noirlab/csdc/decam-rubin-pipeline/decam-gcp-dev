output "oidc_provider" {
  description = "provider"
  value       = module.oidc.oidc_provider
}

output "service_account_email" {
  description = "service account email"
  value       = module.oidc.service_account_email
}

output "input_bucket_name" {
  value = google_storage_bucket.inputs.name
}

output "output_bucket_name" {
  value = google_storage_bucket.outputs.name
}

output "tutorial_cloud_run_service_name" {
  value = var.image_processing_service_name
}

output "artifact_registry_repository" {
  value = google_artifact_registry_repository.artifact-registry.name
}
