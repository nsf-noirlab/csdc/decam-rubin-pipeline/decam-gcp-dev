terraform {
  backend "http" {
  }
  required_providers {
    google = {
      version = "~> 6.10.0"
      source  = "hashicorp/google"
    }

    google-beta = {
      version = "~> 6.10.0"
      source  = "hashicorp/google-beta"
    }
  }
  required_version = "~> 1.8.0"
}
