# DECam Pipeline Platform Infrastructure

The infrastructure of the platform is provisioned with terraform and it's highy
integrated with the Gitlab CI/CD pipelines, from where the `plan` and `apply` terraform
actions are executed for any updates.

The [OpenID Connect protocol (OIDC)](https://about.gitlab.com/blog/2023/06/28/introduction-of-oidc-modules-for-integration-between-google-cloud-and-gitlab-ci/),
based on OAuth 2.0, is used to authenticate Gitlab with Google Cloud Plataform (GCP).

This basically requires the existence of a Google Service Account, which in a few words
has the permissions to change the infrastructure and it's tied to this specific Gitlab
project. So every infrastructure change is always triggered from Gitlab.

## Local configuration

The infrastructure code can be found in `/terraform`. For each terraform environment,
we need to configure a few things. Currently we're using only one environment (`dev`)
but that should change in the future. So using this environment as an example:

1. Change to the environment folder

    ```
    cd terraform/dev
    ```
2. Create `variables_override.tf` and populate the following variables with proper
   values:
    ```
    variable "google_project_id" {
      default = "INSERT GOOGLE PROJECT ID"
    }

    variable "gitlab_project_id" {
      default = INSERT GITLAB PROJECT ID
    }
    ```
    (This file is ignored by Git)

3. Setup a temporary token for auth. The easiest way to set your token locally is:
   ```
   export GOOGLE_OAUTH_ACCESS_TOKEN=$(gcloud auth print-access-token)
   ```
   (Provided you have `gcloud` configured on your local system and are authenticated)

4. On the project page, go to `Operate > Terraform states`, click on the three dots at
   the right of the environment, and click `Copy Terraform init command`. Follow the
   instructions on the popup that's displayed.

5. You should now be able to execute `terraform validate` and `terraform plan`

**Note**: Never ever execute `apply` locally. That's only done through Gitlab CI/CD
pipelines

### CI/CD configuration

If you're setting a new project with Gitlab CI/CD pipelines authenticated to GCP,
a service account and OIDC need to be configured. So `google_service_account.sa`,
`google_project_iam_member.sa_editor`, and `module.gl_oidc` need to
be applied locally from an account with sufficient permissions.

There are 4 CI variables that need defining as well:
```
SERVICE_ACCOUNT - a terraform output
WI_POOL_PROVIDER - a terraform output
TF_VAR_gitlab_project_id - the gitlab project id (integer)
TF_VAR_google_project_id - the google project id
```

Once the above is configured, the Gitlab CI/CD pipeline for `plan` and `apply`
should work.
