variable "google_project_id" {
  type = string
}

variable "region" {
  type = string
}

variable "artifact_registry_name" {
  type = string
}

variable "docker_image_name" {
  type = string
}

variable "docker_image_tag" {
  type    = string
  default = "latest"
}

variable "cloud_run_service_name" {
  type = string
}

variable "input_bucket_name" {
  type = string
}
