# Enable Cloud Run API
resource "google_project_service" "cloudrun_api" {
  service            = "run.googleapis.com"
  disable_on_destroy = false
}

# Enable Compute Engine API (needed for creating Cloud SQL instances)
resource "google_project_service" "compute_api" {
  service            = "compute.googleapis.com"
  disable_on_destroy = false
}

# Enable SQL Admin API
resource "google_project_service" "sqladmin_api" {
  service            = "sqladmin.googleapis.com"
  disable_on_destroy = false
}

# Enable Cloud Filestore API
resource "google_project_service" "filestore_api" {
  service            = "file.googleapis.com"
  disable_on_destroy = false
}

resource "google_sql_database_instance" "default" {
  name             = "postgres-pipeline"
  region           = var.region
  database_version = "POSTGRES_15"
  root_password    = "op]ho5gyYy%+5[O["
  settings {
    tier = "db-custom-4-15360"
    password_validation_policy {
      min_length                  = 6
      complexity                  = "COMPLEXITY_DEFAULT"
      reuse_interval              = 2
      disallow_username_substring = true
      enable_password_policy      = true
    }
  }
  # set `deletion_protection` to true, will ensure that one cannot accidentally delete this instance by
  # use of Terraform whereas `deletion_protection_enabled` flag protects this instance at the GCP level.
  deletion_protection = false
  depends_on          = [google_project_service.compute_api, google_project_service.sqladmin_api]
}

resource "google_sql_database" "database" {
  name     = "pipeline_db"
  instance = google_sql_database_instance.default.name
}

resource "random_password" "pwd" {
  length = 16
}

resource "google_sql_user" "user" {
  name     = "pipeline_user"
  instance = google_sql_database_instance.default.name
  password = random_password.pwd.result
}

resource "random_id" "bucket_suffix" {
  byte_length = 8
}

resource "google_storage_bucket" "pipeline_executions" {
  name     = "pipeline-executions-${random_id.bucket_suffix.hex}"
  location = var.region
}

resource "google_storage_bucket_iam_binding" "readonly_pipeline_executions" {
  bucket = google_storage_bucket.pipeline_executions.name
  role   = "roles/storage.objectViewer"
  members = [
    "domain:noirlab.edu",
  ]
}

resource "google_cloud_run_v2_service" "default" {
  name     = var.cloud_run_service_name
  location = var.region

  client         = "gcloud"
  client_version = "489.0.0"

  template {
    containers {
      image = "${var.region}-docker.pkg.dev/${var.google_project_id}/${var.artifact_registry_name}/${var.docker_image_name}:${var.docker_image_tag}"

      env {
        name  = "INPUT_BUCKET_NAME"
        value = var.input_bucket_name
      }

      env {
        name  = "DB_USER"
        value = google_sql_user.user.name
      }

      env {
        name  = "DB_PASSWORD"
        value = google_sql_user.user.password
      }

      env {
        name  = "DB_NAME"
        value = google_sql_database.database.name
      }

      env {
        name  = "INSTANCE_UNIX_SOCKET"
        value = "/cloudsql/${google_sql_database_instance.default.connection_name}"
      }

      env {
        name  = "EXEC_BUCKET_NAME"
        value = google_storage_bucket.pipeline_executions.name
      }

      resources {
        cpu_idle = false
        limits = {
          cpu    = "8000m"
          memory = "32Gi"
        }
      }

      volume_mounts {
        name       = "pipeline-executions-nfs"
        mount_path = "/home/lsst/pipeline-executions"
      }

      volume_mounts {
        name       = "cloudsql"
        mount_path = "/cloudsql"
      }
    }

    # Set 15 minutes for initial executions (should decrease in production)
    timeout = "900s"

    scaling {
      max_instance_count = 100
      min_instance_count = 1
    }

    vpc_access {
      network_interfaces {
        network    = "default"
        subnetwork = "default"
      }
    }

    volumes {
      name = "pipeline-executions-nfs"
      nfs {
        server    = google_filestore_instance.default.networks[0].ip_addresses[0]
        path      = "/share1"
        read_only = false
      }
    }

    volumes {
      name = "cloudsql"
      cloud_sql_instance {
        instances = [google_sql_database_instance.default.connection_name]
      }
    }

  }

  # Temporarily ignore env variables until we use secret manager
  # This is a workaround to include other env variables not set through terraform
  # The drawback is that any addition or change to the existing env variables
  # through terraform will not be considered
  # TODO: uncomment after applying EXEC_BUCKET_NAME env variable and deploying container
  # lifecycle {
  #   ignore_changes = [
  #     template.0.containers.0.env,
  #   ]
  # }

  depends_on = [google_project_service.cloudrun_api, google_project_service.sqladmin_api]
}

resource "google_cloud_run_v2_service_iam_member" "noauth" {
  location = google_cloud_run_v2_service.default.location
  name     = google_cloud_run_v2_service.default.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}

resource "google_filestore_instance" "default" {
  name     = "executions-nfs"
  location = "${var.region}-a"
  tier     = "BASIC_HDD"

  file_shares {
    capacity_gb = 1024
    name        = "share1"
  }

  networks {
    network = "default"
    modes   = ["MODE_IPV4"]
  }
}
