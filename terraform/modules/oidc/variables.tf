variable "google_project_id" {
  type = string
}

variable "gitlab_project_id" {
  type = number
}

variable "oidc_service_account_id" {
  type    = string
  default = "oidc-service-account"
}

variable "role" {
  type    = string
  default = "roles/editor"
}




