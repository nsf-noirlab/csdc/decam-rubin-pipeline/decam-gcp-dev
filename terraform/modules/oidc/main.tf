# First go to https://console.cloud.google.com/iam-admin/workload-identity-pools and enable the apis.

resource "google_service_account" "sa" {
  account_id = var.oidc_service_account_id
}

resource "google_project_iam_member" "sa_editor" {
  project = var.google_project_id
  role    = var.role
  member  = "serviceAccount:${google_service_account.sa.email}"
}

# https://about.gitlab.com/blog/2023/06/28/introduction-of-oidc-modules-for-integration-between-google-cloud-and-gitlab-ci/
module "gl_oidc" {
  source            = "gitlab.com/gitlab-com/gcp-oidc/google"
  version           = "3.2.0"
  google_project_id = var.google_project_id
  gitlab_project_id = var.gitlab_project_id
  oidc_service_account = {
    "sa" = {
      sa_email  = google_service_account.sa.email
      attribute = "attribute.project_id/${var.gitlab_project_id}"
    }
  }
}
