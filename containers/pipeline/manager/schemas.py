from datetime import datetime
from pydantic import BaseModel


class Pipeline(BaseModel):
    name: str
    created_at: datetime
    current_stage: str
    current_status: str
