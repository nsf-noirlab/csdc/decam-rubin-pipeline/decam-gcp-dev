import uuid
import os
from datetime import datetime
from typing import Annotated
from fastapi import Body, FastAPI, HTTPException

from manager.adapters.archive import AstroDataArchiveConnector
from manager.adapters.database import DatabaseConnector
from manager.adapters.kafka import KafkaConnector
from manager.adapters.gcs import GcsConnector
from manager.adapters.scipipe import LsstSciPipeConnector
from manager.schemas import Pipeline as PipelineSchema
from stages.base import BackgroundExecutor
from stages.pipeline import Pipeline

app = FastAPI(
    title="NSF NOIRLab's DECam Pipeline Platform",
    summary=f"API using LSST Science Pipelines {os.environ.get("LSST_VERSION")}",
    contact={"name": "NOIRLab's CSDC", "email": "sebastian.vicencio@noirlab.edu"},
    license_info={
        "name": "BSD-3-Clause",
        "url": "https://gitlab.com/nsf-noirlab/csdc/decam-rubin-pipeline/decam-gcp-dev/"
        "-/blob/main/LICENSE?ref_type=heads",
    },
)


@app.get("/")
def check_connections():
    return check_adapters()


@app.post("/pipelines", status_code=201)
async def create_pipeline() -> PipelineSchema:
    # TODO: create a model instance that autogenerates name (when models exist)
    created_at = datetime.now()
    name = f"pipeline-{created_at.strftime('%Y%m%d-%H%M%S')}-{uuid.uuid4()}"
    try:
        pipeline = Pipeline(name)
        stage = pipeline.next_stage
        stage.configure()
        BackgroundExecutor(stage).trigger()
    except Exception as err:
        raise HTTPException(status_code=500, detail=str(err))

    return PipelineSchema(
        name=name,
        created_at=created_at,
        current_stage=pipeline.current_stage.name,
        current_status=pipeline.current_stage_status,
    )


@app.get("/pipelines/{pipeline_name}", status_code=200)
async def get_pipeline(pipeline_name: str):
    pipeline = Pipeline(pipeline_name)
    if pipeline.is_new():
        raise HTTPException(
            status_code=404, detail=f"Pipeline with name '{pipeline_name}' not found"
        )

    # TODO: replace below code with actual pipeline data

    # Temporarily writes a sample file to the pipeline folder to test
    # writing capability to a mounted NFS server
    with open(f"{pipeline.path}/gcs_sync.txt", "w") as f:
        f.write(
            f"{datetime.now().strftime('%Y%m%d-%H%M%S')} "
            f"This should be automatically synced to an NFS server\n"
        )

    # Temporarily returns contents of the pipeline-executions folder
    # so we can test read/write to a mounted GCS bucket
    return {"executions": os.listdir(pipeline._home_dir)}


@app.put("/pipelines/{pipeline_name}", status_code=200)
async def continue_pipeline(
    pipeline_name: str,
    stage: Annotated[str | None, Body(embed=True)] = None,
) -> PipelineSchema:
    pipeline = Pipeline(pipeline_name)
    if pipeline.is_new():
        raise HTTPException(
            status_code=404,
            detail=f"Pipeline with name '{pipeline_name}' not found. Cannot continue",
        )
    try:
        if stage:
            stage_instance = pipeline.init_stage_to_attempt(stage)
        else:
            stage_instance = pipeline.next_stage
        stage_instance.configure()
        BackgroundExecutor(stage_instance).trigger()
    except Exception as err:
        raise HTTPException(status_code=500, detail=str(err))

    return PipelineSchema(
        name=pipeline.name,
        # Could calculate from name but not applicable to all pipeline names
        created_at=datetime.now(),
        current_stage=pipeline.current_stage.name,
        current_status=pipeline.current_stage_status,
    )


def check_adapters():
    adapters = []
    scipipe_connector = LsstSciPipeConnector()
    scipipe_connection_result = scipipe_connector.check_connection()
    adapters.append(
        {
            "name": "LSST Science Pipelines",
            "result": scipipe_connection_result,
        }
    )

    gcs_connector = GcsConnector(os.getenv("INPUT_BUCKET_NAME"))
    gcs_connection_result = gcs_connector.check_connection()
    adapters.append(
        {
            "name": "Google Cloud Storage",
            "result": gcs_connection_result,
        }
    )

    database_connector = DatabaseConnector()
    database_connection_result = database_connector.check_connection()
    adapters.append(
        {
            "name": "Cloud SQL (or local database)",
            "result": database_connection_result,
        }
    )

    kafka_connector = KafkaConnector()
    kafka_connection_result = kafka_connector.check_connection()
    adapters.append(
        {
            "name": "Kafka",
            "result": kafka_connection_result,
        }
    )

    archive_connector = AstroDataArchiveConnector("M83")
    archive_connection_result = archive_connector.check_connection()
    adapters.append(
        {
            "name": "Astro Data Archive",
            "result": archive_connection_result,
        }
    )

    return {"adapters": adapters}
