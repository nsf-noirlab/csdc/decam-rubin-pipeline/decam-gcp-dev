import os
import sqlalchemy
from sqlalchemy_utils import database_exists, create_database

from manager.adapters.base import AbstractConnector


class DatabaseConnector(AbstractConnector):
    def __init__(self, db_name: str = None) -> None:
        self._db_name = db_name or os.environ["DB_NAME"]
        if os.environ.get("INSTANCE_UNIX_SOCKET"):
            self._engine = self.connect_unix_socket()
            return

        self._engine = self.connect_standard()

    @property
    def db_name(self):
        return self._db_name

    def get_engine(self) -> sqlalchemy.engine.base.Engine:
        return self._engine

    def check_connection(self) -> str:
        try:
            self.init_db()
            with self._engine.connect() as conn:
                metadata_obj = sqlalchemy.MetaData()
                metadata_obj.reflect(self._engine)
                result = conn.execute(sqlalchemy.text("SELECT 'hello world'"))
                (result_str,) = result.all()[0]
                return (
                    f"Connection to database '{self._db_name}' successful\n"
                    f"Basic select: {result_str}\n"
                    f"Tables: [{", ".join(metadata_obj.tables.keys())}]"
                )
        except (sqlalchemy.exc.InterfaceError, sqlalchemy.exc.ProgrammingError) as err:
            return f"Connection to database '{self._db_name}' failed\n" f"Reason: {err}"

    def init_db(self, extensions_required: bool = False):
        if not database_exists(self._engine.url):
            create_database(self._engine.url)
        if extensions_required:
            with self._engine.connect() as conn:
                conn.execute(
                    sqlalchemy.text('CREATE EXTENSION IF NOT EXISTS "btree_gist"'),
                )
                conn.commit()

    def connect_standard(self) -> sqlalchemy.engine.base.Engine:
        db_user = os.environ["DB_USER"]
        db_pass = os.environ["DB_PASSWORD"]
        pool = sqlalchemy.create_engine(
            f"postgresql+psycopg2://{db_user}:{db_pass}@db/{self._db_name}"
        )
        # For pg8000 use: f"postgresql+pg8000://{db_user}:{db_pass}@db/{self._db_name}"
        return pool

    def connect_unix_socket(self) -> sqlalchemy.engine.base.Engine:
        """Initializes a Unix socket connection pool for a Cloud SQL instance of Postgres."""
        db_user = os.environ["DB_USER"]
        db_pass = os.environ["DB_PASSWORD"]
        unix_socket_path = os.environ[
            "INSTANCE_UNIX_SOCKET"
        ]  # e.g. '/cloudsql/project:region:instance'

        pool = sqlalchemy.create_engine(
            # Equivalent URL:
            # postgresql+pg8000://<db_user>:<db_pass>@/<self._db_name>
            #                         ?unix_sock=<INSTANCE_UNIX_SOCKET>/.s.PGSQL.5432
            # Note: Some drivers require the `unix_sock` query parameter to use a different key.
            # For example, 'psycopg2' uses the path set to `host` in order to connect successfully.
            sqlalchemy.engine.url.URL.create(
                drivername="postgresql+psycopg2",
                username=db_user,
                password=db_pass,
                database=self._db_name,
                query={"host": f"{unix_socket_path}"},
            ),
        )
        # For pg8000 use: drivername="postgresql+pg8000"
        # and query={"unix_sock": f"{unix_socket_path}/.s.PGSQL.5432"}
        return pool
