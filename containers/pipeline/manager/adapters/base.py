import abc


class AbstractConnector(abc.ABC):
    @abc.abstractmethod
    def check_connection(self) -> str:
        raise NotImplementedError
