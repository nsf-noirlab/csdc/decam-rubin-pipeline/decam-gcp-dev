import os
from typing import List

from confluent_kafka import KafkaException, Producer

from manager.adapters.base import AbstractConnector


class KafkaConnector(AbstractConnector):
    def __init__(self):
        config = {
            "bootstrap.servers": "x",
            "security.protocol": "SASL_SSL",
            "sasl.mechanisms": "PLAIN",
            "sasl.username": "x",
            "sasl.password": "x",
        }
        config["bootstrap.servers"] = os.environ.get("KAFKA_SERVER")
        config["sasl.username"] = os.environ.get("KAFKA_USERNAME")
        config["sasl.password"] = os.environ.get("KAFKA_PASSWORD")
        self._producer = Producer(config)

    def check_connection(self) -> str:
        try:
            topics = self.list_topics()
            return (
                f"Connection to Kafka cluster successful\n"
                f"Topics: {', '.join(topics) if len(topics) else '(empty)'}"
            )
        except KafkaException as err:
            return f"Connection to Kafka cluster failed\n" f"Reason: {err}"

    def list_topics(self) -> List[str]:
        topics = self._producer.list_topics(timeout=3).topics
        print(topics)
        return list(topics.keys())
