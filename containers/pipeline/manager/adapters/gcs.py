from typing import List, Tuple
from google.cloud import storage
from google.cloud.storage import transfer_manager
from google.auth.exceptions import DefaultCredentialsError

from manager.adapters.base import AbstractConnector


class GcsConnector(AbstractConnector):
    def __init__(self, bucket_name: str):
        self._client_err = None
        try:
            self._storage_client = storage.Client()
        except DefaultCredentialsError as err:
            self._client_err = f"Error connecting to GCS: {err}"
        self._bucket_name = bucket_name

    @property
    def bucket_name(self):
        return self._bucket_name

    @property
    def current_error(self):
        return self._client_err

    def check_connection(self) -> str:
        def error_message(err: str):
            return f"Connection to bucket {self._bucket_name} failed\n" f"Reason: {err}"

        if self._client_err:
            return error_message(self._client_err)

        try:
            blobs = self.list_bucket_files()
            return (
                f"Connection to bucket {self._bucket_name} successful\n"
                f"Bucket files: {', '.join(blobs) if len(blobs) else '(empty)'}"
            )
        except Exception as err:
            return error_message(err)

    def list_bucket_files(self, prefix=None, delimiter=None) -> List[str]:
        blobs = self._storage_client.list_blobs(
            self._bucket_name,
            prefix=prefix,
            delimiter=delimiter,
        )
        return list([blob.name for blob in blobs])

    def download_many_to_path(
        self,
        blob_names: List[str],
        dest_dir: str,
    ) -> List[Tuple[str, None | Exception]]:
        bucket = self._storage_client.bucket(self._bucket_name)
        results = transfer_manager.download_many_to_path(
            bucket,
            blob_names,
            destination_directory=dest_dir,
            max_workers=4,  # This could be configurable
        )
        return list(zip(blob_names, results))

    def upload_many(
        self,
        filenames: List[str],
        source_dir: str,
        blob_name_prefix: str = "",
    ) -> List[Tuple[str, None | Exception]]:
        bucket = self._storage_client.bucket(self._bucket_name)
        results = transfer_manager.upload_many_from_filenames(
            bucket,
            filenames,
            blob_name_prefix=blob_name_prefix,
            source_directory=source_dir,
            max_workers=4,  # This could be configurable
        )
        return list(zip(filenames, results))
