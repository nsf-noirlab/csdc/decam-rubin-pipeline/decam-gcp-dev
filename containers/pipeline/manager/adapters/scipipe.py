import os
import subprocess

from manager.adapters.base import AbstractConnector


class LsstSciPipeConnector(AbstractConnector):
    def __init__(self):
        self._config_cmd = (
            "source /opt/lsst/software/stack/loadLSST.bash && setup lsst_distrib"
        )
        self._version = os.environ.get("LSST_VERSION")

    @property
    def version(self):
        return self._version

    def build_cmd(self, cmd: str):
        return f"{self._config_cmd} && {cmd}"

    def check_connection(self) -> str:
        cmd = "pipetask run -h"
        try:
            process = self.run_cmd(cmd)
            return (
                f"Connection to LSST Science Pipelines package successful\n"
                f"Output: \n{process.stdout.decode()}"
            )
        except Exception as err:
            return (
                f"Connection to LSST Science Pipelines package failed\n"
                f"Reason: {err}"
            )

    def run_cmd(self, cmd: str) -> subprocess.CompletedProcess:
        return subprocess.run(
            [self.build_cmd(cmd)],
            shell=True,
            capture_output=True,
        )
