import requests

from manager.adapters.base import AbstractConnector

ASTRO_DATA_ARCHIVE_BASE_URL = "https://astroarchive.noirlab.edu/api/"


class AstroDataArchiveConnector(AbstractConnector):
    def __init__(self, object_name: str) -> None:
        self._object_name = object_name

    def check_connection(self) -> str:
        try:
            coordinates = self.get_object_lookup(self._object_name)
            return (
                f"Connection to Astro Data Archive successful\n"
                f"Coordinates: {coordinates}"
            )
        except requests.exceptions.Timeout as err:
            return (
                f"Connection to Astro Data Archive could not be established\n"
                f"Reason: {err}"
            )
        except RuntimeError as err:
            return f"Connection to Astro Data Archive with errors\n" f"Reason: {err}"

    def get_object_lookup(self, object_name: str):
        url = f"{ASTRO_DATA_ARCHIVE_BASE_URL}object-lookup/?object_name={object_name}"
        response = requests.get(url, timeout=9)
        if response.status_code != 200:
            json = response.json()
            raise RuntimeError(json["errorMessage"])

        return response.json()
