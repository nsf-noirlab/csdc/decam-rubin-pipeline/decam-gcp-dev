# DECam Pipeline Platform Application

This the main application that orchestrates executions of the DECam pipeline, aimed to
perform DECam image reduction and subtraction in order to detect changing objects.

The application exposes a service that acts as an intermediary between the user and the
[LSST Science Pipelines](https://pipelines.lsst.io/). The LSST package has many different
components for a broad scientify usage. As part of the DPP application, only a subset of
the functionality of the LSST Science Pipelines is used, which is the minimum required
to perform image reduction and subtraction.

The application includes 2 main packages:
- `stages`: takes care of executing the DECam pipeline by handling "pipeline executions"
, which are isolated sets of files and databases grouped together under an identifier
(ID). Each pipeline execution is different and based on given input datasets
- `manager`: REST API provided as a FastAPI application that allows users to trigger
DECam pipeline executions. It contains endpoints to create new pipeline executions, as
well as continuing existing pipeline executions that are not finished

To learn how to use the pipeline application, [see the related section](#how-to-use).

## How to access the application

### Staging

There is a working instance of the REST API running on Google Cloud Run. It is not open
to the public, so ask a team member for the URL if you'd like to see it in action.

### Local environment

A Docker container with the pipeline `manager` can be built and deployed where needed.

For local development, a docker compose configuration can spin up the pipeline manager
along with a local PostgreSQL database. There is a Makefile for convenience:

```bash
make
# or docker compose up --build
```

and to run tests:
```bash
make test
```

The above will use the default version of the LSST Science Pipelines package, which
right now is set to `v26_0_2`. If you want to try other versions, you can add the
following extra argument at the end of each of the above commands:

```bash
make LSST_VERSION=v23_0_1
make test LSST_VERSION=v23_0_1

# OR (need to build first when passing arguments)
docker compose build --build-arg LSST_VERSION=v26_0_2
docker compose up
```

The LSST Science Pipelines package supported versions currently are:
- `v26_0_2`
- `v23_0_1`

After having the containers running, the pipeline manager API docs can be accessed at
[localhost:8080/docs](http://localhost:8080/docs).

## Concepts

### Pipeline execution

A pipeline execution is an instance of running the DECam pipeline. A user could run the
pipeline multiple times, and each would create a new pipeline execution with its own set
of data products. This isolation between executions is required to process and analyze
different datasets. Each pipeline execution is identified by a unique ID, generated when
triggered for the first time. This ID can later be used to retrieve pipeline results
(staff only).

The user has access to see the progress of a pipeline execution at any particular time,
as well as controlling when the execution advances throughout the pipeline. However,
they cannot alter the pipeline behavior besides providing input datasets.

### Stages and steps

The DECam pipeline is composed of a series of tasks that are executed in a pre-defined
order. At the most basic level, a pipeline receives input datasets (images) and produces
data products as output, which consist of data folders, databases and alert messages
(the latter generated whenever changes are detected as a result of the image
subtraction tasks).

To facilitate the execution, the DECam pipeline is composed of different stages, each
with a specific purpose. A pipeline execution goes through each of the pipeline stages,
and actually each stage has to be triggered separately (for now). So in a sense, the
stages can be seen as checkpoints within the DECam pipeline.

At the same time, each stage is composed of several steps that perform specific tasks
and that gradually build the final results. Each stage is triggered sequentially, and
the next stage cannot be triggered if the previous one has not finished its execution.
A particular stage can be re-run though if the user requires it.

The stages are:

`Preparation` -> `Ingestion` -> `Calibration` -> `DRP` -> `AP`

To keep track of its execution, each stage has a `status` that records milestones
within the execution. The possible values are:

`NOT_STARTED` -> `CONFIGURING` -> `STARTED` -> `FINISHED`

The following diagram shows an overview of the DECam pipeline workflow:

**TODO: include diagram**

## How to use

### Trigger pipeline executions

The REST API provide 2 main endpoints:
- `POST /pipeline` for creating pipeline executions
- `PUT /pipeline/<execution_id>` for continuing existing pipeline executions

These 2 operations are enough to interact with the DECam Pipeline Platform end-to-end.
The logical order is to create the pipeline (which starts the first stage) and then call
the continuation endpoint once each stage has finished its execution. For each stage
triggered by an endpoint, there is a `configure` task executed synchronously (an HTTP
response is sent when finished) and then the actual processing is done asynchronously
using an asyncio task. The next sections provide more details about each endpoint.

#### Create a pipeline execution

A pipeline can be created by calling the `POST /pipeline` endpoint. This endpoint also
triggers the execution of the `Preparation` stage automatically, so it's the first step
to start any execution. It mainly includes initial configurations required prior to
ingesting raw image data.

The endpoint returns a generated execution ID. You should note down this ID to proceed
with the rest of the pipeline, and to later access results.

A Google Cloud Storage (GCS) bucket is used as part of the `Preparation` stage. Check
the environment requirements for this stage in the
[corresponding section](#requirements-for-running-stages).

#### Continue a pipeline execution

After having created a pipeline execution, the user needs to continue it manually stage
by stage (although there could be an automated way in the future). To do this, the
`PUT /pipeline/<execution_id>` endpoint must be called, where `<execution_id>` is the
ID assigned (and included in the response) when a pipeline execution is created.

There is an optional JSON payload that can be included whenever a previous stage needs to
be re-run. This can happen for a number of reasons, but most likely because the user
wants to provide different input for the stage. In such cases, a JSON object containing
the `stage` key with the stage code to execute should be included. The endpoint returns
some contextual information of the current stage and status of the pipeline execution.
This endpoint can be called until the pipeline execution finishes with the `AP` stage.

Some of the stages have requirements for execution, so make sure to check the
[corresponding section](#requirements-for-running-stages).

### Access pipeline execution results

The results of a pipeline execution are divided into 4 different data products:
1. An execution folder with input data, logs files and the Butler repository that’s
generated by the LSST Science Pipelines package
2. A Butler database for the execution
3. An Alert/Prompt Production database (APDB in short) which contains the results of the
difference imaging task
4. (Not yet implemented) Alert messages sent through message streams (such as Kafka)

The execution folder (1) is stored with the pipeline execution ID as name. It should
look like `pipeline-YYYYMMDD-HHMMSS-random-number`. When the user has access to the
filesystem, the folder with the pipeline ID can be found under the
`/home/lsst/pipeline-executions` folder.
Each pipeline execution folder has 3 relevant sub-folders:

- `data`: for files that are used as input for the pipeline execution
- `repo`: for the Butler repository created by the LSST Science Pipelines
- `logs`: for log files created during the pipeline execution

When deployed to Cloud Run, the `/home/lsst/pipeline-executions` folder (which contains
all the pipeline executions folders) is synced with a Filestore instance, so each
pipeline execution folder is persisted to this NFS server. Access to the Filestore
instance should be requested directly to the development team.

When using a local environment, you can directly access the container filesystem to
check results, but additionally the `logs` folder of each pipeline execution can be
copied over to the `containers/pipeline/data/executions` folder under a folder with
the name of the pipeline. To achieve this, you should leave the `EXEC_BUCKET_NAME`
env variable with the default value (if the usage of this env variable seems odd, it is
because of legacy behavior that will be changed to something more meaningful soon).

We keep a log file named `status.log` under the `logs` folder of each pipeline execution
folder, where you can see what the pipeline has executed at a certain point in time.

So inside the `status.log` file you will find entries like the following example:

```
2024-08-22 20:21:09,756 - INFO: Preparation - NOT STARTED
2024-08-22 20:21:10,439 - INFO: Preparation - CONFIGURING
2024-08-22 20:21:53,882 - INFO: Preparation - STARTED
2024-08-22 20:31:38,747 - INFO: Preparation - FINISHED
2024-08-22 20:51:01,370 - INFO: Ingestion - NOT STARTED
2024-08-22 20:51:02,290 - INFO: Ingestion - CONFIGURING
2024-08-22 20:51:13,548 - INFO: Ingestion - STARTED
2024-08-22 20:58:37,820 - INFO: Ingestion - FINISHED
2024-08-22 21:18:56,119 - INFO: Calibration - NOT STARTED
2024-08-22 21:18:57,011 - INFO: Calibration - CONFIGURING
2024-08-22 21:19:06,573 - INFO: Calibration - STARTED
```

The last line of this file basically tells us the current state of the pipeline
execution. In the above example, we know that the pipeline has started the `Calibration`
stage but has not finished it yet (so the `DRP` cannot be triggered yet).

For the databases that are created ((2) and (3)), you can directly access the database
container to query results when using a local environment, or you can request access to
the development team for the Google Cloud SQL instance that's created in the Cloud.

## Pipeline execution examples

**TODO**: provide access to an example pipeline execution so users can familiarize with
the structure without installing anything.

## Requirements for running stages

### Preparation

The `Preparation` stage includes the butler creation, the ingestion of a subset of
reference catalogs (Gaia DR2 and Pan-STARRS PS1) covering the area of the DECam
observations, and some other configurations.

A Google Cloud Storage (GCS) bucket is used for the ingestion of reference datasets as
a preliminary fixed dataset. If you're working locally, you need to setup an
authentication method for this GCS bucket in order to download the FITS files of the
reference catalogs. This can be achieved by setting the `GOOGLE_CLOUD_PROJECT` and
`GOOGLE_APPLICATION_CREDENTIALS` (the latter with a path the container can access) env
variables inside the `docker-compose.yml` file. The `INPUT_BUCKET_NAME` env variable
should also be set with the bucket name that contains the reference catalogs. Consider
that the pipeline expects FITS files to be in the `refcats/gaia_dr2_GW170817` and
`refcats/ps1_dr1_GW170817` folders inside the bucket.

Alternatively, if you don't want to download files from GCS (or you don't have access),
the filesystem can also be used. You can get the FITS files on your own and copy them
inside the `containers/pipeline/data` folder. In this case, the reference catalogs must
be organized in the `refcats/gaia_dr2_GW170817` and `refcats/ps1_dr1_GW170817` folders.
If not, an error will be thrown. You should not set the GCS env variables in this case.

**NOTE**: when using the filesystem (the default), copy the files after building the
Docker image. Otherwise, these files will be copied over as part of the image and your
Docker environment will use more disk space.

As for the deployment in Cloud Run, no special consideration should be taken besides
setting the `INPUT_BUCKET_NAME` env variable and having the FITS files organized inside
the bucket as mentioned in the above paragraph, since Cloud Run services already have
access to other Google Cloud services (like GCS in this case).

### Ingestion

The `Ingestion` stage requires the raw image datasets, which need to be provided by the
user. A preliminar solution uses a fixed dataset, so similar to the `refcats` case, a
Google Cloud Storage (GCS) bucket is used as source, but if this is not set up (the
default), the filesystem is used. The only consideration to have here is to put the raw
datasets in the correct folder, under `containers/pipeline/data/raw/<raw_type>`, where
`<raw_type>` is `science`, `bias`, `flat`. All 3 of these subfolders should be populated
with FITS files when using the filesystem instead of a GCS bucket. When using a GCS
bucket, the files should follow a similar path format: `raw/<raw_type>/file.fits.fz`.

**NOTE**: when using the filesystem (the default), copy the files after building the
Docker image. Otherwise, these files will be copied over as part of the image and your
Docker environment will use more disk space.

### AP

The last stage, the `AP` stage produces results by storing files in the butler
repository, and also by populating the AP association database. This database is
created automatically when the stage executes successfully, and it has a name following
the format: `apdb_<execution_id>`. If you have access to this database, you can perform
your queries of interest there. There are no special requirements rather than having
the database environment configured (by default in local environment and by terraform
in Google Cloud Platform).

This is current output of the pipeline execution. There is still more work to do when
a pipeline finishes its execution and this is described in the following section.

## Future work

After having the AP association database populated, the next step is to package the
results into alerts using the LSST schema in the Apacha Avro format. Some useful
resources to guide the work here:

- https://github.com/lsst/ap_association/blob/main/python/lsst/ap/association/packageAlerts.py
- https://github.com/lsst/alert_packet

The final step is to distribute these alerts through Kafka, and we currently have a
Confluent Kafka cluster configured for this.
