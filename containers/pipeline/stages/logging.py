import logging

STAGES_NAMES = [
    "Preparation",
    "Ingestion",
    "Calibration",
    "DRP",
    "AP",
]


def filter_console_handler(record: logging.LogRecord):
    if record.levelno >= logging.WARNING:
        return True

    if any(name in record.getMessage() for name in STAGES_NAMES):
        return True

    return False


console_handler = logging.StreamHandler()
console_handler.addFilter(filter_console_handler)

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(name)-12s - %(levelname)-8s: %(message)s",
    handlers=[console_handler],
)


def get_logger(name: str, filename: str) -> logging.Logger:
    """
    Returns a Logger object with the specified `name` that has an attached `FileHandler`
    instance to send logging output to the provided `filename`. Supports only a single
    handler
    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter("%(asctime)s - %(levelname)s: %(message)s")
    for existing_handler in logger.handlers:
        logger.removeHandler(existing_handler)
    file_handler = logging.FileHandler(filename)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    return logger
