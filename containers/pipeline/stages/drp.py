import json
from typing import List

from stages.base import AbstractPipeline, BaseStage, Status, log_step


class DRPStage(BaseStage):
    NAME = "DRP"
    DRP_PIPE_DIR = "config/pipelines/drp_pipe"
    ANALYSIS_DRP_DIR = "config/pipelines/analysis_drp"
    OUTPUT = "DECam/runs/20250217"
    VALID_SUBSTEPS = [
        "step1",
        "step2a",
        "step2b",
        "step2c",
        "step2d",
        "step2e",
        "step3",
    ]

    def __init__(self, pipeline: AbstractPipeline):
        super().__init__(pipeline, log_name="4_00_drp")

    def configure(self):
        super().configure()
        self.validate_butler_config()

    def start(self):
        self.pipeline.commit_status(Status.STARTED.name)
        self.run_single_frame_processing()
        self.run_initial_visit_aggregation()
        self.run_tract_level_characterization()
        self.run_global_collection_summary()
        self.run_final_source_table_generation()
        self.run_final_visit_summary()
        self.run_coadd_processing()
        self.pipeline.commit_status(Status.FINISHED.name)

    @log_step("Single Frame Processing")
    def run_single_frame_processing(self):
        step_config = self.config["run_single_frame_processing"]
        self.build_drp_substep_graph("step1")
        self.run_drp_substep(
            "step1",
            config_override_files=step_config["config_override_files"],
            custom_data_query=step_config["data_query"],
        )

    @log_step("Initial visit aggregation")
    def run_initial_visit_aggregation(self):
        step_config = self.config["run_initial_visit_aggregation"]
        self.build_drp_substep_graph("step2a")
        self.run_drp_substep(
            "step2a",
            custom_data_query=step_config["data_query"],
        )

    @log_step("Tract-level characterization")
    def run_tract_level_characterization(self):
        step_config = self.config["run_tract_level_characterization"]
        self.build_drp_substep_graph(
            "step2b",
            custom_pipeline=step_config["custom_pipeline"],
        )
        self.run_drp_substep(
            "step2b",
            custom_pipeline=step_config["custom_pipeline"],
            config_override_files=step_config["config_override_files"],
            custom_data_query="skymap='decam_rings_v1'",
        )

    @log_step("Global collection summary")
    def run_global_collection_summary(self):
        step_config = self.config["run_global_collection_summary"]
        if not step_config["enabled"]:
            return

        self.build_drp_substep_graph("step2c")
        self.run_drp_substep("step2c")

    @log_step("Final source table generation")
    def run_final_source_table_generation(self):
        # This step is not expected to work but nevertheless we include it here
        # so we have a reference of how to execute it
        step_config = self.config["run_final_source_table_generation"]
        self.build_drp_substep_graph(
            "step2d",
            custom_pipeline=step_config["custom_pipeline"],
        )
        self.run_drp_substep(
            "step2d",
            custom_pipeline=step_config["custom_pipeline"],
            config_overrides=step_config["config_overrides"],
            custom_data_query=step_config["data_query"],
        )

    @log_step("Final visit summary")
    def run_final_visit_summary(self):
        step_config = self.config["run_final_visit_summary"]
        if not step_config["enabled"]:
            return

        self.build_drp_substep_graph("step2e")
        self.run_drp_substep(
            "step2e",
            custom_data_query=step_config["data_query"],
        )

    @log_step("Coadd processing", final_step=True)
    def run_coadd_processing(self):
        step_config = self.config["run_coadd_processing"]
        (process,) = self.run_pipeline_commands(
            [
                f"python config/find_tracts.py --repo {self.pipeline.repo_path} "
                f"--collection {self.OUTPUT} "
                f"--detector {step_config["find_tracts"]['detector']} "
                f"--visit {step_config["find_tracts"]['visit']}"
            ]
        )
        tract_numbers = json.loads(process.stdout.decode())
        self.build_drp_substep_graph(
            "step3",
            custom_pipeline=step_config["custom_pipeline"],
        )
        self.run_drp_substep(
            "step3",
            custom_pipeline=step_config["custom_pipeline"],
            config_override_files=step_config["config_override_files"],
            custom_data_query=f"skymap='decam_rings_v1' AND tract={tract_numbers[0]}",
        )

    def build_drp_substep_graph(self, substep: str, custom_pipeline: str = None):
        if substep not in self.VALID_SUBSTEPS:
            raise Exception(f"Invalid substep. Must be one of {self.VALID_SUBSTEPS}")

        step_config = self.config["build_drp_substep_graph"]
        env_prefix = (
            (
                f"export DRP_PIPE_DIR={self.DRP_PIPE_DIR};"
                f"export ANALYSIS_DRP_DIR={self.ANALYSIS_DRP_DIR};"
            )
            if step_config["export_custom_env_variables"]
            else ""
        )
        pipeline_file = (
            custom_pipeline
            or f"$DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#{substep}"
        )
        cmd_list = [
            (
                f"{env_prefix} "
                f"pipetask build "
                f"-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#{substep} "
                f"--show pipeline"
            ),
            (
                f"{env_prefix} "
                f"pipetask build "
                f"-p {pipeline_file} "
                f"--pipeline-dot /tmp/{self.pipeline.name}-pipeline.dot"
            ),
            (
                f"dot /tmp/{self.pipeline.name}-pipeline.dot -Tpdf > "
                f"{self.pipeline.logs_path}/pipeline_{substep}.pdf"
            ),
        ]
        self.run_pipeline_commands(cmd_list)

    def run_drp_substep(
        self,
        substep: str,
        config_overrides: List[str] = [],
        config_override_files: List[str] = [],
        custom_data_query: str = None,
        custom_pipeline: str = None,
    ):
        if substep not in self.VALID_SUBSTEPS:
            raise Exception(f"Invalid substep. Must be one of {self.VALID_SUBSTEPS}")

        step_config = self.config["run_drp_substep"]
        env_prefix = (
            (
                f"export DRP_PIPE_DIR={self.DRP_PIPE_DIR};"
                f"export ANALYSIS_DRP_DIR={self.ANALYSIS_DRP_DIR};"
            )
            if step_config["export_custom_env_variables"]
            else ""
        )
        data_query = (
            f"instrument='DECam'"
            f"{f' AND {custom_data_query}' if custom_data_query else ''}"
        )
        parsed_config_overrides = " ".join(
            [f"-c {override}" for override in config_overrides],
        )
        parsed_config_override_files = " ".join(
            [f"-C {override}" for override in config_override_files],
        )
        pipeline_file = (
            custom_pipeline
            or f"$DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#{substep}"
        )

        input = "DECam/defaults"
        logfile = f"{self.pipeline.logs_path}/4_01_{substep}.log"
        cmd_list = [
            f"date | tee {logfile};",
            (
                f"{env_prefix} "
                f"pipetask --long-log run --register-dataset-types -j 6 "
                f"-b {self.pipeline.repo_path} "
                f"--instrument lsst.obs.decam.DarkEnergyCamera "
                f"-i {input} "
                f"-o {self.OUTPUT} "
                f"{parsed_config_overrides} "
                f"{parsed_config_override_files} "
                f"-p {pipeline_file} "
                f'-d "{data_query}" '
                f"2>&1 | tee -a {logfile};"
            ),
            f"date | tee -a {logfile}",
        ]
        self.run_pipeline_commands(cmd_list)
