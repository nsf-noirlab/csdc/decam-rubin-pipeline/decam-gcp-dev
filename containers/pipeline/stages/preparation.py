import os
import shutil
import yaml

from manager.adapters.database import DatabaseConnector
from manager.adapters.gcs import GcsConnector
from stages.base import AbstractPipeline, BaseStage, Status, log_step


LOCAL_REFCATS_PREFIX = "data/refcats"


class PreparationStage(BaseStage):
    NAME = "Preparation"

    def __init__(self, pipeline: AbstractPipeline):
        super().__init__(pipeline, log_name="1_00_preparation")
        self._butler_seed_config_path = f"{self.pipeline.repo_path}/seed-config.yaml"
        # Reference catalogs used for photometric and astrometric calibration (static)
        self._reference_catalogs = [
            "gaia_dr2_GW170817",
            "ps1_dr1_GW170817",
        ]

    @property
    def reference_catalogs(self):
        return self._reference_catalogs

    def configure(self):
        super().configure()
        self.configure_butler_repository()

    def init_extra_connectors(self):
        self.connectors["database"] = DatabaseConnector(
            db_name=f"butler_{self.pipeline.name}"
        )
        self.connectors["gcs"] = GcsConnector(os.getenv("INPUT_BUCKET_NAME"))

    def start(self) -> str:
        self.pipeline.commit_status(Status.STARTED.name)
        self.generate_reference_catalogs()
        self.register_skymap()
        self.write_curated_calibrations()
        self.pipeline.commit_status(Status.FINISHED.name)

    @log_step("Butler configuration")
    def configure_butler_repository(self):
        # Create butler database
        self.connectors["database"].init_db(extensions_required=True)

        # Create butler repository
        self.create_butler_database_files()
        cmd_list = [
            (
                f"butler create --seed-config {self._butler_seed_config_path} "
                f"{self.pipeline.repo_path}"
            ),
            (
                f"butler register-instrument {self.pipeline.repo_path} "
                f"lsst.obs.decam.DarkEnergyCamera"
            ),
            f"butler query-dimension-records {self.pipeline.repo_path} physical_filter",
        ]
        self.run_pipeline_commands(cmd_list)

    def create_butler_database_files(self):
        engine_url = self.connectors["database"].get_engine().url
        # Create self._butler_seed_config_path YAML file
        # Writing the entire connection string to the registry.db entry allows us to use
        # both standard and unix domain socket connections (Cloud SQL Auth Proxy case)
        seed_config = {
            "datastore": {"root": "<butlerRoot>"},
            "registry": {
                "db": engine_url.render_as_string(False),
            },
        }
        with open(self._butler_seed_config_path, "w") as f:
            yaml.dump(seed_config, f)

    @log_step("Reference catalogs ingestion")
    def generate_reference_catalogs(self):
        self.retrieve_reference_catalogs_files()
        self.register_reference_catalogs_dataset_types()
        self.ingest_reference_catalogs()

    def retrieve_reference_catalogs_files(self):
        if self.connectors["gcs"].current_error is not None:
            # Get reference catalogs from filesystem at `pipeline/data/refcats`
            # when GCS connection could not be established
            for ref_cat in self._reference_catalogs:
                shutil.copytree(
                    f"{LOCAL_REFCATS_PREFIX}/{ref_cat}",
                    f"{self.pipeline.data_path}/refcats/{ref_cat}",
                )
            return

        blob_names = []
        for ref_cat in self._reference_catalogs:
            blobs = self.connectors["gcs"].list_bucket_files(
                prefix=f"refcats/{ref_cat}/",
                delimiter="/",
            )
            blob_names.extend(
                [blob_name for blob_name in blobs if blob_name.endswith(".fits")],
            )
        downloaded_list = self.connectors["gcs"].download_many_to_path(
            blob_names=blob_names,
            dest_dir=self.pipeline.data_path,
        )
        for name, result in downloaded_list:
            # The results list is either `None` or an exception for each blob in
            # the input list, in order.
            if isinstance(result, Exception):
                self.logger.error(
                    f"Failed to download {name} due to exception: {result}"
                )
            else:
                self.logger.info(
                    f"Downloaded {name} to {self.pipeline.data_path}/{name}"
                )

    def register_reference_catalogs_dataset_types(self):
        cmd_list = [
            *[
                (
                    f"butler register-dataset-type {self.pipeline.repo_path} "
                    f"{ref_cat} SimpleCatalog htm7"
                )
                for ref_cat in self._reference_catalogs
            ],
            f"butler query-dataset-types {self.pipeline.repo_path}",
        ]
        self.run_pipeline_commands(cmd_list)

    def ingest_reference_catalogs(self):
        cmd_list = [
            (
                f"python config/generate_refcat_ecsv.py --refcats "
                f"{' '.join(self._reference_catalogs)} --outdir {self.pipeline.path}"
            ),
            *[
                (
                    f"butler ingest-files -t link {self.pipeline.repo_path} "
                    f"{ref_cat} refcats {self.pipeline.path}/{ref_cat}.ecsv"
                )
                for ref_cat in self._reference_catalogs
            ],
            f"butler query-datasets --collections refcats {self.pipeline.repo_path}",
        ]
        self.run_pipeline_commands(cmd_list)

    @log_step("Skymap registration")
    def register_skymap(self):
        logfile = f"{self.pipeline.logs_path}/1_01_register_decam_rings_v1.log"
        cmd_list = [
            f"date | tee {logfile};",
            (
                f"butler register-skymap {self.pipeline.repo_path} "
                f"-C {self.config['register_skymap']['make_skymap']} "
                f"-c name='decam_rings_v1' "
                f"2>&1 | tee -a {logfile};"
            ),
            f"date | tee -a {logfile}",
            f"butler query-datasets {self.pipeline.repo_path} skyMap",
        ]
        self.run_pipeline_commands(cmd_list)

    @log_step("Write curated calibrations", final_step=True)
    def write_curated_calibrations(self):
        cmd_list = [
            (
                f"butler write-curated-calibrations {self.pipeline.repo_path} "
                f"lsst.obs.decam.DarkEnergyCamera"
            ),
            f'butler query-collections {self.pipeline.repo_path} "DECam/calib*"',
        ]
        self.run_pipeline_commands(cmd_list)
