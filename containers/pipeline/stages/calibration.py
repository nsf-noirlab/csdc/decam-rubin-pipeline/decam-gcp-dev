from stages.base import AbstractPipeline, BaseStage, Status, log_step


class CalibrationStage(BaseStage):
    NAME = "Calibration"
    DRP_PIPE_DIR = "config/pipelines/drp_pipe"
    ANALYSIS_DRP_DIR = "config/pipelines/analysis_drp"

    def __init__(self, pipeline: AbstractPipeline):
        super().__init__(pipeline, log_name="3_00_calibration")

    def configure(self):
        super().configure()
        self.validate_butler_config()

    def start(self):
        self.pipeline.commit_status(Status.STARTED.name)
        self.build_master_bias()
        self.certify_master_bias()
        self.generate_crosstalk_sources()
        self.build_master_flat()
        self.certify_master_flat()
        self.setup_default_collection()
        self.pipeline.commit_status(Status.FINISHED.name)

    @log_step("Build master bias")
    def build_master_bias(self):
        self.check_tasks_before_building("bias")
        self.build_calibration_frames("bias")

    @log_step("Certify master bias")
    def certify_master_bias(self):
        self.certify_calibration_frames("bias")

    @log_step("Build master flat")
    def build_master_flat(self):
        self.check_tasks_before_building("flat")
        self.build_calibration_frames("flat")

    @log_step("Certify master flat")
    def certify_master_flat(self):
        self.certify_calibration_frames("flat")

    @log_step("Generate crosstalk sources")
    def generate_crosstalk_sources(self):
        self.check_drp_step0_pipeline()
        self.run_drp_step0_pipeline()

    @log_step("Set up default collection", final_step=True)
    def setup_default_collection(self):
        input = "DECam/defaults"
        children = (
            "DECam/raw/all,DECam/calib,DECam/calib/crosstalk,"
            "DECam/calib/curated/19700101T000000Z,DECam/calib/unbounded,"
            "skymaps,refcats"
        )
        self.run_pipeline_commands(
            [
                f"butler collection-chain {self.pipeline.repo_path} {input} {children}",
            ]
        )

    def check_drp_step0_pipeline(self):
        step_config = self.config["check_drp_step0_pipeline"]
        env_prefix = (
            (
                f"export DRP_PIPE_DIR={self.DRP_PIPE_DIR};"
                f"export ANALYSIS_DRP_DIR={self.ANALYSIS_DRP_DIR};"
            )
            if step_config["export_custom_env_variables"]
            else ""
        )
        cmd_list = [
            (
                f"{env_prefix} "
                f"pipetask build -p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step0 "
                f"--show pipeline"
            ),
            (
                f"{env_prefix} "
                f"pipetask build -p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step0 "
                f"--pipeline-dot /tmp/{self.pipeline.name}-pipeline.dot"
            ),
            (
                f"dot /tmp/{self.pipeline.name}-pipeline.dot -Tpdf > "
                f"{self.pipeline.logs_path}/pipeline_step0.pdf"
            ),
        ]
        self.run_pipeline_commands(cmd_list)

    def run_drp_step0_pipeline(self):
        step_config = self.config["run_drp_step0_pipeline"]
        env_prefix = (
            (
                f"export DRP_PIPE_DIR={self.DRP_PIPE_DIR};"
                f"export ANALYSIS_DRP_DIR={self.ANALYSIS_DRP_DIR};"
            )
            if step_config["export_custom_env_variables"]
            else ""
        )
        (process,) = self.run_pipeline_commands(
            [f"python config/list_flat.py --repo {self.pipeline.repo_path}"]
        )
        exps_list = process.stdout.decode()
        logfile = f"{self.pipeline.logs_path}/3_02_step0_flat.log"
        cmd_list = [
            f"date | tee {logfile};",
            (
                f"{env_prefix} "
                f"pipetask --long-log run --register-dataset-types -j 6 "
                f"-b {self.pipeline.repo_path} "
                f"--instrument lsst.obs.decam.DarkEnergyCamera "
                f"-i DECam/raw/all,DECam/calib,DECam/calib/unbounded "
                f"-o DECam/calib/crosstalk "
                f"-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step0 "
                f"-d \"instrument='DECam' AND exposure IN {exps_list}\" "
                f"2>&1 | tee -a {logfile};"
            ),
            f"date | tee -a {logfile}",
        ]
        self.run_pipeline_commands(cmd_list)

        (process,) = self.run_pipeline_commands(
            [f"python config/list_raw.py --repo {self.pipeline.repo_path}"]
        )
        exps_list = process.stdout.decode()
        logfile = f"{self.pipeline.logs_path}/3_02_step0_science.log"
        cmd_list = [
            f"date | tee -a {logfile};",
            (
                f"{env_prefix} "
                f"pipetask --long-log run --register-dataset-types -j 6 "
                f"--extend-run --skip-existing "
                f"-b {self.pipeline.repo_path} "
                f"--instrument lsst.obs.decam.DarkEnergyCamera "
                f"-i DECam/raw/all,DECam/calib,DECam/calib/unbounded "
                f"-o DECam/calib/crosstalk "
                f"-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step0 "
                f"-d \"instrument='DECam' AND exposure IN {exps_list}\" "
                f"2>&1 | tee -a {logfile};"
            ),
            f"date | tee -a {logfile}",
            f"butler query-collections {self.pipeline.repo_path}",
            f"butler query-datasets {self.pipeline.repo_path} overscanRaw",
        ]
        self.run_pipeline_commands(cmd_list)

    def check_tasks_before_building(self, calib_type: str):
        valid_calib_types = ["bias", "flat"]
        if calib_type not in valid_calib_types:
            raise Exception(
                f"Invalid calibration type. Must be one of {valid_calib_types}"
            )
        step_config = self.config["check_tasks_before_building"]
        pipe_folder_path = step_config["pipeline_folder_path"]
        pipe_filename = f"cp{calib_type.capitalize()}"
        cmd_list = [
            (
                f"pipetask build -p {pipe_folder_path}{pipe_filename}.yaml "
                f"--show pipeline"
            ),
            (
                f"pipetask build -p {pipe_folder_path}{pipe_filename}.yaml "
                f"--pipeline-dot /tmp/{self.pipeline.name}-pipeline.dot"
            ),
            (
                f"dot /tmp/{self.pipeline.name}-pipeline.dot -Tpdf > "
                f"{self.pipeline.logs_path}/pipeline_{pipe_filename}.pdf"
            ),
        ]
        self.run_pipeline_commands(cmd_list)

    def build_calibration_frames(self, calib_type: str):
        valid_calib_types = ["bias", "flat"]
        if calib_type not in valid_calib_types:
            raise Exception(
                f"Invalid calibration type. Must be one of {valid_calib_types}"
            )
        pipe_filename = f"cp{calib_type.capitalize()}"
        (process,) = self.run_pipeline_commands(
            [f"python config/list_{calib_type}.py --repo {self.pipeline.repo_path}"]
        )
        exps_list = process.stdout.decode()
        step_config = self.config["build_calibration_frames"]
        pipe_folder_path = step_config["pipeline_folder_path"]
        logfile = f"{self.pipeline.logs_path}/3_01_{pipe_filename}.log"
        extra_collections = (
            "DECam/calib,DECam/calib/crosstalk," if calib_type == "flat" else ""
        )
        cmd_list = [
            f"date | tee {logfile};",
            (
                f"pipetask --long-log run --register-dataset-types -j 6 "
                f"-b {self.pipeline.repo_path} "
                f"--instrument lsst.obs.decam.DarkEnergyCamera "
                f"-i DECam/raw/all,{extra_collections}DECam/calib/curated/19700101T000000Z,DECam/calib/unbounded "
                f"-o DECam/calib/{calib_type} "
                f"-p {pipe_folder_path}{pipe_filename}.yaml "
                f"-d \"instrument='DECam' AND exposure IN {exps_list}\" "
                f"2>&1 | tee -a {logfile};"
            ),
            f"date | tee -a {logfile}",
            f'butler query-collections {self.pipeline.repo_path} "*{calib_type}*"',
            f"butler query-dataset-types {self.pipeline.repo_path}",
            (
                f"butler query-datasets {self.pipeline.repo_path} "
                f"--collections DECam/calib/{calib_type}"
            ),
            (
                f"butler query-datasets {self.pipeline.repo_path} "
                f"--collections DECam/calib/{calib_type} {calib_type}"
            ),
        ]
        self.run_pipeline_commands(cmd_list)

    def certify_calibration_frames(self, calib_type: str):
        valid_calib_types = ["bias", "flat"]
        if calib_type not in valid_calib_types:
            raise Exception(
                f"Invalid calibration type. Must be one of {valid_calib_types}"
            )
        cmd_list = [
            (
                f"butler certify-calibrations {self.pipeline.repo_path} "
                f"DECam/calib/{calib_type} DECam/calib {calib_type} "
                f"--begin-date 2017-08-01T00:00:00 --end-date 2017-08-31T23:59:59"
            ),
            f"python config/{calib_type}_date_range.py --repo {self.pipeline.repo_path}",
        ]
        self.run_pipeline_commands(cmd_list)
