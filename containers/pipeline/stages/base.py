from __future__ import annotations
import abc
import asyncio
from enum import Enum
import functools
from logging import Logger
import os
from subprocess import CompletedProcess
import time
from typing import Dict, List
import yaml

from manager.adapters.base import AbstractConnector
from manager.adapters.scipipe import LsstSciPipeConnector
from stages.logging import get_logger


def ensure_path_exists(path: str):
    if not os.path.exists(path):
        os.makedirs(path)


class Status(Enum):
    NOT_STARTED = "NOT_STARTED"
    CONFIGURING = "CONFIGURING"
    STARTED = "STARTED"
    FINISHED = "FINISHED"


class AbstractStage(abc.ABC):
    @property
    @abc.abstractmethod
    def config(self) -> Dict[str, Dict] | None:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def connectors(self) -> Dict[str, AbstractConnector]:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def logger(self) -> Logger | None:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def name(self) -> str:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def pipeline(self) -> AbstractPipeline:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def start_time(self) -> float:
        raise NotImplementedError

    def configure_logger(self):
        raise NotImplementedError

    @abc.abstractmethod
    def configure(self):
        raise NotImplementedError

    @abc.abstractmethod
    def init_extra_connectors(self):
        raise NotImplementedError

    @abc.abstractmethod
    def start(self) -> str:
        raise NotImplementedError

    @abc.abstractmethod
    def log_cmd_result(self, process: CompletedProcess):
        raise NotImplementedError

    @abc.abstractmethod
    def run_pipeline_commands(self, cmd_list: List[str]) -> List[CompletedProcess]:
        raise NotImplementedError


class AbstractPipeline(abc.ABC):
    @property
    @abc.abstractmethod
    def name(self) -> str:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def path(self) -> str:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def data_path(self) -> str:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def logs_path(self) -> str:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def repo_path(self) -> str:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def current_stage(self) -> AbstractStage | None:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def current_stage_status(self) -> str:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def next_stage(self) -> AbstractStage | None:
        raise NotImplementedError

    @abc.abstractmethod
    def is_new(self) -> bool:
        raise NotImplementedError

    @abc.abstractmethod
    def initialize(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def set_status(self, status: str) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def commit_files(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def commit_status(self, status: str) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def transition_to(self, stage: AbstractStage) -> None:
        raise NotImplementedError


def log_step(step_name: str, final_step: bool = False):
    """
    A decorator factory for logging a specific step inside an `AbstractStage` instance,
    including its execution time as well as the stage total elapsed time.
    Can only be used to decorate a method of an `AbstractStage` instance
    """

    def log_step_decorator(func):
        @functools.wraps(func)
        def log_step_wrapper(*args, **kwargs):
            stage_instance: AbstractStage
            (stage_instance,) = args
            logger = stage_instance.logger
            step_start_time = time.time()
            logger.info(
                f"\n{'=' * 60}\n"
                f"  Stage {stage_instance.name} - "
                f"Step \"{step_name}\" started\n"
                f"{'=' * 60}\n"
            )
            func_value = func(*args, **kwargs)
            step_end_time = time.time()
            step_exec_time = step_end_time - step_start_time
            stage_exec_time = step_end_time - stage_instance.start_time
            logger.info(
                f"\n{'=' * 60}\n"
                f"  Stage {stage_instance.name} - Step \"{step_name}\" finished\n\n"
                f"  - Step execution time: {step_exec_time:.4f} sec\n"
                f"  - Stage elapsed time: {stage_exec_time:.4f} sec\n"
                f"{'=' * 60}\n"
            )
            if final_step:
                logger.info(
                    f"\nStage {stage_instance.name} finished. "
                    f"Total execution time: {stage_exec_time:.4f} sec\n"
                    f"{'=' * 60}\n"
                )
            return func_value

        return log_step_wrapper

    return log_step_decorator


class BaseStage(AbstractStage):
    NAME = "BASE"

    def __init__(self, pipeline: AbstractPipeline, log_name: str = "0_00_base"):
        self._pipeline = pipeline
        self._connectors = {
            "scipipe": LsstSciPipeConnector(),
        }
        self.set_config()
        self.init_extra_connectors()
        self._log_filename = f"{log_name}.log"
        self._start_time = time.time()
        self._logger = None

    @property
    def config(self) -> Dict[str, Dict] | None:
        return self._config

    @property
    def connectors(self) -> Dict[str, AbstractConnector]:
        return self._connectors

    @property
    def logger(self):
        return self._logger

    @property
    def name(self) -> str:
        return self.NAME

    @property
    def pipeline(self):
        return self._pipeline

    @property
    def start_time(self) -> float:
        return self._start_time

    def set_config(self):
        config_dir = os.path.dirname(__file__)
        with open(os.path.join(config_dir, "config.yaml")) as file:
            config = yaml.safe_load(file)
        current_version = self.connectors["scipipe"].version
        try:
            self._config = config[current_version]
        except KeyError:
            raise Exception(
                f"LSST Science Pipelines {current_version} not supported. "
                f"Supported versions: {", ".join(config.keys())}"
            )
        self._config = self._config.get(self.NAME.lower())

    def configure_logger(self):
        self._logger = get_logger(
            f"{self._pipeline.name}.{self.name.lower()}",
            f"{self._pipeline.logs_path}/{self._log_filename}",
        )

    def configure(self):
        self._pipeline.transition_to(self)
        self._pipeline.commit_status(Status.CONFIGURING.name)
        self.configure_logger()
        self._start_time = time.time()

    def init_extra_connectors(self):
        return

    def start(self) -> str:
        super().start()

    def log_cmd_result(self, process: CompletedProcess):
        if process.stdout:
            self._logger.info(f"\n{process.stdout.decode()}")
        else:
            self._logger.error(f"\n{process.stderr.decode()}")

    def run_pipeline_commands(self, cmd_list: List[str]) -> List[CompletedProcess]:
        processes = []
        for cmd in cmd_list:
            process = self.connectors["scipipe"].run_cmd(cmd)
            self.log_cmd_result(process)
            processes.append(process)
        return processes

    @log_step("Validate butler configuration")
    def validate_butler_config(self):
        (process,) = self.run_pipeline_commands(
            [
                f"butler config-validate {self.pipeline.repo_path}",
            ]
        )
        ok_msg = "No problems encountered with configuration"
        if ok_msg not in process.stdout.decode():
            raise Exception(
                "An error occurred while validating butler configuration so execution "
                "could not continue. Check execution logs for details of this error"
            )


class BackgroundExecutor:
    def __init__(self, stage: AbstractStage) -> None:
        self._stage = stage
        self._background_tasks = set()

    async def trigger_stage_execution(self):
        self._stage.start()

    def trigger(self):
        """
        Trigger stage in background, creating an asyncio task with a strong
        reference to avoid garbage collection in the middle of its execution.
        The task is added to a set to reliably execute it until completion.
        More info on: https://docs.python.org/3/library/asyncio-task.html#creating-tasks
        """
        task = asyncio.create_task(self.trigger_stage_execution())
        self._background_tasks.add(task)
        task.add_done_callback(self._background_tasks.discard)
