from manager.adapters.database import DatabaseConnector
from stages.base import AbstractPipeline, BaseStage, Status, log_step


class APStage(BaseStage):
    NAME = "AP"

    def __init__(self, pipeline: AbstractPipeline):
        super().__init__(pipeline, log_name="5_00_ap")

    def configure(self):
        super().configure()
        self.validate_butler_config()
        self.configure_apdb()

    def init_extra_connectors(self):
        self.connectors["apdb"] = DatabaseConnector(
            db_name=f"apdb_{self.pipeline.name}"
        )

    def start(self):
        self.pipeline.commit_status(Status.STARTED.name)
        self.build_ap_graph()
        self.run_ap_pipeline()
        self.pipeline.commit_status(Status.FINISHED.name)

    @log_step("Configure AP database")
    def configure_apdb(self):
        db_url = self.connectors["apdb"].get_engine().url.render_as_string(False)
        self.connectors["apdb"].init_db()
        self.run_pipeline_commands(
            [
                f"make_apdb.py -c isolation_level=READ_UNCOMMITTED -c db_url={db_url}",
            ]
        )

    def create_dia_pipe_config_file(self):
        """
        Creates a temporary diaPipe config file for the associated pipeline
        This config file can be passed as argument to pipetask commands
        """
        db_url = self.connectors["apdb"].get_engine().url.render_as_string(False)
        dia_pipe_config_file_path = f"/tmp/{self.pipeline.name}-dia_pipe_config.py"
        with open(dia_pipe_config_file_path, "w") as f:
            config_lines = [
                'config.apdb.isolation_level = "READ_UNCOMMITTED"',
                f'config.apdb.db_url = "{db_url}"',
            ]
            f.write("\n".join(config_lines))

        return dia_pipe_config_file_path

    @log_step("Build AP graph")
    def build_ap_graph(self):
        dia_pipe_config_file_path = self.create_dia_pipe_config_file()
        step_config = self.config["build_ap_graph"]
        cmd_list = [
            f"butler query-collections {self.pipeline.repo_path}",
            (
                "pipetask build "
                f"-p {step_config['pipeline']} "
                f"-C diaPipe:{dia_pipe_config_file_path} "
                "--show pipeline"
            ),
            (
                f"pipetask build "
                f"-p {step_config['pipeline']} "
                f"-C diaPipe:{dia_pipe_config_file_path} "
                f"--pipeline-dot /tmp/{self.pipeline.name}-pipeline.dot"
            ),
            (
                f"dot /tmp/{self.pipeline.name}-pipeline.dot -Tpdf > "
                f"{self.pipeline.logs_path}/pipeline_ap.pdf"
            ),
        ]
        self.run_pipeline_commands(cmd_list)

    @log_step("Run AP pipeline", final_step=True)
    def run_ap_pipeline(self):
        dia_pipe_config_file_path = self.create_dia_pipe_config_file()
        step_config = self.config["run_ap_pipeline"]
        input = "DECam/defaults"
        output = "DECam/runs/20250217"
        logfile = f"{self.pipeline.logs_path}/5_01_ap_pipe.log"
        # TODO: include -j 6 or similar
        cmd_list = [
            f"date | tee {logfile};",
            (
                f"pipetask --long-log run "
                f"-p {step_config['pipeline']} "
                f"--instrument lsst.obs.decam.DarkEnergyCamera "
                f"--register-dataset-types "
                f"-C diaPipe:{dia_pipe_config_file_path} "
                f"-C {step_config['calibrate_override']} "
                f"-b {self.pipeline.repo_path} "
                f"-i {input} "
                f"-o {output} "
                f'-d "{step_config["data_query"]}" '
                f"2>&1 | tee -a {logfile};"
            ),
            f"date | tee -a {logfile}",
        ]
        self.run_pipeline_commands(cmd_list)
