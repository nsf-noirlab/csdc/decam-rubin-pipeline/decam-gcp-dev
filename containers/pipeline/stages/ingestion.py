import os
import shutil

from manager.adapters.gcs import GcsConnector
from stages.base import AbstractPipeline, BaseStage, Status, log_step

LOCAL_RAW_PREFIX = "data/raw"


class IngestionStage(BaseStage):
    NAME = "Ingestion"

    def __init__(self, pipeline: AbstractPipeline):
        super().__init__(pipeline, log_name="2_00_ingestion")

    def configure(self):
        super().configure()
        self.validate_butler_config()

    def init_extra_connectors(self):
        self.connectors["gcs"] = GcsConnector(os.getenv("INPUT_BUCKET_NAME"))

    def start(self):
        self.pipeline.commit_status(Status.STARTED.name)
        self.ingest_raw_science_images()
        self.ingest_raw_bias_images()
        self.ingest_raw_flat_images()
        self.define_visits_for_raws()
        self.pipeline.commit_status(Status.FINISHED.name)

    @log_step("Ingest raw science images")
    def ingest_raw_science_images(self):
        self.retrieve_raw_files("science")
        self.ingest_raws_with_butler("science")

    @log_step("Ingest raw bias images")
    def ingest_raw_bias_images(self):
        self.retrieve_raw_files("bias")
        self.ingest_raws_with_butler("bias")

    @log_step("Ingest raw flat images")
    def ingest_raw_flat_images(self):
        self.retrieve_raw_files("flat")
        self.ingest_raws_with_butler("flat")

    @log_step("Define visits for raws", final_step=True)
    def define_visits_for_raws(self):
        logfile = f"{self.pipeline.logs_path}/2_02_define_visits.log"
        cmd_list = [
            f"date | tee {logfile};",
            (
                f"butler define-visits {self.pipeline.repo_path} "
                f"lsst.obs.decam.DarkEnergyCamera "
                f"2>&1 | tee -a {logfile};"
            ),
            f"date | tee -a {logfile}",
        ]
        self.run_pipeline_commands(cmd_list)

    def retrieve_raw_files(self, raw_type: str):
        if self.connectors["gcs"].current_error is not None:
            # Get raw images from filesystem at `pipeline/data/raw/<raw_type>`
            # when GCS connection could not be established
            shutil.copytree(
                f"{LOCAL_RAW_PREFIX}/{raw_type}",
                f"{self.pipeline.data_path}/raw/{raw_type}",
                dirs_exist_ok=True,
            )
            return

        blob_names = []
        blobs = self.connectors["gcs"].list_bucket_files(
            prefix=f"raw/{raw_type}/",
            delimiter="/",
        )
        blob_names.extend(
            [blob_name for blob_name in blobs if blob_name.endswith(".fz")],
        )
        downloaded_list = self.connectors["gcs"].download_many_to_path(
            blob_names=blob_names,
            dest_dir=self.pipeline.data_path,
        )
        for name, result in downloaded_list:
            if isinstance(result, Exception):
                self.logger.error(
                    f"Failed to download {name} due to exception: {result}"
                )
            else:
                self.logger.info(
                    f"Downloaded {name} to {self._pipeline.data_path}/{name}"
                )

    def ingest_raws_with_butler(self, raw_type: str):
        logfile = f"{self.pipeline.logs_path}/2_01_ingest_{raw_type}.log"
        rawfiles = f"{self.pipeline.data_path}/raw/{raw_type}/*.fz"
        obs_type_mapping = {
            "science": "science",
            "bias": "zero",
            "flat": "dome flat",
        }
        obs_type = obs_type_mapping.get(raw_type, raw_type)
        cmd_list = [
            f"date | tee {logfile};",
            (
                f"butler ingest-raws {self.pipeline.repo_path} "
                f"{rawfiles} --transfer link "
                f"2>&1 | tee -a {logfile};"
            ),
            f"date | tee -a {logfile}",
            f'butler query-collections {self.pipeline.repo_path} "DECam/raw/all"',
            (
                f"butler query-dimension-records {self.pipeline.repo_path} exposure "
                f"--where \"instrument='DECam' AND exposure.observation_type='{obs_type}'\""
            ),
        ]
        self.run_pipeline_commands(cmd_list)
