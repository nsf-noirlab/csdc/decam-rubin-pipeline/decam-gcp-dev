import os
from typing import Dict, Tuple
import shutil

from manager.adapters.gcs import GcsConnector
from stages.base import AbstractPipeline, AbstractStage, Status, ensure_path_exists
from stages.logging import get_logger
from stages.ap import APStage
from stages.calibration import CalibrationStage
from stages.drp import DRPStage
from stages.ingestion import IngestionStage
from stages.preparation import PreparationStage


STAGE_CLS_MAPPING: Dict[str, Tuple[AbstractStage, AbstractStage | None]] = {
    PreparationStage.NAME: (PreparationStage, IngestionStage),
    IngestionStage.NAME: (IngestionStage, CalibrationStage),
    CalibrationStage.NAME: (CalibrationStage, DRPStage),
    DRPStage.NAME: (DRPStage, APStage),
    APStage.NAME: (APStage, None),
}


class Pipeline(AbstractPipeline):
    def __init__(self, name: str = "decam-pipeline"):
        self._name = name
        self._home_dir = "/home/lsst/pipeline-executions"
        self._path = f"{self._home_dir}/{self.name}"
        self._data_path = f"{self._path}/data"
        self._logs_path = f"{self._path}/logs"
        self._repo_path = f"{self._path}/repo"
        self._status_log_path = f"{self._logs_path}/status.log"
        self._current_stage: AbstractStage | None = None
        self._current_stage_status: str | None = None
        self._next_stage: AbstractStage | None = None
        self._logger = None
        self.gcs_connector = GcsConnector(os.getenv("EXEC_BUCKET_NAME"))

    @property
    def name(self):
        return self._name

    @property
    def path(self):
        return self._path

    @property
    def data_path(self):
        return self._data_path

    @property
    def logs_path(self):
        return self._logs_path

    @property
    def repo_path(self):
        return self._repo_path

    @property
    def current_stage(self) -> AbstractStage | None:
        if self._current_stage or self.is_new():
            return self._current_stage

        curr_stage_str, _ = self._retrieve_status()
        curr_stage_cls = STAGE_CLS_MAPPING[curr_stage_str][0]
        self._current_stage = curr_stage_cls(self)
        return self._current_stage

    @property
    def current_stage_status(self) -> str:
        if self._current_stage_status or self.is_new():
            return self._current_stage_status

        _, curr_status_str = self._retrieve_status()
        self._current_stage_status = curr_status_str
        return self._current_stage_status

    @property
    def next_stage(self) -> AbstractStage | None:
        if self._next_stage:
            return self._next_stage

        if self.is_new():
            self._next_stage = PreparationStage(self)
            return self._next_stage

        curr_stage_str, _ = self._retrieve_status()
        next_stage_cls = STAGE_CLS_MAPPING[curr_stage_str][1]
        self._next_stage = None if next_stage_cls is None else next_stage_cls(self)
        return self._next_stage

    def is_new(self) -> bool:
        # A pipeline is new if it's not present on the filesystem where this method
        # is executed
        return not os.path.isdir(self.path) or not os.path.isfile(self._status_log_path)

    def initialize(self):
        if self.is_new():
            ensure_path_exists(self._data_path)
            ensure_path_exists(self._logs_path)
            ensure_path_exists(self._repo_path)

        self._logger = get_logger(f"{self._name}.status", self._status_log_path)

    def set_status(self, status: str):
        self._current_stage_status = Status[status].value
        self._log_current_status()

    def init_stage_to_attempt(self, stage: str) -> AbstractStage:
        stage_cls = STAGE_CLS_MAPPING[stage][0]
        return stage_cls(self)

    def commit_files(self):
        if self.gcs_connector.current_error is None:
            # Do not commit anything when GCS connection to exec bucket is successful
            # TODO: add env variable flag to know when to commit to filesystem instead
            # without involving GCS connections. If possible, remove `gcs_connector`
            return

        # Copy log files to filesystem at `pipeline/data/<pipeline_name>`
        # when GCS connection could not be established
        shutil.copytree(
            self._logs_path,
            f"data/executions/{self.name}/logs",
            dirs_exist_ok=True,
        )

    def commit_status(self, status: str):
        self.set_status(status)
        self.commit_files()

    def transition_to(self, stage: AbstractStage):
        if self.next_stage != stage:
            raise Exception(f"Transition to {stage.name} not allowed as next stage")

        if (
            self.current_stage is not None
            and self.current_stage_status != Status.FINISHED.value
        ):
            raise Exception(
                f"Transition to {stage.name} not allowed: "
                f"Current stage has not finished execution"
            )

        self.initialize()
        self._current_stage = self.next_stage
        next_stage_cls = STAGE_CLS_MAPPING[self.current_stage.name][1]
        self._next_stage = None if next_stage_cls is None else next_stage_cls(self)
        self.commit_status(Status.NOT_STARTED.name)

    def _log_current_status(self):
        if not self._logger:
            raise Exception(
                f"Logger not set for pipeline {self.name}. Call initialize first",
            )

        self._logger.info(f"{self.current_stage.name} - {self.current_stage_status}")

    def _retrieve_status(self) -> Tuple[str, str]:
        if self.is_new():
            raise Exception(
                f"Cannot retrieve status from new pipeline with name {self.name}",
            )

        last_line = ""
        with open(self._status_log_path, "r") as f:
            last_line = f.readlines()[-1]
        *_, stage, _, status = last_line.replace("\n", "").split(" ")
        return stage, status
