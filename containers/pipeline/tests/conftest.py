import pytest
from unittest.mock import patch


@pytest.fixture
def mock_base_stage_scipipe_connector():
    with patch(
        "stages.base.LsstSciPipeConnector", autospec=True
    ) as mock_scipipe_connector:
        scipipe_connector_instance = mock_scipipe_connector.return_value
        scipipe_connector_instance.version = "v26_0_2"
        yield scipipe_connector_instance


@pytest.fixture
def mock_pipeline_gcs_connector():
    with patch("stages.pipeline.GcsConnector", autospec=True) as mock_gcs_connector:
        yield mock_gcs_connector
