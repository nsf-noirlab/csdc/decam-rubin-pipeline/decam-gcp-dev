from typing import Dict, List


class FakeBlob:
    def __init__(self, name: str):
        self._created = False
        self.name = name

    def exists(self):
        return self._created

    def download_to_filename(self, filename: str):
        pass

    def upload_from_filename(self, filename: str):
        self._created = True


class FakeGcsClient:
    def __init__(self):
        self._buckets: Dict[str, FakeBucket] = {}

    def bucket(self, name: str):
        bucket = self._buckets.get(name) or FakeBucket(name)
        self._buckets[name] = bucket
        return bucket

    def list_blobs(self, bucket_name):
        bucket = self.bucket(bucket_name)
        return bucket.list_blobs()


class FakeBucket:
    def __init__(self, name: str):
        self._blobs: Dict[str, FakeBlob] = {}
        self._created = False
        self.name = name

    def exists(self):
        return self._created

    def create(self):
        self._created = True

    def get_blob(self, blob_name: str):
        blob = self._blobs.get(blob_name)
        return blob if blob and blob.exists() else None

    def blob(self, blob_name: str):
        blob = self._blobs.get(blob_name) or FakeBlob(blob_name)
        self._blobs[blob_name] = blob
        return blob

    def list_blobs(self):
        return [blob for blob in self._blobs.values() if blob.exists()]


class FakeKafkaTopicMetadata:
    pass


class FakeKafkaClusterMetadata:
    def __init__(self, topics: Dict[str, FakeKafkaTopicMetadata] = None):
        self.topics = topics or {}


class FakeKafkaProducer:
    def __init__(self, config: Dict[str, str | int | Dict]):
        self._config = config

    def list_topics(self, timeout: int = None):
        return FakeKafkaClusterMetadata()


class FakeSqlAlchemyMetadata:
    def __init__(self, tables: Dict[str, Dict] = {}):
        self.tables = tables

    def reflect(self, engine):
        pass


class FakeSqlAlchemyResult:
    def all(self):
        pass


class FakeSqlAlchemyConnection:
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def execute(self, statement):
        return FakeSqlAlchemyResult()


class FakeSqlAlchemyUrl:
    def __init__(self, url: str):
        self._conn_str = url

    def __repr__(self):
        return self._conn_str

    def render_as_string(self, hide_password: bool = True):
        return repr(self)


class FakeSqlAlchemyEngine:
    def __init__(self, url: str = ""):
        self.url = FakeSqlAlchemyUrl(url)

    def connect(self):
        return FakeSqlAlchemyConnection()


class FakeCompletedProcess:
    def __init__(self, args: str | List[str], stdout: bytes | str = b"output"):
        self.args = args
        self.returncode = 0
        self.stdout = stdout
        self.stderr = self.stdout
