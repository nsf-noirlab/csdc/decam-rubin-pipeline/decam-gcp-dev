from asyncio import iscoroutine
from unittest.mock import patch, Mock
import os
import pytest
import time

from tests.fakes import FakeCompletedProcess
from stages.base import (
    AbstractPipeline,
    AbstractStage,
    BackgroundExecutor,
    BaseStage,
    Status,
    ensure_path_exists,
)
from stages.pipeline import Pipeline

stage_abstractmethods = "stages.base.AbstractStage.__abstractmethods__"
pipeline_abstractmethods = "stages.base.AbstractPipeline.__abstractmethods__"


class TestEnsurePathExists:
    def test_does_nothing_when_path_already_exists(self):
        existing_path = "tests/unit/stages"
        assert os.path.exists(existing_path)
        ensure_path_exists(existing_path)
        assert os.path.exists(existing_path)

    def test_creates_directory_when_path_does_not_exist(self):
        non_existing_path = "tests/unit/stages/test_dir"
        assert not os.path.exists(non_existing_path)
        ensure_path_exists(non_existing_path)
        assert os.path.exists(non_existing_path)
        os.rmdir(non_existing_path)


class TestAbstractStage:
    def test_cant_instantiate_abc(self):
        with pytest.raises(TypeError) as err:
            AbstractStage()
        assert "Can't instantiate abstract class" in str(err.value)

    @patch(stage_abstractmethods, set())
    def test_abstract_methods(self):
        stage = AbstractStage()
        with pytest.raises(NotImplementedError):
            stage.config
        with pytest.raises(NotImplementedError):
            stage.connectors
        with pytest.raises(NotImplementedError):
            stage.logger
        with pytest.raises(NotImplementedError):
            stage.name
        with pytest.raises(NotImplementedError):
            stage.pipeline
        with pytest.raises(NotImplementedError):
            stage.start_time
        with pytest.raises(NotImplementedError):
            stage.configure_logger()
        with pytest.raises(NotImplementedError):
            stage.configure()
        with pytest.raises(NotImplementedError):
            stage.init_extra_connectors()
        with pytest.raises(NotImplementedError):
            stage.start()
        with pytest.raises(NotImplementedError):
            stage.log_cmd_result(FakeCompletedProcess("cmd"))
        with pytest.raises(NotImplementedError):
            stage.run_pipeline_commands([])


class TestAbstractPipeline:
    def test_cant_instantiate_abc(self):
        with pytest.raises(TypeError) as err:
            AbstractPipeline()
        assert "Can't instantiate abstract class" in str(err.value)

    @patch(pipeline_abstractmethods, set())
    @patch(stage_abstractmethods, set())
    def test_abstract_methods(self):
        pipeline = AbstractPipeline()
        with pytest.raises(NotImplementedError):
            pipeline.name
        with pytest.raises(NotImplementedError):
            pipeline.path
        with pytest.raises(NotImplementedError):
            pipeline.data_path
        with pytest.raises(NotImplementedError):
            pipeline.logs_path
        with pytest.raises(NotImplementedError):
            pipeline.repo_path
        with pytest.raises(NotImplementedError):
            pipeline.current_stage
        with pytest.raises(NotImplementedError):
            pipeline.current_stage_status
        with pytest.raises(NotImplementedError):
            pipeline.next_stage
        with pytest.raises(NotImplementedError):
            pipeline.is_new()
        with pytest.raises(NotImplementedError):
            pipeline.initialize()
        with pytest.raises(NotImplementedError):
            pipeline.set_status("status")
        with pytest.raises(NotImplementedError):
            pipeline.commit_files()
        with pytest.raises(NotImplementedError):
            pipeline.commit_status("status")
        with pytest.raises(NotImplementedError):
            pipeline.transition_to(AbstractStage())


class TestBaseStageProperties:
    def test_config_property_returns_none_by_default(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        assert stage.config is None

    def test_connectors_property_returns_dict_mapping_default_connectors(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        assert "scipipe" in stage.connectors.keys()
        assert stage.connectors["scipipe"] == mock_base_stage_scipipe_connector

    def test_logger_property_returns_none_by_default(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        assert stage.logger is None

    @patch("stages.base.get_logger", autospec=True)
    def test_logger_property_returns_logger_instance_when_set(
        self,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_logger = mock_get_logger.return_value
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        stage.configure_logger()
        assert stage.logger == mock_logger

    def test_name_property_returns_stage_name(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        assert stage.name == BaseStage.NAME

    def test_pipeline_property_returns_pipeline_instance(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        assert stage.pipeline == pipeline

    def test_start_time_property_returns_init_time(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        assert isinstance(stage.start_time, float)
        assert stage.start_time < time.time()


class TestBaseStageSetConfig:
    def test_sets_config_when_lsst_version_is_supported_and_stage_has_config(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        # We intentionally change `NAME` so we set an existing configuration
        stage.NAME = "Preparation"
        stage.set_config()
        assert type(stage.config) is dict
        assert len(stage.config.keys()) > 0

    def test_sets_config_as_none_when_lsst_version_is_supported_but_stage_has_no_config(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        stage.set_config()
        assert stage.config is None

    def test_raises_exception_when_lsst_version_is_not_supported(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        # Set unsupported version
        version = "v20_0_0"
        mock_base_stage_scipipe_connector.version = version
        pipeline = Pipeline("test-decam")
        with pytest.raises(Exception) as err:
            stage = BaseStage(pipeline)
            stage.set_config()
        assert f"LSST Science Pipelines {version} not supported" in str(err.value)


@patch("stages.base.get_logger", autospec=True)
class TestBaseStageConfigureLogger:
    def test_calls_get_logger_with_pipeline_and_stage_data(
        self,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_logger = mock_get_logger.return_value
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        stage.configure_logger()
        mock_get_logger.assert_called_once()
        assert mock_logger == stage.logger
        logger_name, logger_filename = mock_get_logger.call_args.args
        assert pipeline.name in logger_name
        assert stage.name.lower() in logger_name.lower()
        assert pipeline.logs_path in logger_filename


@patch.object(BaseStage, "configure_logger", autospec=True)
@patch.object(Pipeline, "transition_to", autospec=True)
@patch.object(Pipeline, "commit_status", autospec=True)
class TestBaseStageConfigure:
    def test_calls_configuration_steps_and_updates_start_time(
        self,
        mock_pipeline_commit_status,
        mock_pipeline_transition_to,
        mock_configure_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        original_start_time = stage.start_time
        stage.configure()
        mock_pipeline_transition_to.assert_called_once_with(pipeline, stage)
        mock_pipeline_commit_status.assert_called_once_with(
            pipeline,
            Status.CONFIGURING.name,
        )
        mock_configure_logger.assert_called_once()
        assert stage.start_time > original_start_time


@patch.object(AbstractStage, "start", autospec=True)
class TestBaseStageStart:
    def test_calls_super(
        self,
        mock_super_start,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        stage.start()
        mock_super_start.assert_called_once()


@patch("stages.base.get_logger", autospec=True)
class TestBaseStageLogCmdResult:
    def test_calls_logger_info_with_starting_line_break_when_cmd_has_no_errors(
        self,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_logger = mock_get_logger.return_value
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        stdout = "some_output"
        process = FakeCompletedProcess("some_cmd", stdout.encode())
        stage.configure_logger()
        stage.log_cmd_result(process)
        mock_logger.info.assert_called_once()
        mock_logger.error.assert_not_called()
        assert mock_logger.info.call_args.args[0] == f"\n{stdout}"

    def test_calls_logger_error_with_starting_line_break_when_cmd_has_errors(
        self,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_logger = mock_get_logger.return_value
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        process = FakeCompletedProcess("some_cmd", b"")
        stage.configure_logger()
        stage.log_cmd_result(process)
        mock_logger.info.assert_not_called()
        mock_logger.error.assert_called_once()
        assert mock_logger.error.call_args.args[0] == "\n"


@patch.object(BaseStage, "log_cmd_result", autospec=True)
class TestBaseStageRunPipelineCommands:
    def test_runs_scipipe_commands_and_logs_results_from_cmd_list_arg(
        self,
        mock_log_cmd_result,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        cmd_list = ["cmd_1", "cmd2"]
        processes = stage.run_pipeline_commands(cmd_list)
        run_cmd_calls = mock_base_stage_scipipe_connector.run_cmd.call_args_list
        assert len(run_cmd_calls) == len(cmd_list)
        assert mock_log_cmd_result.call_count == len(cmd_list)
        for i, process in enumerate(processes):
            assert run_cmd_calls[i].args[0] == cmd_list[i]
            assert mock_log_cmd_result.call_args_list[i].args[1] == process


@patch("stages.base.get_logger", autospec=True)
@patch.object(BaseStage, "run_pipeline_commands", autospec=True)
class TestBaseStageValidateButlerConfig:
    def test_calls_butler_config_command_and_receives_success_msg(
        self,
        mock_run_pipeline_commands,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_run_pipeline_commands.return_value = [
            FakeCompletedProcess("cmd", b"No problems encountered with configuration")
        ]
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        stage.configure_logger()
        stage.validate_butler_config()
        (butler_config_cmd,) = mock_run_pipeline_commands.call_args.args[1]
        assert "butler config-validate" in butler_config_cmd
        assert pipeline.repo_path in butler_config_cmd

    def test_calls_butler_config_command_and_raises_when_there_are_errors(
        self,
        mock_run_pipeline_commands,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_run_pipeline_commands.return_value = [
            FakeCompletedProcess(
                "cmd",
                b"Butler configuration error\n",
            )
        ]
        pipeline = Pipeline("test-decam")
        stage = BaseStage(pipeline)
        stage.configure_logger()
        with pytest.raises(Exception) as err:
            stage.validate_butler_config()
        assert "An error occurred while validating butler configuration" in str(
            err.value
        )


class TestBackgroundExecutorTriggerStageExecution:
    def test_returns_coroutine(self):
        executor = BackgroundExecutor(Mock())
        result = executor.trigger_stage_execution()
        assert iscoroutine(result)

    @pytest.mark.asyncio
    async def test_calls_stage_methods(self):
        mock_stage = Mock()
        executor = BackgroundExecutor(mock_stage)
        await executor.trigger_stage_execution()
        mock_stage.start.assert_called_once()


@patch.object(BackgroundExecutor, "trigger_stage_execution")
@patch("stages.base.asyncio.create_task")
class TestBackgroundExecutorTrigger:
    def test_creates_task_with_stage_execution(
        self,
        mock_asyncio_create_task,
        mock_trigger_stage_execution,
    ):
        executor = BackgroundExecutor(Mock())
        executor.trigger()
        mock_asyncio_create_task.assert_called_once()
        mock_trigger_stage_execution.assert_called_once()
