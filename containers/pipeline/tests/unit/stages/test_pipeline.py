import os
import pytest
import shutil
from unittest.mock import Mock, patch

from stages.base import Status, ensure_path_exists
from stages.pipeline import Pipeline
from stages.ap import APStage
from stages.calibration import CalibrationStage
from stages.ingestion import IngestionStage
from stages.preparation import PreparationStage


class TestPipelineSimpleProperties:
    def test_name_property_returns_init_passed_name(self, mock_pipeline_gcs_connector):
        name = "test-pipeline"
        pipeline = Pipeline(name)
        assert pipeline.name == name

    def test_path_property_returns_path_including_name(
        self,
        mock_pipeline_gcs_connector,
    ):
        name = "test-pipeline"
        pipeline = Pipeline(name)
        assert pipeline.name in pipeline.path

    def test_folders_properties_returns_corresponding_names(
        self,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        assert pipeline.data_path.split("/")[-1] == "data"
        assert pipeline.logs_path.split("/")[-1] == "logs"
        assert pipeline.repo_path.split("/")[-1] == "repo"


class TestPipelineCurrentStage:
    def test_returns_current_stage_when_already_set(self, mock_pipeline_gcs_connector):
        pipeline = Pipeline("test-pipeline")
        stage_mock = Mock()
        pipeline._current_stage = stage_mock
        assert pipeline.current_stage == stage_mock

    @patch.object(Pipeline, "is_new", autospec=True)
    def test_returns_none_when_pipeline_is_new(
        self,
        mock_is_new,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = True
        pipeline = Pipeline("test-pipeline")
        assert pipeline.current_stage is None

    @patch.object(Pipeline, "is_new", autospec=True)
    @patch.object(Pipeline, "_retrieve_status", autospec=True)
    @patch("stages.ingestion.GcsConnector", autospec=True)
    def test_calculates_current_stage_by_retrieving_status_log_when_pipeline_exists(
        self,
        mock_gcs_connector,
        mock_retrieve_status,
        mock_is_new,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = False
        status_tuple = (IngestionStage.NAME, Status.NOT_STARTED.name)
        mock_retrieve_status.return_value = status_tuple
        pipeline = Pipeline("test-pipeline")
        current_stage = pipeline.current_stage
        assert isinstance(current_stage, IngestionStage)


class TestPipelineCurrentStageStatus:
    def test_returns_current_stage_status_when_already_set(
        self,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        pipeline._current_stage_status = Status.STARTED.name
        assert pipeline.current_stage_status == Status.STARTED.name

    @patch.object(Pipeline, "is_new", autospec=True)
    def test_returns_none_when_pipeline_is_new(
        self,
        mock_is_new,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = True
        pipeline = Pipeline("test-pipeline")
        assert pipeline.current_stage_status is None

    @patch.object(Pipeline, "is_new", autospec=True)
    @patch.object(Pipeline, "_retrieve_status", autospec=True)
    def test_calculates_current_stage_status_by_retrieving_status_log_when_not_set(
        self,
        mock_retrieve_status,
        mock_is_new,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = False
        status_tuple = (PreparationStage.NAME, Status.CONFIGURING.name)
        mock_retrieve_status.return_value = status_tuple
        pipeline = Pipeline("test-pipeline")
        assert pipeline.current_stage_status == Status.CONFIGURING.name


class TestPipelineNextStage:
    def test_returns_next_stage_when_already_set(self, mock_pipeline_gcs_connector):
        pipeline = Pipeline("test-pipeline")
        stage_mock = Mock()
        pipeline._next_stage = stage_mock
        assert pipeline.next_stage == stage_mock

    @patch.object(Pipeline, "is_new", autospec=True)
    @patch("stages.preparation.DatabaseConnector", autospec=True)
    @patch("stages.preparation.GcsConnector", autospec=True)
    def test_returns_preparation_stage_instance_when_pipeline_is_new(
        self,
        mock_gcs_connector,
        mock_database_connector,
        mock_is_new,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = True
        pipeline = Pipeline("test-pipeline")
        next_stage = pipeline.next_stage
        assert isinstance(next_stage, PreparationStage)

    @patch.object(Pipeline, "is_new", autospec=True)
    @patch.object(Pipeline, "_retrieve_status", autospec=True)
    def test_returns_none_when_current_stage_is_final_and_pipeline_exists(
        self,
        mock_retrieve_status,
        mock_is_new,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = False
        status_tuple = (APStage.NAME, Status.FINISHED.name)
        mock_retrieve_status.return_value = status_tuple
        pipeline = Pipeline("test-pipeline")
        assert pipeline.next_stage is None

    @patch.object(Pipeline, "is_new", autospec=True)
    @patch.object(Pipeline, "_retrieve_status", autospec=True)
    @patch("stages.preparation.DatabaseConnector", autospec=True)
    @patch("stages.preparation.GcsConnector", autospec=True)
    def test_calculates_next_stage_when_current_stage_is_not_final_and_pipeline_exists(
        self,
        mock_gcs_connector,
        mock_database_connector,
        mock_retrieve_status,
        mock_is_new,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = False
        status_tuple = (IngestionStage.NAME, Status.FINISHED.name)
        mock_retrieve_status.return_value = status_tuple
        pipeline = Pipeline("test-pipeline")
        assert isinstance(pipeline.next_stage, CalibrationStage)


class TestPipelineIsNew:
    def test_returns_false_when_pipeline_folders_exist_on_filesystem(
        self,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        ensure_path_exists(pipeline.logs_path)
        open(f"{pipeline.logs_path}/status.log", "a").close()
        assert not pipeline.is_new()
        shutil.rmtree(pipeline.path)

    def test_returns_true_when_pipeline_main_folder_does_not_exist_on_filesystem(
        self,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        assert pipeline.is_new()

    def test_returns_true_when_pipeline_status_file_does_not_exist_on_filesystem(
        self,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        ensure_path_exists(pipeline.path)
        assert pipeline.is_new()
        shutil.rmtree(pipeline.path)


@patch("stages.pipeline.get_logger", autospec=True)
@patch.object(Pipeline, "is_new", autospec=True)
class TestPipelineInitialize:
    def test_creates_folders_when_pipeline_is_new(
        self,
        mock_is_new,
        mock_get_logger,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = True
        pipeline = Pipeline("test-pipeline")
        pipeline.initialize()
        assert os.path.isdir(pipeline.data_path)
        assert os.path.isdir(pipeline.logs_path)
        assert os.path.isdir(pipeline.repo_path)
        shutil.rmtree(pipeline.path)

    def test_does_not_create_folders_when_pipeline_exists(
        self,
        mock_is_new,
        mock_get_logger,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = False
        pipeline = Pipeline("test-pipeline")
        pipeline.initialize()
        assert not os.path.isdir(pipeline.data_path)
        assert not os.path.isdir(pipeline.logs_path)
        assert not os.path.isdir(pipeline.repo_path)

    def test_initializes_pipeline_logger(
        self,
        mock_is_new,
        mock_get_logger,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = False
        pipeline = Pipeline("test-pipeline")
        pipeline.initialize()
        mock_get_logger.assert_called_once()


@patch.object(Pipeline, "_log_current_status", autospec=True)
class TestPipelineSetStatus:
    def test_sets_and_logs_status_when_passing_valid_status(
        self,
        mock_log_current_status,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        original_current_stage_status = pipeline.current_stage_status
        pipeline.set_status(Status.FINISHED.name)
        mock_log_current_status.assert_called_once()
        pipeline.current_stage_status != original_current_stage_status
        pipeline.current_stage_status == Status.FINISHED.value

    def test_raises_when_passing_invalid_status(
        self,
        mock_log_current_status,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        original_current_stage_status = pipeline.current_stage_status
        invalid_status = "INVALID_STATUS"
        with pytest.raises(KeyError) as err:
            pipeline.set_status(invalid_status)
        assert invalid_status in str(err.value)
        mock_log_current_status.assert_not_called()
        pipeline.current_stage_status == original_current_stage_status


class TestPipelineInitStageToAttempt:
    def test_returns_stage_instance_when_passing_valid_stage_name(
        self,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        stage = pipeline.init_stage_to_attempt(APStage.NAME)
        assert isinstance(stage, APStage)

    def test_raises_when_passing_invalid_stage_name(
        self,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        invalid_stage = "INVALID_STAGE"
        with pytest.raises(KeyError) as err:
            pipeline.init_stage_to_attempt(invalid_stage)
        assert invalid_stage in str(err.value)


class TestPipelineCommitFiles:
    def test_when_gcs_connection_fails_copies_logs_folder(
        self,
        mock_pipeline_gcs_connector,
    ):
        mock_gcs_connector_instance = mock_pipeline_gcs_connector.return_value
        mock_gcs_connector_instance.current_error = "Error connecting to GCS"
        pipeline = Pipeline("test-pipeline")
        # Create example log file and cleanup at the end
        ensure_path_exists(pipeline.logs_path)
        open(f"{pipeline.logs_path}/sample.log", "a").close()

        pipeline.commit_files()
        log_dir_copy = f"data/executions/{pipeline.name}/logs"
        assert os.path.exists(log_dir_copy)
        assert len(os.listdir(log_dir_copy)) > 0
        assert os.path.isfile(f"{log_dir_copy}/sample.log")
        shutil.rmtree(pipeline.path)
        shutil.rmtree(log_dir_copy.replace("/logs", ""))

    def test_when_gcs_connection_succeeds_does_not_copy_anything(
        self,
        mock_pipeline_gcs_connector,
    ):
        mock_gcs_connector_instance = mock_pipeline_gcs_connector.return_value
        mock_gcs_connector_instance.current_error = None
        pipeline = Pipeline("test-pipeline")
        pipeline.commit_files()
        log_dir_copy = f"data/executions/{pipeline.name}/logs"
        assert not os.path.exists(log_dir_copy)


@patch.object(Pipeline, "set_status", autospec=True)
@patch.object(Pipeline, "commit_files", autospec=True)
class TestPipelineCommitStatus:
    def test_sets_status_and_commits_files(
        self,
        mock_commit_files,
        mock_set_status,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        new_status = "SOME_STATUS"
        pipeline.commit_status(new_status)
        mock_set_status.assert_called_once_with(pipeline, new_status)
        mock_commit_files.assert_called_once_with(pipeline)


class TestPipelineTransitionTo:
    @patch.object(Pipeline, "next_stage", autospec=True)
    @patch.object(Pipeline, "current_stage", autospec=True)
    def test_raises_when_passed_stage_is_not_allowed_as_next_stage(
        self,
        mock_current_stage,
        mock_next_stage,
        mock_pipeline_gcs_connector,
    ):
        stage = Mock()
        pipeline = Pipeline("test-pipeline")
        with pytest.raises(Exception) as err:
            pipeline.transition_to(stage)
        assert f"Transition to {stage.name} not allowed as next stage" == str(err.value)

    @patch.object(Pipeline, "next_stage", autospec=True)
    @patch.object(Pipeline, "current_stage", autospec=True)
    @patch.object(Pipeline, "current_stage_status", autospec=True)
    def test_raises_when_current_stage_exists_and_has_not_finished(
        self,
        mock_current_stage_status,
        mock_current_stage,
        mock_next_stage,
        mock_pipeline_gcs_connector,
    ):
        mock_current_stage_status.return_value = Status.STARTED.value
        pipeline = Pipeline("test-pipeline")
        with pytest.raises(Exception) as err:
            pipeline.transition_to(mock_next_stage)
        assert f"Transition to {mock_next_stage.name} not allowed" in str(err.value)
        assert "Current stage has not finished execution" in str(err.value)

    @patch.object(Pipeline, "next_stage", autospec=True)
    @patch.object(Pipeline, "initialize", autospec=True)
    @patch.object(Pipeline, "commit_status", autospec=True)
    def test_transitions_successfully_to_initial_stage_and_sets_status_as_not_started(
        self,
        mock_commit_status,
        mock_initialize,
        mock_next_stage,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        mock_next_stage.name = PreparationStage.NAME
        assert pipeline.current_stage is None
        pipeline.transition_to(mock_next_stage)
        assert pipeline.current_stage == mock_next_stage
        # Internal calls should be avoided but for simplicity we'll allow it here
        assert isinstance(pipeline._next_stage, IngestionStage)
        mock_initialize.assert_called_once()
        mock_commit_status.assert_called_once_with(pipeline, Status.NOT_STARTED.name)

    @patch.object(Pipeline, "next_stage", autospec=True)
    @patch.object(Pipeline, "initialize", autospec=True)
    @patch.object(Pipeline, "commit_status", autospec=True)
    def test_transitions_successfully_to_mid_stage_and_sets_status_as_not_started(
        self,
        mock_commit_status,
        mock_initialize,
        mock_next_stage,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        mock_next_stage.name = IngestionStage.NAME
        pipeline.transition_to(mock_next_stage)
        assert pipeline.current_stage == mock_next_stage
        # Internal calls should be avoided but for simplicity we'll allow it here
        assert isinstance(pipeline._next_stage, CalibrationStage)
        mock_initialize.assert_called_once()
        mock_commit_status.assert_called_once_with(pipeline, Status.NOT_STARTED.name)

    @patch.object(Pipeline, "next_stage", autospec=True)
    @patch.object(Pipeline, "initialize", autospec=True)
    @patch.object(Pipeline, "commit_status", autospec=True)
    def test_transitions_successfully_to_final_stage_and_sets_status_as_not_started(
        self,
        mock_commit_status,
        mock_initialize,
        mock_next_stage,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-pipeline")
        mock_next_stage.name = APStage.NAME
        pipeline.transition_to(mock_next_stage)
        assert pipeline.current_stage == mock_next_stage
        # Internal calls should be avoided but for simplicity we'll allow it here
        assert pipeline._next_stage is None
        mock_initialize.assert_called_once()
        mock_commit_status.assert_called_once_with(pipeline, Status.NOT_STARTED.name)


class TestPipelineLogCurrentStatus:
    def test_raises_when_logger_is_not_set(self, mock_pipeline_gcs_connector):
        name = "test-pipeline"
        pipeline = Pipeline(name)
        with pytest.raises(Exception) as err:
            pipeline._log_current_status()
        assert f"Logger not set for pipeline {name}" in str(err.value)
        assert "Call initialize first" in str(err.value)

    @patch("stages.pipeline.get_logger", autospec=True)
    @patch.object(Pipeline, "is_new", autospec=True)
    @patch.object(Pipeline, "current_stage", autospec=True)
    @patch.object(Pipeline, "current_stage_status", autospec=True)
    def test_logs_current_stage_name_and_current_stage_status(
        self,
        mock_current_stage_status,
        mock_current_stage,
        mock_is_new,
        mock_get_logger,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = False
        pipeline = Pipeline("test-pipeline")
        pipeline.initialize()
        pipeline._log_current_status()
        mock_logger = mock_get_logger.return_value
        mock_logger.info.assert_called_once_with(
            f"{mock_current_stage.name} - {mock_current_stage_status}"
        )


@patch.object(Pipeline, "is_new", autospec=True)
class TestPipelineRetrieveStatus:
    def test_raises_when_pipeline_is_new(
        self,
        mock_is_new,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = True
        pipeline = Pipeline("test-pipeline")
        with pytest.raises(Exception) as err:
            pipeline._retrieve_status()
        assert "Cannot retrieve status from new pipeline" in str(err.value)

    def test_returns_tuple_after_reading_status_log_last_line_when_pipeline_exists(
        self,
        mock_is_new,
        mock_pipeline_gcs_connector,
    ):
        mock_is_new.return_value = False
        pipeline = Pipeline("test-pipeline")
        stage_name = PreparationStage.NAME
        status_name = Status.FINISHED.value
        ensure_path_exists(pipeline.logs_path)
        with open(f"{pipeline.logs_path}/status.log", "a") as f:
            f.writelines(
                [
                    (
                        f"2024-07-24 04:43:15,218 - INFO: {PreparationStage.NAME} - "
                        f"{Status.STARTED.value}\n"
                    ),
                    f"2024-07-24 04:43:15,225 - INFO: {stage_name} - {status_name}",
                ]
            )
        stage_result, status_result = pipeline._retrieve_status()
        assert stage_result == stage_name
        assert status_result == status_name
        shutil.rmtree(pipeline.path)
