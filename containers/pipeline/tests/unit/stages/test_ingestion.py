import pytest
import os
import shutil
from unittest.mock import patch

from stages.base import BaseStage, ensure_path_exists
from stages.ingestion import IngestionStage
from stages.pipeline import Pipeline


@patch.object(BaseStage, "configure", autospec=True)
@patch.object(IngestionStage, "init_extra_connectors", autospec=True)
@patch.object(IngestionStage, "validate_butler_config", autospec=True)
class TestIngestionStageConfigure:
    def test_calls_super_and_validates_butler_config(
        self,
        mock_validate_butler_config,
        mock_init_extra_connectors,
        mock_super_configure,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        stage.configure()
        mock_super_configure.assert_called_once()
        mock_validate_butler_config.assert_called_once()


@patch("stages.ingestion.GcsConnector", autospec=True)
class TestPreparationStageInitExtraConnectors:
    def test_instantiates_extra_connectors(
        self,
        mock_gcs_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        gcs_connector_instance = mock_gcs_connector.return_value
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        stage.init_extra_connectors()
        assert stage.connectors["gcs"] == gcs_connector_instance


@patch.object(IngestionStage, "init_extra_connectors", autospec=True)
@patch.object(IngestionStage, "define_visits_for_raws", autospec=True)
@patch.object(IngestionStage, "ingest_raw_flat_images", autospec=True)
@patch.object(IngestionStage, "ingest_raw_bias_images", autospec=True)
@patch.object(IngestionStage, "ingest_raw_science_images", autospec=True)
@patch.object(Pipeline, "commit_status", autospec=True)
class TestIngestionStageStart:
    def test_calls_start_steps(
        self,
        mock_pipeline_commit_status,
        mock_ingest_raw_science_images,
        mock_ingest_raw_bias_images,
        mock_ingest_raw_flat_images,
        mock_define_visits_for_raws,
        mock_init_extra_connectors,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        stage.start()
        mock_ingest_raw_science_images.assert_called_once()
        mock_ingest_raw_bias_images.assert_called_once()
        mock_ingest_raw_flat_images.assert_called_once()
        mock_define_visits_for_raws.assert_called_once()
        assert mock_pipeline_commit_status.call_count == 2


@patch("stages.base.get_logger", autospec=True)
@patch.object(IngestionStage, "init_extra_connectors", autospec=True)
@patch.object(IngestionStage, "retrieve_raw_files", autospec=True)
@patch.object(IngestionStage, "ingest_raws_with_butler", autospec=True)
class TestIngestionStageIngestRawScienceImages:
    def test_calls_required_sub_steps(
        self,
        mock_ingest_raws_with_butler,
        mock_retrieve_raw_files,
        mock_init_extra_connectors,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        stage.configure_logger()
        stage.ingest_raw_science_images()
        mock_retrieve_raw_files.assert_called_once_with(stage, "science")
        mock_ingest_raws_with_butler.assert_called_once_with(stage, "science")


@patch("stages.base.get_logger", autospec=True)
@patch.object(IngestionStage, "init_extra_connectors", autospec=True)
@patch.object(IngestionStage, "retrieve_raw_files", autospec=True)
@patch.object(IngestionStage, "ingest_raws_with_butler", autospec=True)
class TestIngestionStageIngestRawBiasImages:
    def test_calls_required_sub_steps(
        self,
        mock_ingest_raws_with_butler,
        mock_retrieve_raw_files,
        mock_init_extra_connectors,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        stage.configure_logger()
        stage.ingest_raw_bias_images()
        mock_retrieve_raw_files.assert_called_once_with(stage, "bias")
        mock_ingest_raws_with_butler.assert_called_once_with(stage, "bias")


@patch("stages.base.get_logger", autospec=True)
@patch.object(IngestionStage, "init_extra_connectors", autospec=True)
@patch.object(IngestionStage, "retrieve_raw_files", autospec=True)
@patch.object(IngestionStage, "ingest_raws_with_butler", autospec=True)
class TestIngestionStageIngestRawFlatImages:
    def test_calls_required_sub_steps(
        self,
        mock_ingest_raws_with_butler,
        mock_retrieve_raw_files,
        mock_init_extra_connectors,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        stage.configure_logger()
        stage.ingest_raw_flat_images()
        mock_retrieve_raw_files.assert_called_once_with(stage, "flat")
        mock_ingest_raws_with_butler.assert_called_once_with(stage, "flat")


@patch("stages.ingestion.GcsConnector", autospec=True)
class TestIngestionStageRetrieveRawFiles:
    raw_science_test_folder = "data/test-raw"

    @patch("stages.ingestion.LOCAL_RAW_PREFIX", raw_science_test_folder)
    def test_when_gcs_connection_fails_and_local_raws_exist_copies_science_folder(
        self,
        mock_gcs_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_gcs_connector_instance = mock_gcs_connector.return_value
        mock_gcs_connector_instance.current_error = "Error connecting to GCS"
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        # Create example raw science folder and files and cleanup at the end
        raw_type_folder = f"{self.raw_science_test_folder}/raw_type"
        ensure_path_exists(raw_type_folder)
        open(f"{raw_type_folder}/sample.fz", "a").close()

        stage.retrieve_raw_files("raw_type")
        assert os.path.exists(pipeline.data_path)
        raw_files_dir = f"{pipeline.data_path}/raw/raw_type"
        assert os.path.exists(raw_files_dir)
        assert len(os.listdir(raw_files_dir)) > 0
        shutil.rmtree(pipeline.path)
        shutil.rmtree(self.raw_science_test_folder)

    @patch("stages.ingestion.LOCAL_RAW_PREFIX", raw_science_test_folder)
    def test_when_gcs_connection_fails_and_no_local_raws_exist_raises_file_not_found(
        self,
        mock_gcs_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_gcs_connector_instance = mock_gcs_connector.return_value
        mock_gcs_connector_instance.current_error = "Error connecting to GCS"
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        with pytest.raises(FileNotFoundError) as err:
            stage.retrieve_raw_files("raw_type")
        assert f"No such file or directory: '{self.raw_science_test_folder}" in str(
            err.value
        )

        assert not os.path.exists(pipeline.data_path)
        raw_files_dir = f"{pipeline.data_path}/raw/raw_type"
        assert not os.path.exists(raw_files_dir)

    @patch("stages.base.get_logger", autospec=True)
    def test_when_gcs_connection_succeeds_downloads_only_fz_files_to_raw_type_folder(
        self,
        mock_get_logger,
        mock_gcs_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_gcs_connector_instance = mock_gcs_connector.return_value
        mock_gcs_connector_instance.current_error = None
        mock_logger = mock_get_logger.return_value
        original_blob_names = [
            "123456.fz",
            "some_non_fits_file.jpg",
            "654321.fz",
        ]
        downloaded_results = [
            ("654321.fz", None),
            ("123456.fz", Exception("Network error")),
        ]
        mock_gcs_connector_instance.list_bucket_files.return_value = original_blob_names
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        mock_gcs_connector_instance.download_many_to_path.return_value = (
            downloaded_results
        )
        stage.configure_logger()
        stage.retrieve_raw_files("raw_type")
        download_call_args = mock_gcs_connector_instance.download_many_to_path.call_args
        assert download_call_args.kwargs["blob_names"] == ["123456.fz", "654321.fz"]
        assert download_call_args.kwargs["dest_dir"] == pipeline.data_path
        assert mock_logger.info.call_count == 1
        assert mock_logger.error.call_count == 1


@patch.object(IngestionStage, "init_extra_connectors", autospec=True)
@patch.object(IngestionStage, "run_pipeline_commands", autospec=True)
class TestIngestionStageIngestRawsWithButler:
    def test_calls_ingest_raws_butler_command_with_raw_type_images(
        self,
        mock_run_pipeline_commands,
        mock_init_extra_connectors,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        raw_type = "raw_type"
        stage.ingest_raws_with_butler(raw_type)
        mock_run_pipeline_commands.assert_called_once()
        _, ingest_raws_cmd, _, query_coll_cmd, query_dims_cmd = (
            mock_run_pipeline_commands.call_args.args[1]
        )
        assert "butler ingest-raws" in ingest_raws_cmd
        assert f"raw/{raw_type}/*.fz --transfer link" in ingest_raws_cmd
        assert "butler query-collections" in query_coll_cmd
        assert "DECam/raw/all" in query_coll_cmd
        assert "butler query-dimension-records" in query_dims_cmd
        assert (
            f"instrument='DECam' AND exposure.observation_type='{raw_type}'"
            in query_dims_cmd
        )


@patch("stages.base.get_logger", autospec=True)
@patch.object(IngestionStage, "init_extra_connectors", autospec=True)
@patch.object(IngestionStage, "run_pipeline_commands", autospec=True)
class TestIngestionStageDefineVisitsForRaws:
    def test_calls_define_visits_butler_command(
        self,
        mock_run_pipeline_commands,
        mock_init_extra_connectors,
        mock_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = IngestionStage(pipeline)
        stage.configure_logger()
        stage.define_visits_for_raws()
        mock_run_pipeline_commands.assert_called_once()
        _, define_visits_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert "butler define-visits" in define_visits_cmd
        assert "lsst.obs.decam.DarkEnergyCamera" in define_visits_cmd
