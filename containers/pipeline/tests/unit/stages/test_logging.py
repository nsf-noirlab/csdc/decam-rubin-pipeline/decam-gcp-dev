import logging
import os
import pytest

from stages.logging import filter_console_handler, get_logger, STAGES_NAMES


class TestFilterConsoleHandler:
    @pytest.mark.parametrize(
        "level", [logging.WARNING, logging.ERROR, logging.CRITICAL]
    )
    def test_returns_true_when_level_is_warning_or_higher(self, level):
        log_records_attrs = {
            "levelno": level,
            "msg": "An example log message with a warning",
        }
        log_record = logging.makeLogRecord(log_records_attrs)
        filter_result = filter_console_handler(log_record)
        assert filter_result

    @pytest.mark.parametrize("stage", STAGES_NAMES)
    def test_returns_true_when_record_msg_includes_any_of_the_stages_with_lower_level(
        self,
        stage,
    ):
        log_records_attrs = {
            "levelno": logging.INFO,
            "msg": f"An example log message with just info of {stage} stage",
        }
        log_record = logging.makeLogRecord(log_records_attrs)
        filter_result = filter_console_handler(log_record)
        assert filter_result

    @pytest.mark.parametrize("level", [logging.DEBUG, logging.INFO])
    def test_returns_false_when_record_msg_does_not_include_stages_and_level_is_lower(
        self, level
    ):
        log_records_attrs = {
            "levelno": level,
            "msg": "An example log message with just info but no stage",
        }
        log_record = logging.makeLogRecord(log_records_attrs)
        filter_result = filter_console_handler(log_record)
        assert not filter_result


class TestGetLogger:
    def test_returns_logger_with_file_handler_based_on_name_and_filename(
        self,
    ):
        name = "test-logger"
        filename = "test-logger-filename-path.log"
        logger = get_logger(name, filename)
        assert isinstance(logger, logging.Logger)
        assert logger.name == name
        assert len(logger.handlers) == 1
        assert isinstance(logger.handlers[0], logging.FileHandler)
        assert os.path.isfile(filename)
        os.remove(filename)

    def test_returns_same_logger_with_single_file_handler_when_called_multiple_times(
        self,
    ):
        name = "test-logger"
        filename = "test-logger-filename-path.log"
        logger = get_logger(name, filename)
        same_logger = get_logger(name, filename)
        assert logger == same_logger
        assert len(logger.handlers) == 1
        assert isinstance(logger.handlers[0], logging.FileHandler)
        assert os.path.isfile(filename)
        os.remove(filename)
