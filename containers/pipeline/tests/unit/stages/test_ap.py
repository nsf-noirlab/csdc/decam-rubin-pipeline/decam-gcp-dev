import os
from unittest.mock import patch


from stages.ap import BaseStage, APStage
from stages.pipeline import Pipeline


@patch.object(BaseStage, "configure", autospec=True)
@patch.object(APStage, "init_extra_connectors", autospec=True)
@patch.object(APStage, "validate_butler_config", autospec=True)
@patch.object(APStage, "configure_apdb", autospec=True)
class TestAPStageConfigure:
    def test_calls_configuration_steps_and_updates_start_time(
        self,
        mock_configure_apdb,
        mock_validate_butler_config,
        mock_init_extra_connectors,
        mock_super_configure,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = APStage(pipeline)
        stage.configure()
        mock_super_configure.assert_called_once()
        mock_validate_butler_config.assert_called_once()
        mock_configure_apdb.assert_called_once()


@patch("stages.ap.DatabaseConnector", autospec=True)
class TestAPStageInitExtraConnectors:
    def test_instantiates_extra_connectors(
        self,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        database_connector_instance = mock_database_connector.return_value
        pipeline = Pipeline("test-decam")
        stage = APStage(pipeline)
        stage.init_extra_connectors()
        assert stage.connectors["apdb"] == database_connector_instance


@patch("stages.ap.DatabaseConnector", autospec=True)
@patch.object(APStage, "init_extra_connectors", autospec=True)
@patch.object(APStage, "run_ap_pipeline", autospec=True)
@patch.object(APStage, "build_ap_graph", autospec=True)
@patch.object(Pipeline, "commit_status", autospec=True)
class TestAPStageStart:
    def test_calls_start_steps(
        self,
        mock_pipeline_commit_status,
        mock_build_ap_graph,
        mock_run_ap_pipeline,
        mock_init_extra_connectors,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = APStage(pipeline)
        stage.start()
        mock_build_ap_graph.assert_called_once()
        mock_run_ap_pipeline.assert_called_once()
        assert mock_pipeline_commit_status.call_count == 2


@patch("stages.base.get_logger", autospec=True)
@patch("stages.ap.DatabaseConnector", autospec=True)
@patch.object(APStage, "run_pipeline_commands", autospec=True)
class TestAPStageConfigureApdb:
    def test_creates_apdb_and_initializes_schema(
        self,
        mock_run_pipeline_commands,
        mock_database_connector,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        db_connector_instance = mock_database_connector.return_value
        db_url = "test://db/url"
        db_connector_instance.get_engine.return_value.url.render_as_string.return_value = db_url
        pipeline = Pipeline("test-decam")
        stage = APStage(pipeline)
        stage.configure_logger()
        stage.configure_apdb()
        db_connector_instance.get_engine.assert_called_once()
        db_connector_instance.init_db.assert_called_once()
        mock_run_pipeline_commands.assert_called_once()
        (make_apdb_cmd,) = mock_run_pipeline_commands.call_args.args[1]
        assert (
            f"make_apdb.py -c isolation_level=READ_UNCOMMITTED -c db_url={db_url}"
            in make_apdb_cmd
        )


@patch("stages.ap.DatabaseConnector", autospec=True)
class TestAPStageCreateDiaPipeConfigFile:
    def test_temporary_file_creation_with_config_content(
        self,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        db_url = "test://user:pass@host/dbname?query1=value1"
        db_connector_instance = mock_database_connector.return_value
        db_connector_instance.get_engine.return_value.url.render_as_string.return_value = db_url
        pipeline = Pipeline("test-decam")
        stage = APStage(pipeline)
        config_file_path = stage.create_dia_pipe_config_file()
        assert config_file_path == f"/tmp/{pipeline.name}-dia_pipe_config.py"
        assert os.path.isfile(config_file_path)
        with open(config_file_path, "r") as f:
            config_lines = [line.replace("\n", "") for line in f.readlines()]
            assert 'config.apdb.isolation_level = "READ_UNCOMMITTED"' in config_lines
            assert f'config.apdb.db_url = "{db_url}"' in config_lines
        os.remove(config_file_path)


@patch("stages.base.get_logger", autospec=True)
@patch("stages.ap.DatabaseConnector", autospec=True)
@patch.object(APStage, "create_dia_pipe_config_file", autospec=True)
@patch.object(APStage, "run_pipeline_commands", autospec=True)
class TestAPStageBuildApGraph:
    def test_calls_pipetask_commands_with_ap_pipeline(
        self,
        mock_run_pipeline_commands,
        mock_create_dia_pipe_config_file,
        mock_database_connector,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        dia_pipe_config_file_path = "path/to/config/file"
        mock_create_dia_pipe_config_file.return_value = dia_pipe_config_file_path
        pipeline = Pipeline("test-decam")
        stage = APStage(pipeline)
        step_config = stage.config["build_ap_graph"]
        stage.configure_logger()
        stage.build_ap_graph()
        mock_create_dia_pipe_config_file.assert_called_once()
        mock_run_pipeline_commands.assert_called_once()
        q_colls_cmd, show_pipe_cmd, gen_dot_cmd, export_pdf_cmd = (
            mock_run_pipeline_commands.call_args.args[1]
        )
        assert "butler query-collections" in q_colls_cmd
        assert "pipetask build" in show_pipe_cmd
        assert f"-p {step_config['pipeline']}" in show_pipe_cmd
        assert (
            f"-C diaPipe:{dia_pipe_config_file_path} --show pipeline" in show_pipe_cmd
        )
        assert "pipetask build" in gen_dot_cmd
        assert f"-p {step_config['pipeline']}" in gen_dot_cmd
        assert f"-C diaPipe:{dia_pipe_config_file_path}" in gen_dot_cmd
        assert "--pipeline-dot /tmp/test-decam-pipeline.dot" in gen_dot_cmd
        assert "dot /tmp/test-decam-pipeline.dot -Tpdf" in export_pdf_cmd
        assert "pipeline_ap.pdf" in export_pdf_cmd


@patch("stages.base.get_logger", autospec=True)
@patch("stages.ap.DatabaseConnector", autospec=True)
@patch.object(APStage, "create_dia_pipe_config_file", autospec=True)
@patch.object(APStage, "run_pipeline_commands", autospec=True)
class TestAPStageRunDrpSubstep:
    def test_calls_pipetask_commands_with_ap_pipeline(
        self,
        mock_run_pipeline_commands,
        mock_create_dia_pipe_config_file,
        mock_database_connector,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        dia_pipe_config_file_path = "path/to/config/file"
        mock_create_dia_pipe_config_file.return_value = dia_pipe_config_file_path
        pipeline = Pipeline("test-decam")
        stage = APStage(pipeline)
        step_config = stage.config["run_ap_pipeline"]
        stage.configure_logger()
        stage.run_ap_pipeline()
        mock_create_dia_pipe_config_file.assert_called_once()
        mock_run_pipeline_commands.assert_called_once()
        _, pipe_run_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert "pipetask --long-log run" in pipe_run_cmd
        assert f"-p {step_config['pipeline']}" in pipe_run_cmd
        assert "--instrument lsst.obs.decam.DarkEnergyCamera" in pipe_run_cmd
        assert f"-C diaPipe:{dia_pipe_config_file_path}" in pipe_run_cmd
        assert f"-C {step_config['calibrate_override']}" in pipe_run_cmd
        assert "-i DECam/defaults" in pipe_run_cmd
        assert "-o DECam/runs/20250217" in pipe_run_cmd
        assert f'-d "{step_config["data_query"]}"' in pipe_run_cmd
