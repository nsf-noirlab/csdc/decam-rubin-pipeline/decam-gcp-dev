import os
import pytest
import shutil
from unittest.mock import patch

from tests.fakes import FakeSqlAlchemyEngine
from stages.base import BaseStage, ensure_path_exists
from stages.pipeline import Pipeline
from stages.preparation import PreparationStage


@patch.object(BaseStage, "configure", autospec=True)
@patch.object(PreparationStage, "init_extra_connectors", autospec=True)
@patch.object(PreparationStage, "configure_butler_repository", autospec=True)
class TestPreparationStageConfigure:
    def test_calls_super_and_butler_configurations(
        self,
        mock_configure_butler_repository,
        mock_init_extra_connectors,
        mock_super_configure,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.configure()
        mock_super_configure.assert_called_once()
        mock_configure_butler_repository.assert_called_once()


@patch("stages.preparation.DatabaseConnector", autospec=True)
@patch("stages.preparation.GcsConnector", autospec=True)
class TestPreparationStageInitExtraConnectors:
    def test_instantiates_extra_connectors(
        self,
        mock_gcs_connector,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        database_connector_instance = mock_database_connector.return_value
        gcs_connector_instance = mock_gcs_connector.return_value
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.init_extra_connectors()
        assert stage.connectors["database"] == database_connector_instance
        assert stage.connectors["gcs"] == gcs_connector_instance


@patch.object(PreparationStage, "init_extra_connectors", autospec=True)
@patch.object(PreparationStage, "generate_reference_catalogs", autospec=True)
@patch.object(PreparationStage, "register_skymap", autospec=True)
@patch.object(PreparationStage, "write_curated_calibrations", autospec=True)
@patch.object(Pipeline, "commit_status", autospec=True)
class TestPreparationStageStart:
    def test_calls_start_steps(
        self,
        mock_pipeline_commit_status,
        mock_write_curated_calibrations,
        mock_register_skymap,
        mock_generate_reference_catalogs,
        mock_init_extra_connectors,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.start()
        mock_generate_reference_catalogs.assert_called_once()
        mock_register_skymap.assert_called_once()
        mock_write_curated_calibrations.assert_called_once()
        assert mock_pipeline_commit_status.call_count == 2


@patch("stages.base.get_logger", autospec=True)
@patch("stages.preparation.DatabaseConnector", autospec=True)
@patch("stages.preparation.GcsConnector", autospec=True)
@patch.object(BaseStage, "run_pipeline_commands", autospec=True)
@patch.object(PreparationStage, "create_butler_database_files", autospec=True)
class TestPreparationStageConfigureButlerRepository:
    def test_calls_creation_and_instrument_registration_butler_command(
        self,
        mock_create_butler_database_files,
        mock_run_pipeline_commands,
        mock_gcs_connector,
        mock_database_connector,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        db_connector_instance = mock_database_connector.return_value
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.configure_logger()
        stage.configure_butler_repository()
        db_connector_instance.init_db.assert_called_with(extensions_required=True)
        mock_create_butler_database_files.assert_called_once()
        mock_run_pipeline_commands.assert_called_once()
        butler_create_cmd, reg_inst_cmd, query_check_cmd = (
            mock_run_pipeline_commands.call_args.args[1]
        )
        assert "butler create --seed-config" in butler_create_cmd
        assert pipeline.repo_path in butler_create_cmd
        assert "butler register-instrument" in reg_inst_cmd
        assert "lsst.obs.decam.DarkEnergyCamera" in reg_inst_cmd
        assert "butler query-dimension-records" in query_check_cmd
        assert "physical_filter" in query_check_cmd


@patch("stages.preparation.DatabaseConnector", autospec=True)
@patch("stages.preparation.GcsConnector", autospec=True)
class TestPreparationStageCreateButlerDatabaseFiles:
    def test_creation_fails_when_butler_directory_does_not_exist(
        self,
        mock_gcs_connector,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        database_connector_instance = mock_database_connector.return_value
        database_connector_instance.get_engine.return_value = FakeSqlAlchemyEngine(
            "postgresql+psycopg2://db_user:db_password@host/db_name"
        )
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        with pytest.raises(FileNotFoundError) as err:
            stage.create_butler_database_files()
        assert pipeline.repo_path in str(err.value)

    def test_creation_successful_when_butler_directory_exists(
        self,
        mock_gcs_connector,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        database_connector_instance = mock_database_connector.return_value
        database_connector_instance.get_engine.return_value = FakeSqlAlchemyEngine(
            "postgresql+psycopg2://db_user:db_password@host/db_name"
        )
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        ensure_path_exists(pipeline.repo_path)
        stage.create_butler_database_files()
        assert os.path.isfile(stage._butler_seed_config_path)
        shutil.rmtree(stage.pipeline.path)


@patch("stages.base.get_logger", autospec=True)
@patch.object(PreparationStage, "init_extra_connectors", autospec=True)
@patch.object(PreparationStage, "ingest_reference_catalogs", autospec=True)
@patch.object(
    PreparationStage, "register_reference_catalogs_dataset_types", autospec=True
)
@patch.object(PreparationStage, "retrieve_reference_catalogs_files", autospec=True)
class TestPreparationStageGenerateReferenceCatalogs:
    def test_calls_required_sub_steps(
        self,
        mock_retrieve_reference_catalogs_files,
        mock_register_reference_catalogs_dataset_types,
        mock_ingest_reference_catalogs,
        mock_init_extra_connectors,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.configure_logger()
        stage.generate_reference_catalogs()
        mock_retrieve_reference_catalogs_files.assert_called_once()
        mock_register_reference_catalogs_dataset_types.assert_called_once()
        mock_ingest_reference_catalogs.assert_called_once()


@patch("stages.preparation.DatabaseConnector", autospec=True)
@patch("stages.preparation.GcsConnector", autospec=True)
class TestPreparationStageRetrieveReferenceCatalogsFiles:
    refcats_test_folder = "data/test-refcats"

    @patch("stages.preparation.LOCAL_REFCATS_PREFIX", refcats_test_folder)
    def test_when_gcs_connection_fails_and_local_refcats_exist_copies_refcats_folders(
        self,
        mock_gcs_connector,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_gcs_connector_instance = mock_gcs_connector.return_value
        mock_gcs_connector_instance.current_error = "Error connecting to GCS"
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        # Create example refcats folders and files and cleanup at the end
        for refcat in stage.reference_catalogs:
            ensure_path_exists(f"{self.refcats_test_folder}/{refcat}")
            open(f"{self.refcats_test_folder}/{refcat}/sample.fits", "a").close()

        stage.retrieve_reference_catalogs_files()
        assert os.path.exists(pipeline.data_path)
        for refcat in stage.reference_catalogs:
            refcat_dir = f"{pipeline.data_path}/refcats/{refcat}"
            assert os.path.exists(refcat_dir)
            assert len(os.listdir(refcat_dir)) > 0
        shutil.rmtree(stage.pipeline.path)
        shutil.rmtree(self.refcats_test_folder)

    @patch("stages.preparation.LOCAL_REFCATS_PREFIX", refcats_test_folder)
    def test_when_gcs_connection_fails_and_no_local_refcats_exist_raises_file_not_found(
        self,
        mock_gcs_connector,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_gcs_connector_instance = mock_gcs_connector.return_value
        mock_gcs_connector_instance.current_error = "Error connecting to GCS"
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        with pytest.raises(FileNotFoundError) as err:
            stage.retrieve_reference_catalogs_files()
        assert f"No such file or directory: '{self.refcats_test_folder}" in str(
            err.value
        )

        assert not os.path.exists(pipeline.data_path)
        for refcat in stage.reference_catalogs:
            refcat_dir = f"{pipeline.data_path}/refcats/{refcat}"
            assert not os.path.exists(refcat_dir)

    @patch("stages.base.get_logger", autospec=True)
    def test_when_gcs_connection_succeeds_downloads_only_fits_files_to_refcats_folders(
        self,
        mock_get_logger,
        mock_gcs_connector,
        mock_database_connector,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_gcs_connector_instance = mock_gcs_connector.return_value
        mock_gcs_connector_instance.current_error = None
        mock_logger = mock_get_logger.return_value
        original_blob_names = [
            "123456.fits",
            "some_non_fits_file.jpg",
            "654321.fits",
        ]
        downloaded_results = [
            ("654321.fits", None),
            ("123456.fits", Exception("Network error")),
        ]
        mock_gcs_connector_instance.list_bucket_files.return_value = original_blob_names
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        mock_gcs_connector_instance.download_many_to_path.return_value = (
            downloaded_results * len(stage.reference_catalogs)
        )
        stage.configure_logger()
        stage.retrieve_reference_catalogs_files()
        download_call_args = mock_gcs_connector_instance.download_many_to_path.call_args
        assert download_call_args.kwargs["blob_names"] == [
            "123456.fits",
            "654321.fits",
        ] * len(stage.reference_catalogs)
        assert download_call_args.kwargs["dest_dir"] == pipeline.data_path
        assert mock_logger.info.call_count == 1 * len(stage.reference_catalogs)
        assert mock_logger.error.call_count == 1 * len(stage.reference_catalogs)


@patch.object(PreparationStage, "init_extra_connectors", autospec=True)
@patch.object(PreparationStage, "run_pipeline_commands", autospec=True)
class TestPreparationStageRegisterReferenceCatalogsDatasetTypes:
    def test_calls_refcat_registration_butler_commands(
        self,
        mock_run_pipeline_commands,
        mock_init_extra_connectors,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.register_reference_catalogs_dataset_types()
        mock_run_pipeline_commands.assert_called_once()
        # We register each refcat dataset type and perform one extra query to validate
        pipeline_cmds = mock_run_pipeline_commands.call_args.args[1]
        assert len(pipeline_cmds) == len(stage.reference_catalogs) + 1
        *refcat_reg_cmds, refcat_check_cmd = pipeline_cmds
        for i, refcat_reg_cmd in enumerate(refcat_reg_cmds):
            assert "butler register-dataset-type" in refcat_reg_cmd
            assert stage.reference_catalogs[i] in refcat_reg_cmd
        assert "butler query-dataset-types" in refcat_check_cmd


@patch.object(PreparationStage, "init_extra_connectors", autospec=True)
@patch.object(PreparationStage, "run_pipeline_commands", autospec=True)
class TestPreparationStageIngestReferenceCatalogs:
    def test_calls_refcat_ingest_files_butler_commands(
        self,
        mock_run_pipeline_commands,
        mock_init_extra_connectors,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.ingest_reference_catalogs()
        mock_run_pipeline_commands.assert_called_once()
        # We call a python script to generate ECSV files, then each refcat is
        # ingested to the butler and perform one extra query to validate
        pipeline_cmds = mock_run_pipeline_commands.call_args.args[1]
        assert len(pipeline_cmds) == len(stage.reference_catalogs) + 2
        refcat_ecsv_cmd, *refcat_ingest_cmds, refcat_check_cmd = pipeline_cmds
        assert "python config/generate_refcat_ecsv.py --refcats" in refcat_ecsv_cmd
        for i, refcat_ingest_cmd in enumerate(refcat_ingest_cmds):
            assert "butler ingest-files -t link" in refcat_ingest_cmd
            assert f"{stage.reference_catalogs[i]}.ecsv" in refcat_ingest_cmd
        assert "butler query-datasets --collections refcats" in refcat_check_cmd


@patch("stages.base.get_logger", autospec=True)
@patch.object(PreparationStage, "init_extra_connectors", autospec=True)
@patch.object(PreparationStage, "run_pipeline_commands", autospec=True)
class TestPreparationStageRegisterSkymap:
    def test_calls_skymap_registration_butler_command(
        self,
        mock_run_pipeline_commands,
        mock_init_extra_connectors,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.configure_logger()
        stage.register_skymap()
        mock_run_pipeline_commands.assert_called_once()
        # We register DECam skymap and perform one extra query to validate result
        _, reg_skymap_cmd, _, reg_check_cmd = mock_run_pipeline_commands.call_args.args[
            1
        ]
        assert "butler register-skymap" in reg_skymap_cmd
        assert (
            "-C $OBS_DECAM_DIR/config/makeSkyMap.py -c name='decam_rings_v1'"
            in reg_skymap_cmd
        )
        assert "butler query-datasets" in reg_check_cmd
        assert "skyMap" in reg_check_cmd


@patch("stages.base.get_logger", autospec=True)
@patch.object(PreparationStage, "init_extra_connectors", autospec=True)
@patch.object(PreparationStage, "run_pipeline_commands", autospec=True)
class TestPreparationStageWriteCuratedCalibrations:
    def test_calls_write_curated_calibrations_butler_command(
        self,
        mock_run_pipeline_commands,
        mock_init_extra_connectors,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = PreparationStage(pipeline)
        stage.configure_logger()
        stage.write_curated_calibrations()
        mock_run_pipeline_commands.assert_called_once()
        # We write curated calibrations for DECam and perform a query to validate result
        write_cur_calib_cmd, query_check_cmd = (
            mock_run_pipeline_commands.call_args.args[1]
        )
        assert "butler write-curated-calibrations" in write_cur_calib_cmd
        assert "lsst.obs.decam.DarkEnergyCamera" in write_cur_calib_cmd
        assert "butler query-collections" in query_check_cmd
        assert "DECam/calib*" in query_check_cmd
