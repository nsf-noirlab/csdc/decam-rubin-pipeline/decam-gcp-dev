import pytest
from unittest.mock import patch

from tests.fakes import FakeCompletedProcess
from stages.calibration import BaseStage, CalibrationStage
from stages.pipeline import Pipeline


@patch.object(BaseStage, "configure", autospec=True)
@patch.object(CalibrationStage, "validate_butler_config", autospec=True)
class TestCalibrationStageConfigure:
    def test_calls_super_and_validates_butler_config(
        self,
        mock_validate_butler_config,
        mock_super_configure,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.configure()
        mock_super_configure.assert_called_once()
        mock_validate_butler_config.assert_called_once()


@patch.object(CalibrationStage, "build_master_bias", autospec=True)
@patch.object(CalibrationStage, "certify_master_bias", autospec=True)
@patch.object(CalibrationStage, "generate_crosstalk_sources", autospec=True)
@patch.object(CalibrationStage, "build_master_flat", autospec=True)
@patch.object(CalibrationStage, "certify_master_flat", autospec=True)
@patch.object(CalibrationStage, "setup_default_collection", autospec=True)
@patch.object(Pipeline, "commit_status", autospec=True)
class TestCalibrationStageStart:
    def test_calls_start_steps(
        self,
        mock_pipeline_commit_status,
        mock_setup_default_collection,
        mock_certify_master_flat,
        mock_build_master_flat,
        mock_generate_crosstalk_sources,
        mock_certify_master_bias,
        mock_build_master_bias,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.start()
        mock_build_master_bias.assert_called_once()
        mock_certify_master_bias.assert_called_once()
        mock_generate_crosstalk_sources.assert_called_once()
        mock_build_master_flat.assert_called_once()
        mock_certify_master_flat.assert_called_once()
        mock_setup_default_collection.assert_called_once()
        assert mock_pipeline_commit_status.call_count == 2


@patch("stages.base.get_logger", autospec=True)
@patch.object(CalibrationStage, "check_tasks_before_building", autospec=True)
@patch.object(CalibrationStage, "build_calibration_frames", autospec=True)
class TestCalibrationStageBuildMasterBias:
    def test_calls_required_sub_steps(
        self,
        mock_build_calibration_frames,
        mock_check_tasks_before_building,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.configure_logger()
        stage.build_master_bias()
        mock_check_tasks_before_building.assert_called_once_with(stage, "bias")
        mock_build_calibration_frames.assert_called_once_with(stage, "bias")


@patch("stages.base.get_logger", autospec=True)
@patch.object(CalibrationStage, "certify_calibration_frames", autospec=True)
class TestCalibrationStageCertifyMasterBias:
    def test_calls_required_sub_steps(
        self,
        mock_certify_calibration_frames,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.configure_logger()
        stage.certify_master_bias()
        mock_certify_calibration_frames.assert_called_once_with(stage, "bias")


@patch("stages.base.get_logger", autospec=True)
@patch.object(CalibrationStage, "check_tasks_before_building", autospec=True)
@patch.object(CalibrationStage, "build_calibration_frames", autospec=True)
class TestCalibrationStageBuildMasterFlat:
    def test_calls_required_sub_steps(
        self,
        mock_build_calibration_frames,
        mock_check_tasks_before_building,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.configure_logger()
        stage.build_master_flat()
        mock_check_tasks_before_building.assert_called_once_with(stage, "flat")
        mock_build_calibration_frames.assert_called_once_with(stage, "flat")


@patch("stages.base.get_logger", autospec=True)
@patch.object(CalibrationStage, "certify_calibration_frames", autospec=True)
class TestCalibrationStageCertifyMasterFlat:
    def test_calls_required_sub_steps(
        self,
        mock_certify_calibration_frames,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.configure_logger()
        stage.certify_master_flat()
        mock_certify_calibration_frames.assert_called_once_with(stage, "flat")


@patch("stages.base.get_logger", autospec=True)
@patch.object(CalibrationStage, "check_drp_step0_pipeline", autospec=True)
@patch.object(CalibrationStage, "run_drp_step0_pipeline", autospec=True)
class TestCalibrationStageGenerateCrosstalkSources:
    def test_calls_required_sub_steps(
        self,
        mock_run_drp_step0_pipeline,
        mock_check_drp_step0_pipeline,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.configure_logger()
        stage.generate_crosstalk_sources()
        mock_check_drp_step0_pipeline.assert_called_once()
        mock_run_drp_step0_pipeline.assert_called_once()


@patch("stages.base.get_logger", autospec=True)
@patch.object(CalibrationStage, "run_pipeline_commands", autospec=True)
class TestCalibrarionStageSetupDefaultCollection:
    def test_calls_butler_command_with_input_and_children(
        self,
        mock_run_pipeline_commands,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.configure_logger()
        stage.setup_default_collection()
        mock_run_pipeline_commands.assert_called_once()
        (collection_chain_cmd,) = mock_run_pipeline_commands.call_args.args[1]
        assert "butler collection-chain" in collection_chain_cmd
        assert "DECam/defaults" in collection_chain_cmd


@patch.object(CalibrationStage, "run_pipeline_commands", autospec=True)
class TestCalibrationStageCheckDrpStep0Pipeline:
    def test_calls_pipetask_commands_with_drp_step0_pipeline_generating_pdf(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.check_drp_step0_pipeline()
        mock_run_pipeline_commands.assert_called_once()
        show_pipe_cmd, gen_dot_cmd, export_pdf_cmd = (
            mock_run_pipeline_commands.call_args.args[1]
        )
        assert "pipetask build -p" in show_pipe_cmd
        assert "DRP-Merian.yaml#step0 --show pipeline" in show_pipe_cmd
        assert "pipetask build -p" in gen_dot_cmd
        assert (
            "DRP-Merian.yaml#step0 --pipeline-dot /tmp/test-decam-pipeline.dot"
            in gen_dot_cmd
        )
        assert "dot /tmp/test-decam-pipeline.dot -Tpdf" in export_pdf_cmd
        assert "pipeline_step0.pdf" in export_pdf_cmd


@patch.object(CalibrationStage, "run_pipeline_commands", autospec=True)
class TestCalibrationStageRunDrpStep0Pipeline:
    def test_calls_pipetask_commands_with_drp_step0_pipeline(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        # Some of the calls for run_pipeline_commands use the resulting list of
        # `CompletedProcess` instances. In this particular case, such calls have one
        # element in the list (the ones with multiple elements are not used)
        mock_run_pipeline_commands.return_value = [FakeCompletedProcess("cmd")]
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.run_drp_step0_pipeline()
        assert mock_run_pipeline_commands.call_count == 4
        (
            flat_list_call_args,
            flat_step0_run_call_args,
            science_list_call_args,
            science_step0_run_call_args,
        ) = mock_run_pipeline_commands.call_args_list

        (flat_list_cmd,) = flat_list_call_args.args[1]
        assert "python config/list_flat.py" in flat_list_cmd

        _, flat_run_cmd, _ = flat_step0_run_call_args.args[1]
        assert "pipetask --long-log run" in flat_run_cmd
        assert "--instrument lsst.obs.decam.DarkEnergyCamera" in flat_run_cmd
        assert "-i DECam/raw/all,DECam/calib,DECam/calib/unbounded" in flat_run_cmd
        assert "-o DECam/calib/crosstalk" in flat_run_cmd
        assert "-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step0" in flat_run_cmd
        assert "-d \"instrument='DECam' AND exposure IN" in flat_run_cmd

        (science_list_cmd,) = science_list_call_args.args[1]
        assert "python config/list_raw.py" in science_list_cmd

        _, science_run_cmd, _, query_coll_cmd, query_data_cmd = (
            science_step0_run_call_args.args[1]
        )
        assert "pipetask --long-log run" in science_run_cmd
        assert "--extend-run --skip-existing" in science_run_cmd
        assert "--instrument lsst.obs.decam.DarkEnergyCamera" in science_run_cmd
        assert "-i DECam/raw/all,DECam/calib,DECam/calib/unbounded" in science_run_cmd
        assert "-o DECam/calib/crosstalk" in science_run_cmd
        assert (
            "-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step0" in science_run_cmd
        )
        assert "-d \"instrument='DECam' AND exposure IN" in science_run_cmd

        assert "butler query-collections" in query_coll_cmd
        assert "butler query-datasets" in query_data_cmd
        assert "overscanRaw" in query_data_cmd


class TestCalibrationStageCheckTasksBeforeBuilding:
    def test_raises_when_passing_invalid_calib_type(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        with pytest.raises(Exception) as err:
            stage.check_tasks_before_building("invalid_calib_type")
        assert "Invalid calibration type" in str(err.value)

    @patch.object(CalibrationStage, "run_pipeline_commands", autospec=True)
    def test_calls_pipetask_commands_generating_pdf_when_passing_valid_calib_type(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.check_tasks_before_building("bias")
        mock_run_pipeline_commands.assert_called_once()
        show_pipe_cmd, gen_dot_cmd, export_pdf_cmd = (
            mock_run_pipeline_commands.call_args.args[1]
        )
        assert "pipetask build -p" in show_pipe_cmd
        assert "cpBias.yaml --show pipeline" in show_pipe_cmd
        assert "pipetask build -p" in gen_dot_cmd
        assert "cpBias.yaml --pipeline-dot /tmp/test-decam-pipeline.dot" in gen_dot_cmd
        assert "dot /tmp/test-decam-pipeline.dot -Tpdf" in export_pdf_cmd
        assert "pipeline_cpBias.pdf" in export_pdf_cmd


class TestCalibrationStageBuildCalibrationFrames:
    def test_raises_when_passing_invalid_calib_type(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        with pytest.raises(Exception) as err:
            stage.build_calibration_frames("invalid_calib_type")
        assert "Invalid calibration type" in str(err.value)

    @patch.object(CalibrationStage, "run_pipeline_commands", autospec=True)
    def test_calls_pipetask_run_commands_when_passing_valid_calib_type(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        # Some of the calls for run_pipeline_commands use the resulting list of
        # `CompletedProcess` instances. In this particular case, such calls have one
        # element in the list (the ones with multiple elements are not used)
        mock_run_pipeline_commands.return_value = [FakeCompletedProcess("cmd")]
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.build_calibration_frames("bias")
        assert mock_run_pipeline_commands.call_count == 2
        (
            list_exps_call_args,
            cp_run_call_args,
        ) = mock_run_pipeline_commands.call_args_list

        (list_exps_cmd,) = list_exps_call_args.args[1]
        assert "python config/list_bias.py" in list_exps_cmd

        _, cp_run_cmd, *_, calib_dataset_cmd, calib_exps_dataset_cmd = (
            cp_run_call_args.args[1]
        )
        assert "pipetask --long-log run" in cp_run_cmd
        assert "--instrument lsst.obs.decam.DarkEnergyCamera" in cp_run_cmd
        assert (
            "-i DECam/raw/all,DECam/calib/curated/19700101T000000Z,DECam/calib/unbounded"
            in cp_run_cmd
        )
        assert "-o DECam/calib/bias" in cp_run_cmd
        assert "-p $CP_PIPE_DIR/pipelines/DarkEnergyCamera/cpBias.yaml" in cp_run_cmd
        assert "-d \"instrument='DECam' AND exposure IN" in cp_run_cmd
        assert "butler query-datasets" in calib_dataset_cmd
        assert "--collections DECam/calib/bias" in calib_dataset_cmd
        assert "butler query-datasets" in calib_exps_dataset_cmd
        assert "--collections DECam/calib/bias bias" in calib_exps_dataset_cmd


class TestCalibrarionStageCertifyCalibrationFrames:
    def test_raises_when_passing_invalid_calib_type(
        self,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        with pytest.raises(Exception) as err:
            stage.certify_calibration_frames("invalid_calib_type")
        assert "Invalid calibration type" in str(err.value)

    @patch.object(CalibrationStage, "run_pipeline_commands", autospec=True)
    def test_calls_pipetask_run_commands_when_passing_valid_calib_type(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = CalibrationStage(pipeline)
        stage.certify_calibration_frames("bias")
        mock_run_pipeline_commands.assert_called_once()
        certify_cmd, date_range_check_cmd = mock_run_pipeline_commands.call_args.args[1]
        assert "butler certify-calibrations" in certify_cmd
        assert "DECam/calib/bias DECam/calib bias" in certify_cmd
        assert (
            "--begin-date 2017-08-01T00:00:00 --end-date 2017-08-31T23:59:59"
            in certify_cmd
        )
        assert "python config/bias_date_range.py" in date_range_check_cmd
