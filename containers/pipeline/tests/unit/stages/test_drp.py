import pytest
from unittest.mock import patch

from tests.fakes import FakeCompletedProcess
from stages.drp import BaseStage, DRPStage
from stages.pipeline import Pipeline


@patch.object(BaseStage, "configure", autospec=True)
@patch.object(DRPStage, "validate_butler_config", autospec=True)
class TestDRPStageConfigure:
    def test_calls_super_and_validates_butler_config(
        self,
        mock_validate_butler_config,
        mock_super_configure,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        stage.configure()
        mock_super_configure.assert_called_once()
        mock_validate_butler_config.assert_called_once()


@patch.object(DRPStage, "run_coadd_processing", autospec=True)
@patch.object(DRPStage, "run_final_visit_summary", autospec=True)
@patch.object(DRPStage, "run_final_source_table_generation", autospec=True)
@patch.object(DRPStage, "run_global_collection_summary", autospec=True)
@patch.object(DRPStage, "run_tract_level_characterization", autospec=True)
@patch.object(DRPStage, "run_initial_visit_aggregation", autospec=True)
@patch.object(DRPStage, "run_single_frame_processing", autospec=True)
@patch.object(Pipeline, "commit_status", autospec=True)
class TestDRPStageStart:
    def test_calls_start_steps(
        self,
        mock_pipeline_commit_status,
        mock_run_single_frame_processing,
        mock_run_initial_visit_aggregation,
        mock_run_tract_level_characterization,
        mock_run_global_collection_summary,
        mock_run_final_source_table_generation,
        mock_run_final_visit_summary,
        mock_run_coadd_processing,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        stage.start()
        mock_run_single_frame_processing.assert_called_once()
        mock_run_initial_visit_aggregation.assert_called_once()
        mock_run_tract_level_characterization.assert_called_once()
        mock_run_global_collection_summary.assert_called_once()
        mock_run_final_source_table_generation.assert_called_once()
        mock_run_final_visit_summary.assert_called_once()
        mock_run_coadd_processing.assert_called_once()
        assert mock_pipeline_commit_status.call_count == 2


@patch("stages.base.get_logger", autospec=True)
@patch.object(DRPStage, "build_drp_substep_graph", autospec=True)
@patch.object(DRPStage, "run_drp_substep", autospec=True)
class TestDRPStageRunSingleFrameProcessing:
    def test_calls_required_sub_steps(
        self,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        step_config = stage.config["run_single_frame_processing"]
        stage.configure_logger()
        stage.run_single_frame_processing()
        mock_build_drp_substep_graph.assert_called_once_with(stage, "step1")
        mock_run_drp_substep.assert_called_once_with(
            stage,
            "step1",
            config_override_files=step_config["config_override_files"],
            custom_data_query=step_config["data_query"],
        )


@patch("stages.base.get_logger", autospec=True)
@patch.object(DRPStage, "build_drp_substep_graph", autospec=True)
@patch.object(DRPStage, "run_drp_substep", autospec=True)
class TestDRPStageRunInitialVisitAggregation:
    def test_calls_required_sub_steps(
        self,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        step_config = stage.config["run_initial_visit_aggregation"]
        stage.configure_logger()
        stage.run_initial_visit_aggregation()
        mock_build_drp_substep_graph.assert_called_once_with(stage, "step2a")
        mock_run_drp_substep.assert_called_once_with(
            stage,
            "step2a",
            custom_data_query=step_config["data_query"],
        )


@patch("stages.base.get_logger", autospec=True)
@patch.object(DRPStage, "build_drp_substep_graph", autospec=True)
@patch.object(DRPStage, "run_drp_substep", autospec=True)
class TestDRPStageRunTractLevelCharacterization:
    def test_calls_required_sub_steps(
        self,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        step_config = stage.config["run_tract_level_characterization"]
        stage.configure_logger()
        stage.run_tract_level_characterization()
        mock_build_drp_substep_graph.assert_called_once_with(
            stage,
            "step2b",
            custom_pipeline=step_config["custom_pipeline"],
        )
        mock_run_drp_substep.assert_called_once_with(
            stage,
            "step2b",
            custom_pipeline=step_config["custom_pipeline"],
            config_override_files=step_config["config_override_files"],
            custom_data_query="skymap='decam_rings_v1'",
        )


@patch("stages.base.get_logger", autospec=True)
@patch.object(DRPStage, "build_drp_substep_graph", autospec=True)
@patch.object(DRPStage, "run_drp_substep", autospec=True)
class TestDRPStageRunGlobalCollectionSummary:
    def test_calls_required_sub_steps_if_enabled(
        self,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        stage.config["run_global_collection_summary"]["enabled"] = True
        stage.configure_logger()
        stage.run_global_collection_summary()
        mock_build_drp_substep_graph.assert_called_once_with(stage, "step2c")
        mock_run_drp_substep.assert_called_once_with(stage, "step2c")

    def test_does_not_call_substeps_if_not_enabled(
        self,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        stage.config["run_global_collection_summary"]["enabled"] = False
        stage.configure_logger()
        stage.run_global_collection_summary()
        mock_build_drp_substep_graph.assert_not_called()
        mock_run_drp_substep.assert_not_called()


@patch("stages.base.get_logger", autospec=True)
@patch.object(DRPStage, "build_drp_substep_graph", autospec=True)
@patch.object(DRPStage, "run_drp_substep", autospec=True)
class TestDRPStageRunFinalSourceTableGeneration:
    def test_calls_required_sub_steps(
        self,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        step_config = stage.config["run_final_source_table_generation"]
        stage.configure_logger()
        stage.run_final_source_table_generation()
        mock_build_drp_substep_graph.assert_called_once_with(
            stage,
            "step2d",
            custom_pipeline=step_config["custom_pipeline"],
        )
        mock_run_drp_substep.assert_called_once_with(
            stage,
            "step2d",
            custom_pipeline=step_config["custom_pipeline"],
            config_overrides=step_config["config_overrides"],
            custom_data_query=step_config["data_query"],
        )


@patch("stages.base.get_logger", autospec=True)
@patch.object(DRPStage, "build_drp_substep_graph", autospec=True)
@patch.object(DRPStage, "run_drp_substep", autospec=True)
class TestDRPStageRunFinalVisitSummary:
    def test_calls_required_sub_steps_if_enabled(
        self,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        step_config = stage.config["run_final_visit_summary"]
        step_config["enabled"] = True
        stage.configure_logger()
        stage.run_final_visit_summary()
        mock_build_drp_substep_graph.assert_called_once_with(
            stage,
            "step2e",
        )
        mock_run_drp_substep.assert_called_once_with(
            stage,
            "step2e",
            custom_data_query=step_config["data_query"],
        )

    def test_does_not_call_substeps_if_not_enabled(
        self,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_get_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        stage.config["run_final_visit_summary"]["enabled"] = False
        stage.configure_logger()
        stage.run_final_visit_summary()
        mock_build_drp_substep_graph.assert_not_called()
        mock_run_drp_substep.assert_not_called()


@patch("stages.base.get_logger", autospec=True)
@patch.object(DRPStage, "build_drp_substep_graph", autospec=True)
@patch.object(DRPStage, "run_drp_substep", autospec=True)
@patch.object(DRPStage, "run_pipeline_commands", autospec=True)
class TestDRPStageRunCoaddProcessing:
    def test_calls_required_sub_steps(
        self,
        mock_run_pipeline_commands,
        mock_run_drp_substep,
        mock_build_drp_substep_graph,
        mock_logger,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        mock_run_pipeline_commands.return_value = [
            FakeCompletedProcess(
                "cmd",
                b"[4321, 1234, 5678, 8765]",
            )
        ]
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        step_config = stage.config["run_coadd_processing"]
        stage.configure_logger()
        stage.run_coadd_processing()
        mock_run_pipeline_commands.assert_called_once()
        (find_tracts_cmd,) = mock_run_pipeline_commands.call_args.args[1]
        assert "python config/find_tracts.py" in find_tracts_cmd
        mock_build_drp_substep_graph.assert_called_once_with(
            stage,
            "step3",
            custom_pipeline=step_config["custom_pipeline"],
        )
        mock_run_drp_substep.assert_called_once_with(
            stage,
            "step3",
            custom_pipeline=step_config["custom_pipeline"],
            config_override_files=step_config["config_override_files"],
            custom_data_query="skymap='decam_rings_v1' AND tract=4321",
        )


@patch.object(DRPStage, "run_pipeline_commands", autospec=True)
class TestDRPStageBuildDrpSubstepGraph:
    def test_raises_when_passing_invalid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        with pytest.raises(Exception) as err:
            stage.build_drp_substep_graph("invalid_substep")
        assert "Invalid substep" in str(err.value)

    def test_calls_pipetask_commands_with_defaults_when_passing_only_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        stage.build_drp_substep_graph("step1")
        mock_run_pipeline_commands.assert_called_once()
        show_pipe_cmd, gen_dot_cmd, export_pdf_cmd = (
            mock_run_pipeline_commands.call_args.args[1]
        )
        assert "pipetask build" in show_pipe_cmd
        assert (
            "-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step1 --show pipeline"
            in show_pipe_cmd
        )
        assert "pipetask build" in gen_dot_cmd
        assert (
            "-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step1 "
            "--pipeline-dot /tmp/test-decam-pipeline.dot" in gen_dot_cmd
        )
        assert "dot /tmp/test-decam-pipeline.dot -Tpdf" in export_pdf_cmd
        assert "pipeline_step1.pdf" in export_pdf_cmd

    def test_calls_pipetask_commands_with_custom_pipeline_file_and_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        pipeline_file = "config/some_custom_pipeline.yaml"
        stage.build_drp_substep_graph("step2a", custom_pipeline=pipeline_file)
        mock_run_pipeline_commands.assert_called_once()
        show_pipe_cmd, gen_dot_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert (
            "-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step2a --show pipeline"
            in show_pipe_cmd
        )
        assert (
            "-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step2a" not in gen_dot_cmd
        )
        assert f"-p {pipeline_file}" in gen_dot_cmd


@patch.object(DRPStage, "run_pipeline_commands", autospec=True)
class TestDRPStageRunDrpSubstep:
    def test_raises_when_passing_invalid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        with pytest.raises(Exception) as err:
            stage.run_drp_substep("invalid_substep")
        assert "Invalid substep" in str(err.value)

    def test_calls_pipetask_commands_with_defaults_when_passing_only_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        stage.run_drp_substep("step1")
        mock_run_pipeline_commands.assert_called_once()
        _, pipe_run_substep_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert "pipetask --long-log run" in pipe_run_substep_cmd
        assert "--instrument lsst.obs.decam.DarkEnergyCamera" in pipe_run_substep_cmd
        assert "-i DECam/defaults" in pipe_run_substep_cmd
        assert f"-o {stage.OUTPUT}" in pipe_run_substep_cmd
        assert "-c" not in pipe_run_substep_cmd
        assert "-C" not in pipe_run_substep_cmd
        assert (
            "-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step1"
            in pipe_run_substep_cmd
        )
        assert "-d \"instrument='DECam'\"" in pipe_run_substep_cmd

    def test_calls_pipetask_commands_with_custom_data_query_and_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        data_query = "detector=62"
        stage.run_drp_substep("step1", custom_data_query=data_query)
        mock_run_pipeline_commands.assert_called_once()
        _, pipe_run_substep_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert f"-d \"instrument='DECam' AND {data_query}\"" in pipe_run_substep_cmd

    def test_calls_pipetask_commands_with_single_config_override_file_and_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        config_override_file = "task_label:config/config_file.py"
        stage.run_drp_substep("step1", config_override_files=[config_override_file])
        mock_run_pipeline_commands.assert_called_once()
        _, pipe_run_substep_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert f"-C {config_override_file}" in pipe_run_substep_cmd

    def test_calls_pipetask_commands_with_multi_config_override_files_and_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        config_override_files = [
            "task_label1:config/config_file1.py",
            "task_label2:config/config_file2.py",
        ]
        stage.run_drp_substep("step1", config_override_files=config_override_files)
        mock_run_pipeline_commands.assert_called_once()
        _, pipe_run_substep_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert (
            f"-C {config_override_files[0]} -C {config_override_files[1]}"
            in pipe_run_substep_cmd
        )

    def test_calls_pipetask_commands_with_single_config_override_and_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        config_override = "task_label:config_key='config_value'"
        stage.run_drp_substep("step1", config_overrides=[config_override])
        mock_run_pipeline_commands.assert_called_once()
        _, pipe_run_substep_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert f"-c {config_override}" in pipe_run_substep_cmd

    def test_calls_pipetask_commands_with_multi_config_overrides_and_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        config_overrides = [
            "task_label1:config_key1='config_value1'",
            "task_label2:config_key2='config_value2'",
        ]
        stage.run_drp_substep("step1", config_overrides=config_overrides)
        mock_run_pipeline_commands.assert_called_once()
        _, pipe_run_substep_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert (
            f"-c {config_overrides[0]} -c {config_overrides[1]}" in pipe_run_substep_cmd
        )

    def test_calls_pipetask_commands_with_custom_pipeline_file_and_valid_substep(
        self,
        mock_run_pipeline_commands,
        mock_base_stage_scipipe_connector,
        mock_pipeline_gcs_connector,
    ):
        pipeline = Pipeline("test-decam")
        stage = DRPStage(pipeline)
        pipeline_file = "config/some_custom_pipeline.yaml"
        stage.run_drp_substep("step1", custom_pipeline=pipeline_file)
        mock_run_pipeline_commands.assert_called_once()
        _, pipe_run_substep_cmd, _ = mock_run_pipeline_commands.call_args.args[1]
        assert (
            "-p $DRP_PIPE_DIR/pipelines/DECam/DRP-Merian.yaml#step1"
            not in pipe_run_substep_cmd
        )
        assert f"-p {pipeline_file}" in pipe_run_substep_cmd
