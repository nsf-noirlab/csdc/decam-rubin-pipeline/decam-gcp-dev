import os
from unittest.mock import patch
from fastapi.testclient import TestClient

from manager.adapters.archive import AstroDataArchiveConnector
from manager.adapters.database import DatabaseConnector
from manager.adapters.kafka import KafkaConnector
from manager.adapters.gcs import GcsConnector
from manager.adapters.scipipe import LsstSciPipeConnector
from manager.main import app, check_adapters

client = TestClient(app)


@patch("manager.main.check_adapters", autospec=True)
class TestReadRoot:
    def test_get_read_root(self, mock_check_adapters):
        mock_check_adapters.return_value = {"adapters": []}
        response = client.get("/")
        assert response.status_code == 200
        assert response.json()["adapters"] == []


@patch("manager.main.BackgroundExecutor", autospec=True)
@patch("manager.main.Pipeline", autospec=True)
class TestCreatePipeline:
    def test_post_successful_creation(
        self,
        mock_pipeline,
        mock_background_executor,
    ):
        instance = mock_pipeline.return_value
        instance.current_stage.name = "Name"
        instance.current_stage_status = "Status"
        response = client.post("/pipelines")
        response_json_keys = response.json().keys()
        assert response.status_code == 201
        assert "name" in response_json_keys
        assert "created_at" in response_json_keys
        assert "current_stage" in response_json_keys
        assert "current_status" in response_json_keys

    def test_post_unsuccessful_creation_returns_server_error(
        self,
        mock_pipeline,
        mock_background_executor,
    ):
        error_msg = "Error on configuring preparation stage"
        instance = mock_background_executor.return_value
        instance.trigger.side_effect = Exception(error_msg)
        response = client.post("/pipelines")
        assert response.status_code == 500
        assert response.json()["detail"] == error_msg


@patch("manager.main.Pipeline", autospec=True)
class TestGetPipeline:
    # TODO: update this test when temporary response is removed
    def test_get_pipeline_returns_temporary_response(
        self,
        mock_pipeline,
    ):
        pipeline_name = "test-pipeline"
        pipeline_instance = mock_pipeline.return_value
        pipeline_instance.is_new.return_value = False
        pipeline_instance.path = "data"
        pipeline_instance._home_dir = "/home"
        response = client.get(f"/pipelines/{pipeline_name}")
        response_json_keys = response.json().keys()
        assert response.status_code == 200
        assert os.path.isfile("data/gcs_sync.txt")
        assert "executions" in response_json_keys
        os.remove("data/gcs_sync.txt")

    def test_get_pipeline_not_found_returns_server_error(
        self,
        mock_pipeline,
    ):
        pipeline_instance = mock_pipeline.return_value
        pipeline_instance.is_new.return_value = True
        response = client.get("/pipelines/test-pipeline")
        assert response.status_code == 404
        assert "not found" in response.json()["detail"]


@patch("manager.main.BackgroundExecutor", autospec=True)
@patch("manager.main.Pipeline", autospec=True)
class TestContinuePipeline:
    def test_put_successful_pipeline_normal_resumption(
        self,
        mock_pipeline,
        mock_background_executor,
    ):
        pipeline_instance = mock_pipeline.return_value
        pipeline_instance.is_new.return_value = False
        pipeline_instance.name = "test-pipeline"
        pipeline_instance.current_stage.name = "Name"
        pipeline_instance.current_stage_status = "Status"
        response = client.put("/pipelines/test-pipeline")
        response_json_keys = response.json().keys()
        pipeline_instance.init_stage_to_attempt.assert_not_called()
        assert response.status_code == 200
        assert "name" in response_json_keys
        assert "created_at" in response_json_keys
        assert "current_stage" in response_json_keys
        assert "current_status" in response_json_keys

    def test_put_successful_pipeline_custom_resumption(
        self,
        mock_pipeline,
        mock_background_executor,
    ):
        pipeline_instance = mock_pipeline.return_value
        pipeline_instance.is_new.return_value = False
        pipeline_instance.name = "test-pipeline"
        pipeline_instance.current_stage.name = "Name"
        pipeline_instance.current_stage_status = "Status"
        data = {"stage": "CustomStage"}
        response = client.put("/pipelines/test-pipeline", json=data)
        response_json_keys = response.json().keys()
        pipeline_instance.init_stage_to_attempt.assert_called_once()
        assert response.status_code == 200
        assert "name" in response_json_keys
        assert "created_at" in response_json_keys
        assert "current_stage" in response_json_keys
        assert "current_status" in response_json_keys

    def test_put_pipeline_not_found_returns_server_error(
        self,
        mock_pipeline,
        mock_background_executor,
    ):
        pipeline_instance = mock_pipeline.return_value
        pipeline_instance.is_new.return_value = True
        response = client.put("/pipelines/test-pipeline")
        assert response.status_code == 404
        assert "not found" in response.json()["detail"]

    def test_put_unsuccessful_pipeline_resumption_returns_server_error(
        self,
        mock_pipeline,
        mock_background_executor,
    ):
        pipeline_instance = mock_pipeline.return_value
        pipeline_instance.is_new.return_value = False
        error_msg = "Error on configuring next stage"
        instance = mock_background_executor.return_value
        instance.trigger.side_effect = Exception(error_msg)
        response = client.put("/pipelines/test-pipeline")
        assert response.status_code == 500
        assert response.json()["detail"] == error_msg


@patch.object(LsstSciPipeConnector, "check_connection", autospec=True)
@patch.object(GcsConnector, "check_connection", autospec=True)
@patch.object(KafkaConnector, "check_connection", autospec=True)
@patch.object(DatabaseConnector, "check_connection", autospec=True)
@patch.object(AstroDataArchiveConnector, "check_connection", autospec=True)
@patch.object(LsstSciPipeConnector, "__init__", autospec=True)
@patch.object(GcsConnector, "__init__", autospec=True)
@patch.object(KafkaConnector, "__init__", autospec=True)
@patch.object(DatabaseConnector, "__init__", autospec=True)
@patch.object(AstroDataArchiveConnector, "__init__", autospec=True)
class TestCheckAdapters:
    def test_returns_connection_result(
        self,
        mock_archive_connector,
        mock_database_connector,
        mock_kafka_connector,
        mock_gcs_connector,
        mock_lsst_scipipe_connector,
        mock_archive_check_connection,
        mock_database_check_connection,
        mock_kafka_check_connection,
        mock_gcs_check_connection,
        mock_lsst_scipipe_check_connection,
    ):
        connection_results = {
            "LSST Science Pipelines": "LSST SciPipe success",
            "Google Cloud Storage": "GCS success",
            "Cloud SQL (or local database)": "Database success",
            "Kafka": "Kafka failure",
            "Astro Data Archive": "Archive success",
        }
        mock_archive_connector.return_value = None
        mock_database_connector.return_value = None
        mock_kafka_connector.return_value = None
        mock_gcs_connector.return_value = None
        mock_lsst_scipipe_connector.return_value = None
        mock_archive_check_connection.return_value = connection_results[
            "Astro Data Archive"
        ]
        mock_database_check_connection.return_value = connection_results[
            "Cloud SQL (or local database)"
        ]
        mock_kafka_check_connection.return_value = connection_results["Kafka"]
        mock_gcs_check_connection.return_value = connection_results[
            "Google Cloud Storage"
        ]
        mock_lsst_scipipe_check_connection.return_value = connection_results[
            "LSST Science Pipelines"
        ]

        adapters_result = check_adapters()
        assert adapters_result.get("adapters") is not None
        assert [adapter["name"] for adapter in adapters_result["adapters"]] == list(
            connection_results.keys()
        )
        for adapter in adapters_result["adapters"]:
            assert adapter["result"] == connection_results[adapter["name"]]
