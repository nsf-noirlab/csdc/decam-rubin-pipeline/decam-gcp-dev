import re
import requests

from manager.adapters.archive import (
    AstroDataArchiveConnector,
    ASTRO_DATA_ARCHIVE_BASE_URL,
)


class TestAstroDataArchiveConnectorCheckConnection:
    def test_connection_when_archive_request_times_out(
        self,
        requests_mock,
    ):
        matcher = re.compile(f"^{ASTRO_DATA_ARCHIVE_BASE_URL}")
        obj_name = "REAL_OBJECT"
        error_msg = "Read timed out"
        requests_mock.get(matcher, exc=requests.exceptions.ReadTimeout(error_msg))
        archive_connector = AstroDataArchiveConnector(obj_name)
        result = archive_connector.check_connection()
        assert "Connection to Astro Data Archive could not be established" in result
        assert error_msg in result

    def test_connection_when_archive_request_returns_error(
        self,
        requests_mock,
    ):
        matcher = re.compile(f"^{ASTRO_DATA_ARCHIVE_BASE_URL}")
        obj_name = "FAKE_OBJECT"
        error_msg = f"Unable to find coordinates for name '{obj_name}' using https://cds.unistra.fr/cgi-bin/nph-sesame/A?{obj_name}"
        requests_mock.get(matcher, status_code=500, json={"errorMessage": error_msg})
        archive_connector = AstroDataArchiveConnector(obj_name)
        result = archive_connector.check_connection()
        assert "Connection to Astro Data Archive with errors" in result
        assert error_msg in result

    def test_connection_when_archive_request_returns_successful_response(
        self,
        requests_mock,
    ):
        matcher = re.compile(f"^{ASTRO_DATA_ARCHIVE_BASE_URL}")
        obj_name = "REAL_OBJECT"
        coordinates = {"ra": 204.25383, "dec": -29.86576111}
        requests_mock.get(matcher, status_code=200, json=coordinates)
        archive_connector = AstroDataArchiveConnector(obj_name)
        result = archive_connector.check_connection()
        assert "Connection to Astro Data Archive successful" in result
        assert f"Coordinates: {coordinates}" in result
