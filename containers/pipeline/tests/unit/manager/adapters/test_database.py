import os
import re
from unittest.mock import patch
import sqlalchemy

from tests.fakes import (
    FakeSqlAlchemyEngine,
    FakeSqlAlchemyMetadata,
    FakeSqlAlchemyResult,
)
from manager.adapters.database import DatabaseConnector


@patch.object(DatabaseConnector, "connect_unix_socket", autospec=True)
@patch.object(DatabaseConnector, "connect_standard", autospec=True)
class TestDatabaseConnector:
    @patch.dict(
        os.environ, {"INSTANCE_UNIX_SOCKET": "/cloudsql/project:region:instance"}
    )
    def test_init_defines_unix_socket_engine_when_env_var_passed(
        self,
        mock_connect_standard,
        mock_connect_unix_socket,
    ):
        DatabaseConnector()
        mock_connect_unix_socket.assert_called_once()
        mock_connect_standard.assert_not_called()

    def test_init_defines_standard_engine_when_env_var_not_passed(
        self,
        mock_connect_standard,
        mock_connect_unix_socket,
    ):
        DatabaseConnector()
        mock_connect_unix_socket.assert_not_called()
        mock_connect_standard.assert_called_once()


@patch.object(DatabaseConnector, "connect_standard", autospec=True)
class TestDatabaseConnectorProperties:
    @patch.dict(os.environ, {"DB_NAME": "test_db_name"})
    def test_db_name_when_not_initializing_with_db_name(self, mock_connect_standard):
        db_connector = DatabaseConnector()
        assert db_connector.db_name == "test_db_name"

    def test_db_name_when_initializing_with_db_name(self, mock_connect_standard):
        custom_db_name = "custom_db_name"
        db_connector = DatabaseConnector(db_name=custom_db_name)
        assert db_connector.db_name == custom_db_name


@patch.object(DatabaseConnector, "connect_standard", autospec=True)
class TestDatabaseConnectorGetEngine:
    def test_returns_engine(self, mock_connect_standard):
        db_url = "postgresql+pg8000://db_user:db_password@host/db_name"
        mock_connect_standard.return_value = FakeSqlAlchemyEngine(db_url)
        db_connector = DatabaseConnector()
        assert str(db_connector.get_engine().url) == db_url


@patch.object(DatabaseConnector, "connect_standard", autospec=True)
@patch.object(DatabaseConnector, "init_db", autospec=True)
class TestDatabaseConnectorCheckConnection:
    def test_database_not_reachable_avoids_connection(
        self,
        mock_init_db,
        mock_connect_standard,
    ):
        mock_connect_standard.return_value = FakeSqlAlchemyEngine()
        with patch.object(FakeSqlAlchemyEngine, "connect") as mock_connect:
            error_msg = "Can't create a connection to host and port"
            mock_connect.side_effect = sqlalchemy.exc.InterfaceError(error_msg, {}, "")
            db_connector = DatabaseConnector()
            result = db_connector.check_connection()
            assert re.match(r"Connection to database '\w+' failed", result)
            assert error_msg in result

    def test_invalid_db_credentials_avoid_db_init(
        self,
        mock_init_db,
        mock_connect_standard,
    ):
        mock_connect_standard.return_value = FakeSqlAlchemyEngine()
        error_msg = "password authentication failed for user"
        mock_init_db.side_effect = sqlalchemy.exc.ProgrammingError(error_msg, {}, "")
        db_connector = DatabaseConnector()
        result = db_connector.check_connection()
        assert re.match(r"Connection to database '\w+' failed", result)
        assert error_msg in result

    def test_invalid_db_credentials_avoid_connection(
        self,
        mock_init_db,
        mock_connect_standard,
    ):
        mock_connect_standard.return_value = FakeSqlAlchemyEngine()
        with patch.object(FakeSqlAlchemyEngine, "connect") as mock_connect:
            error_msg = "password authentication failed for user"
            mock_connect.side_effect = sqlalchemy.exc.ProgrammingError(
                error_msg, {}, ""
            )
            db_connector = DatabaseConnector()
            result = db_connector.check_connection()
            assert re.match(r"Connection to database '\w+' failed", result)
            assert error_msg in result

    @patch.object(sqlalchemy, "MetaData", autospec=True)
    def test_valid_db_credentials_returns_success_message_with_empty_tables(
        self,
        mock_metadata,
        mock_init_db,
        mock_connect_standard,
    ):
        mock_metadata.return_value = FakeSqlAlchemyMetadata({})
        mock_connect_standard.return_value = FakeSqlAlchemyEngine()
        with patch.object(FakeSqlAlchemyResult, "all") as mock_result_all:
            select_msg = "hello world"
            mock_result_all.return_value = [(select_msg,)]
            db_connector = DatabaseConnector()
            result = db_connector.check_connection()
            assert re.match(r"Connection to database '\w+' successful", result)
            assert select_msg in result
            assert "Tables: []" in result

    @patch.object(sqlalchemy, "MetaData", autospec=True)
    def test_valid_db_credentials_returns_success_message_with_non_empty_tables(
        self,
        mock_metadata,
        mock_init_db,
        mock_connect_standard,
    ):
        tables = {"table1": {}, "table2": {}}
        mock_metadata.return_value = FakeSqlAlchemyMetadata(tables)
        mock_connect_standard.return_value = FakeSqlAlchemyEngine()
        with patch.object(FakeSqlAlchemyResult, "all") as mock_result_all:
            select_msg = "hello world"
            mock_result_all.return_value = [(select_msg,)]
            db_connector = DatabaseConnector()
            result = db_connector.check_connection()
            assert re.match(r"Connection to database '\w+' successful", result)
            assert select_msg in result
            assert f"Tables: [{', '.join(tables.keys())}]" in result


@patch.object(DatabaseConnector, "connect_standard", autospec=True)
@patch("manager.adapters.database.database_exists", autospec=True)
@patch("manager.adapters.database.create_database", autospec=True)
class TestDatabaseConnectorInitDb:
    def test_creates_database_when_does_not_exist(
        self,
        mock_create_database,
        mock_database_exists,
        mock_connect_standard,
    ):
        mock_database_exists.return_value = False
        db_connector = DatabaseConnector()
        db_connector.init_db()
        mock_create_database.assert_called_once()

    def test_skips_database_creation_when_already_exists(
        self,
        mock_create_database,
        mock_database_exists,
        mock_connect_standard,
    ):
        mock_database_exists.return_value = True
        db_connector = DatabaseConnector()
        db_connector.init_db()
        mock_create_database.assert_not_called()

    def test_creates_extensions_when_required(
        self,
        mock_create_database,
        mock_database_exists,
        mock_connect_standard,
    ):
        mock_database_exists.return_value = True
        db_connector = DatabaseConnector()
        # mock_conn = mock_connect_standard.return_value.connect.return_value
        db_connector.init_db(extensions_required=True)
        # TODO: The following should work but it's not. Fix it
        # mock_conn.execute.assert_called_once()
        # mock_conn.commit.assert_called_once()


@patch.object(sqlalchemy, "create_engine", autospec=True)
class TestDatabaseConnectorConnectStandard:
    def test_returns_engine_with_expected_url(self, mock_create_engine):
        db_url = "postgresql+pg8000://db_user:db_password@host/db_name"
        mock_create_engine.return_value = FakeSqlAlchemyEngine(db_url)
        db_connector = DatabaseConnector()
        engine = db_connector.connect_standard()
        assert str(engine.url) == db_url


@patch.object(sqlalchemy, "create_engine", autospec=True)
@patch.dict(os.environ, {"INSTANCE_UNIX_SOCKET": "/cloudsql/project:region:instance"})
class TestDatabaseConnectorConnectUnixSocket:
    def test_returns_engine_with_expected_url(self, mock_create_engine):
        db_url = "postgresql+pg8000://db_user:db_password@/db_name?unix_sock"
        mock_create_engine.return_value = FakeSqlAlchemyEngine(db_url)
        db_connector = DatabaseConnector()
        engine = db_connector.connect_unix_socket()
        assert str(engine.url) == db_url
