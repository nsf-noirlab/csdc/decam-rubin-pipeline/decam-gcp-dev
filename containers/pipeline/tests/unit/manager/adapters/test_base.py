from unittest.mock import patch

import pytest

from manager.adapters.base import AbstractConnector

abstractmethods = "manager.adapters.base.AbstractConnector.__abstractmethods__"


class TestAbstractConnector:
    def test_cant_instantiate_abc(self):
        with pytest.raises(TypeError) as err:
            AbstractConnector()
        assert "Can't instantiate abstract class" in str(err.value)

    @patch(abstractmethods, set())
    def test_abstract_methods(self):
        connector = AbstractConnector()
        with pytest.raises(NotImplementedError):
            connector.check_connection()
