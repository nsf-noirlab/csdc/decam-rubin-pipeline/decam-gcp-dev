import pytest
from unittest.mock import patch

from google.api_core.exceptions import NotFound
from google.auth.exceptions import DefaultCredentialsError

from tests.fakes import FakeBlob, FakeGcsClient
from manager.adapters.gcs import GcsConnector


@patch("manager.adapters.gcs.storage", autospec=True)
class TestGcsConnectorProperties:
    def test_current_error_returns_string_when_connection_to_gcs_fails(
        self,
        mock_storage,
    ):
        error_msg = "Connection error"
        mock_storage.Client.side_effect = DefaultCredentialsError(error_msg)
        gcs_connector = GcsConnector("some_bucket_name")
        assert isinstance(gcs_connector.current_error, str)
        assert error_msg in gcs_connector.current_error

    def test_current_error_returns_none_when_connection_to_gcs_is_successful(
        self,
        mock_storage,
    ):
        gcs_connector = GcsConnector("some_bucket_name")
        assert gcs_connector.current_error is None

    def test_bucket_name_returns_passed_when_instantiating(self, mock_storage):
        bucket_name = "some_bucket_name"
        gcs_connector = GcsConnector(bucket_name)
        assert gcs_connector.bucket_name == bucket_name


@patch("manager.adapters.gcs.storage", autospec=True)
class TestGcsConnectorCheckConnection:
    def test_storage_client_invalid_credentials_avoid_connection(self, mock_storage):
        error_msg = "Your default credentials were not found"
        mock_storage.Client.side_effect = DefaultCredentialsError(error_msg)
        gcs_connector = GcsConnector("some_bucket_name")
        result = gcs_connector.check_connection()
        assert error_msg in result

    @patch.object(GcsConnector, "list_bucket_files", autospec=True)
    def test_nonexistent_bucket_avoids_connection(
        self,
        mock_list_bucket_files,
        mock_storage,
    ):
        error_msg = "The specified bucket does not exist."
        mock_list_bucket_files.side_effect = NotFound(error_msg)
        gcs_connector = GcsConnector("some_bucket_name")
        result = gcs_connector.check_connection()
        assert error_msg in result

    @patch.object(GcsConnector, "list_bucket_files", autospec=True)
    def test_successful_connection_to_bucket_returns_empty_blobs_list(
        self,
        mock_list_bucket_files,
        mock_storage,
    ):
        mock_list_bucket_files.return_value = []
        gcs_connector = GcsConnector("some_bucket_name")
        result = gcs_connector.check_connection()
        assert "Bucket files: (empty)" in result

    @patch.object(GcsConnector, "list_bucket_files", autospec=True)
    def test_successful_connection_to_bucket_returns_non_empty_blobs_list(
        self,
        mock_list_bucket_files,
        mock_storage,
    ):
        blobs_list = ["blob1", "blob2"]
        mock_list_bucket_files.return_value = blobs_list
        gcs_connector = GcsConnector("some_bucket_name")
        result = gcs_connector.check_connection()
        assert "Bucket files" in result
        for blob in blobs_list:
            assert blob in result


@patch("manager.adapters.gcs.storage", autospec=True)
class TestGcsConnectorListBucketFiles:
    def test_nonexistent_bucket_raises_exception(self, mock_storage):
        error_msg = "The specified bucket does not exist."
        with patch.object(FakeGcsClient, "list_blobs") as mock_list_blobs:
            mock_list_blobs.side_effect = NotFound(error_msg)
            mock_storage.Client.return_value = FakeGcsClient()
            gcs_connector = GcsConnector("some_bucket_name")
            with pytest.raises(NotFound) as err:
                gcs_connector.list_bucket_files()
            assert error_msg in str(err.value)

    def test_existent_bucket_with_no_files_returns_empty_list(self, mock_storage):
        with patch.object(FakeGcsClient, "list_blobs") as mock_list_blobs:
            mock_list_blobs.return_value = []
            mock_storage.Client.return_value = FakeGcsClient()
            gcs_connector = GcsConnector("some_bucket_name")
            result_list = gcs_connector.list_bucket_files()
            assert result_list == []

    def test_existent_bucket_with_files_returns_list_with_filenames(self, mock_storage):
        blobs_list = ["blob1", "blob2"]
        fake_blobs_list = [FakeBlob(blob) for blob in blobs_list]
        with patch.object(FakeGcsClient, "list_blobs") as mock_list_blobs:
            mock_list_blobs.return_value = fake_blobs_list
            mock_storage.Client.return_value = FakeGcsClient()
            gcs_connector = GcsConnector("some_bucket_name")
            result_list = gcs_connector.list_bucket_files()
            assert len(blobs_list) == len(result_list)
            for blob in blobs_list:
                assert blob in result_list


@patch("manager.adapters.gcs.storage", autospec=True)
@patch("manager.adapters.gcs.transfer_manager", autospec=True)
class TestGcsConnectorDownloadManyToPath:
    def test_transfer_manager_downloads_passed_blob_names_from_bucket_to_destination(
        self,
        mock_transfer_manager,
        mock_storage,
    ):
        mock_client = mock_storage.Client.return_value
        bucket_name = "some_bucket_name"
        blob_names = [
            "123456.fits",
            "some_non_fits_file.jpg",
            "654321.fits",
        ]
        dest_dir = "some_dir"
        gcs_connector = GcsConnector(bucket_name)
        gcs_connector.download_many_to_path(blob_names, dest_dir)
        download_call_args = mock_transfer_manager.download_many_to_path.call_args
        mock_client.bucket.assert_called_with(bucket_name)
        assert download_call_args.args[0] == mock_client.bucket.return_value
        assert download_call_args.args[1] == blob_names
        assert download_call_args.kwargs["destination_directory"] == dest_dir

    def test_returns_successful_and_failed_downloads_as_list_of_tuples(
        self,
        mock_transfer_manager,
        mock_storage,
    ):
        blob_names = [
            "123456.fits",
            "some_non_fits_file.jpg",
            "654321.fits",
        ]
        downloaded_results = [
            None,
            Exception("Network error"),
            None,
        ]
        mock_transfer_manager.download_many_to_path.return_value = downloaded_results
        gcs_connector = GcsConnector("some_bucket_name")
        downloaded_list = gcs_connector.download_many_to_path(blob_names, "some_dir")
        for i, (name, result) in enumerate(downloaded_list):
            assert name == blob_names[i]
            assert result == downloaded_results[i]


@patch("manager.adapters.gcs.storage", autospec=True)
@patch("manager.adapters.gcs.transfer_manager", autospec=True)
class TestGcsConnectorUploadMany:
    def test_transfer_manager_uploads_passed_filenames_from_source_to_bucket(
        self,
        mock_transfer_manager,
        mock_storage,
    ):
        mock_client = mock_storage.Client.return_value
        bucket_name = "some_bucket_name"
        filenames = [
            "some_log.log",
            "some_jpg.jpg",
            "some_fits.fits",
        ]
        source_dir = "some_dir"
        blob_name_prefix = "some_prefix/"
        gcs_connector = GcsConnector(bucket_name)
        gcs_connector.upload_many(
            filenames,
            source_dir,
            blob_name_prefix=blob_name_prefix,
        )
        upload_call_args = mock_transfer_manager.upload_many_from_filenames.call_args
        mock_client.bucket.assert_called_with(bucket_name)
        assert upload_call_args.args[0] == mock_client.bucket.return_value
        assert upload_call_args.args[1] == filenames
        assert upload_call_args.kwargs["blob_name_prefix"] == blob_name_prefix
        assert upload_call_args.kwargs["source_directory"] == source_dir

    def test_returns_successful_and_failed_downloads_as_list_of_tuples(
        self,
        mock_transfer_manager,
        mock_storage,
    ):
        filenames = [
            "some_log.log",
            "some_jpg.jpg",
            "some_fits.fits",
        ]
        uploaded_results = [
            None,
            None,
            Exception("Network error"),
        ]
        mock_transfer_manager.upload_many_from_filenames.return_value = uploaded_results
        gcs_connector = GcsConnector("some_bucket_name")
        uploaded_list = gcs_connector.upload_many(filenames, "some_dir")
        for i, (name, result) in enumerate(uploaded_list):
            assert name == filenames[i]
            assert result == uploaded_results[i]
