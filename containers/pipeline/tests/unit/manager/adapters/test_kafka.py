import os
from unittest.mock import patch

from confluent_kafka import KafkaException

from tests.fakes import (
    FakeKafkaClusterMetadata,
    FakeKafkaTopicMetadata,
)
from manager.adapters.kafka import KafkaConnector


@patch("manager.adapters.kafka.Producer", autospec=True)
class TestKafkaConnectorInit:
    def test_init_creates_producer_using_envvar(self, mock_producer):
        kafka_server = os.environ["KAFKA_SERVER"]
        KafkaConnector()
        kafka_config = mock_producer.call_args[0][0]
        assert "bootstrap.servers" in kafka_config
        assert kafka_config["bootstrap.servers"] == kafka_server


@patch.object(KafkaConnector, "list_topics", autospec=True)
class TestKafkaConnectorCheckConnection:
    def test_broken_connection_returns_error_msg(self, mock_list_topics):
        error_msg = "Broker transport failure"
        mock_list_topics.side_effect = KafkaException(error_msg)
        kafka_connector = KafkaConnector()
        result = kafka_connector.check_connection()
        assert error_msg in result

    def test_successful_connection_returns_topics(self, mock_list_topics):
        topics_list = ["topic1", "topic2"]
        mock_list_topics.return_value = topics_list
        kafka_connector = KafkaConnector()
        result = kafka_connector.check_connection()
        assert "Topics" in result
        for topic in topics_list:
            assert topic in result


@patch("manager.adapters.kafka.Producer", autospec=True)
class TestKafkaConnectorListTopics:
    def test_cluster_with_no_topics_returns_empty_list(self, mock_producer):
        producer = mock_producer.return_value
        producer.list_topics.return_value = FakeKafkaClusterMetadata()
        kafka_connector = KafkaConnector()
        result_list = kafka_connector.list_topics()
        assert result_list == []

    def test_cluster_with_topics_returns_topics_names(self, mock_producer):
        topics_list = ["topic1", "topic2"]
        producer = mock_producer.return_value
        producer.list_topics.return_value = FakeKafkaClusterMetadata(
            {topic: FakeKafkaTopicMetadata() for topic in topics_list}
        )
        kafka_connector = KafkaConnector()
        result_list = kafka_connector.list_topics()
        assert len(topics_list) == len(result_list)
        for topic in topics_list:
            assert topic in result_list
