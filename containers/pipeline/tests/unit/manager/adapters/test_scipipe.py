import subprocess
from unittest.mock import patch

from tests.fakes import FakeCompletedProcess
from manager.adapters.scipipe import LsstSciPipeConnector


class TestLsstSciPipeConnectorBuildCmd:
    def test_returns_command_with_setup_prefix(self):
        cmd = "sample_cmd"
        scipipe_connector = LsstSciPipeConnector()
        result = scipipe_connector.build_cmd(cmd)
        assert (
            result
            == f"source /opt/lsst/software/stack/loadLSST.bash && setup lsst_distrib && {cmd}"
        )


@patch.object(subprocess, "run", autospec=True)
class TestLsstSciPipeConnectorCheckConnection:
    def test_subprocess_successful_returns_stdout_text(self, mock_subprocess_run):
        expected_output = "Some output\nincluding new lines"
        mock_subprocess_run.return_value = FakeCompletedProcess(
            "some command",
            expected_output.encode(),
        )
        scipipe_connector = LsstSciPipeConnector()
        result = scipipe_connector.check_connection()
        assert "Connection to LSST Science Pipelines package successful" in result
        assert expected_output in result

    def test_subprocess_not_successful_returns_error_msg(self, mock_subprocess_run):
        error_msg = "Some error description"
        mock_subprocess_run.side_effect = Exception(error_msg)
        scipipe_connector = LsstSciPipeConnector()
        result = scipipe_connector.check_connection()
        assert "Connection to LSST Science Pipelines package failed" in result
        assert error_msg in result
