import argparse
import lsst.daf.butler as dafButler

def print_flat_certified_date_ranges(repo: str):
    butler = dafButler.Butler(repo)
    qda = butler.registry.queryDatasetAssociations
    coll = "DECam/calib"
    flats = [x for x in qda("flat", collections=coll) if x.ref.dataId["detector"] == 1]
    print(flats)
    print(f"{flats[0].timespan.begin.value = }")
    print(f"{flats[0].timespan.end.value = }")

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description="Prints flat certified date ranges querying a Butler repository"
    )
    argparser.add_argument(
        "--repo",
        help="Butler repository to query from",
        required=True,
    )
    args = argparser.parse_args()
    print_flat_certified_date_ranges(args.repo)
