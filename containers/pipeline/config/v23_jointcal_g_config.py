config.astrometryRefObjLoader.ref_dataset_name='gaia_dr2_GW170817'
config.photometryRefObjLoader.ref_dataset_name='ps1_dr1_GW170817'
# These are the Gen3 configuration options for reference catalog name
config.connections.astrometryRefCat = "gaia_dr2_GW170817"
config.connections.photometryRefCat = "ps1_dr1_GW170817"

config.astrometryRefObjLoader.anyFilterMapsToThis=None
config.astrometryRefObjLoader.filterMap = {
#    'u':'phot_g_mean_mag',
    'g':'phot_g_mean_mag',
#    'r':'phot_g_mean_mag',
#    'i':'phot_g_mean_mag',
#    'z':'phot_g_mean_mag',
#    'r':'r_psf',
}

config.photometryRefObjLoader.filterMap = {
#    'u':'v_psf',
#    'g':'g_psf',
    'g':'gmag',
#    'r':'r_psf',
#    'r':'wavg_mag_psf_r',
#    'i':'wavg_mag_psf_i',
#    'z':'wavg_mag_psf_z',
}

#config.applyColorTerms = False
config.photometryVisitOrder = 2


config.sourceFluxType='apFlux_12_0'
#config.sourceFluxType='calib'
config.sourceSelector['science'].fluxLimit.fluxField='apFlux_12_0_instFlux'
config.sourceSelector['science'].signalToNoise.fluxField='apFlux_12_0_instFlux'
config.sourceSelector['science'].signalToNoise.errField='apFlux_12_0_instFluxErr'

config.sourceSelector['science'].flags.bad=['pixelFlags_edge', 'pixelFlags_saturated', 'pixelFlags_interpolatedCenter', 'psfFlux_flag', 'pixelFlags_suspectCenter']
config.sourceSelector['objectSize'].badFlags=['pixelFlags_edge', 'pixelFlags_interpolatedCenter', 'pixelFlags_saturatedCenter', 'pixelFlags_crCenter', 'pixelFlags_bad', 'pixelFlags_interpolated']
config.sourceSelector['astrometry'].badFlags=['pixelFlags_edge', 'pixelFlags_interpolatedCenter', 'pixelFlags_saturatedCenter', 'pixelFlags_crCenter', 'pixelFlags_bad']

config.sourceSelector['science'].unresolved.name='extendedness'
config.sourceSelector['references'].unresolved.name='extendedness'
config.astrometryReferenceSelector.unresolved.name='extendedness'
config.photometryReferenceSelector.unresolved.name='extendedness'


config.sourceSelector['science'].isolated.parentName='parentSourceId'
# A list of column names from preSourceTable_visit:
#coord_ra
#coord_dec
#ccdVisitId
#parentSourceId
#x
#y
#xErr
#yErr
#ra
#decl
#calibFlux
#calibFluxErr
#ap03Flux
#ap03FluxErr
#ap03Flux_flag
#ap06Flux
#ap06FluxErr
#ap06Flux_flag
#ap09Flux
#ap09FluxErr
#ap09Flux_flag
#ap12Flux
#ap12FluxErr
#ap12Flux_flag
#ap17Flux
#ap17FluxErr
#ap17Flux_flag
#ap25Flux
#ap25FluxErr
#ap25Flux_flag
#ap35Flux
#ap35FluxErr
#ap35Flux_flag
#ap50Flux
#ap50FluxErr
#ap50Flux_flag
#ap70Flux
#ap70FluxErr
#ap70Flux_flag
#sky
#skyErr
#psfFlux
#psfFluxErr
#ixx
#iyy
#ixy
#ixxPSF
#iyyPSF
#ixyPSF
#gaussianFlux
#gaussianFluxErr
#extendedness
#localPhotoCalib
#localPhotoCalib_flag
#localPhotoCalibErr
#localWcs_flag
#localWcs_CDMatrix_2_1
#localWcs_CDMatrix_1_1
#localWcs_CDMatrix_1_2
#localWcs_CDMatrix_2_2
#blendedness_abs
#blendedness_flag
#blendedness_flag_noCentroid
#blendedness_flag_noShape
#apFlux_12_0_flag
#apFlux_12_0_flag_apertureTruncated
#apFlux_12_0_instFlux
#apFlux_12_0_instFluxErr
#apFlux_17_0_flag
#apFlux_17_0_instFlux
#apFlux_17_0_instFluxErr
#extendedness_flag
#footprintArea_value
#localBackground_instFlux
#localBackground_instFluxErr
#localBackground_flag
#localBackground_flag_noGoodPixels
#localBackground_flag_noPsf
#pixelFlags_bad
#pixelFlags_cr
#pixelFlags_crCenter
#pixelFlags_edge
#pixelFlags_interpolated
#pixelFlags_interpolatedCenter
#pixelFlags_offimage
#pixelFlags_saturated
#pixelFlags_saturatedCenter
#pixelFlags_suspect
#pixelFlags_suspectCenter
#psfFlux_apCorr
#psfFlux_apCorrErr
#psfFlux_area
#psfFlux_flag
#psfFlux_flag_apCorr
#psfFlux_flag_edge
#psfFlux_flag_noGoodPixels
#gaussianFlux_flag
#centroid_flag
#centroid_flag_almostNoSecondDerivative
#centroid_flag_badError
#centroid_flag_edge
#centroid_flag_noSecondDerivative
#centroid_flag_notAtMaximum
#centroid_flag_resetToPeak
#variance_flag
#variance_flag_emptyFootprint
#variance_value
#calib_astrometry_used
#calib_detected
#calib_photometry_reserved
#calib_photometry_used
#calib_psf_candidate
#calib_psf_reserved
#calib_psf_used
#deblend_deblendedAsPsf
#deblend_hasStrayFlux
#deblend_masked
#deblend_nChild
#deblend_parentTooBig
#deblend_patchedTemplate
#deblend_rampedTemplate
#deblend_skipped
#deblend_tooManyPeaks
#hsmPsfMoments_flag
#hsmPsfMoments_flag_no_pixels
#hsmPsfMoments_flag_not_contained
#hsmPsfMoments_flag_parent_source
#hsmShapeRegauss_flag
#hsmShapeRegauss_flag_galsim
#hsmShapeRegauss_flag_no_pixels
#hsmShapeRegauss_flag_not_contained
#hsmShapeRegauss_flag_parent_source
#sky_source
#detect_isPrimary
#band
#instrument
#detector
#physical_filter
#visit_system
#visit
