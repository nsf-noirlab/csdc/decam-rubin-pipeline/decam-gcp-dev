import argparse
from collections import defaultdict
import lsst.daf.butler as dafButler

def find_tract_numbers(repo: str, collection: str, **kwargs):
    butler = dafButler.Butler(repo)

    grouped_by_tract = defaultdict(set)
    extra_params = dict((k, v) for k, v in kwargs.items() if v is not None)
    
    for data_id in butler.registry.queryDataIds(
        ["tract", "visit", "detector"],
        datasets="visitSummary",
        collections=collection,
        instrument="DECam",
        skymap="decam_rings_v1",
        **extra_params,
    ):
        grouped_by_tract[data_id["tract"]].add(data_id)

    tract_numbers = list(grouped_by_tract.keys())
    print(tract_numbers)

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description="Finds and prints tract numbers covered by data"
    )
    argparser.add_argument(
        "--repo",
        help="Butler repository to query from",
        required=True,
    )
    argparser.add_argument(
        "--collection",
        help="Collection to search in",
        required=True,
    )
    argparser.add_argument(
        "--visit",
        help="Visit number to filter tracts from",
        type=int,
    )
    argparser.add_argument(
        "--detector",
        help="Detector number to filter tracts from",
        type=int,
    )
    args = argparser.parse_args()
    find_tract_numbers(
        args.repo, args.collection, detector=args.detector, visit=args.visit,
    )
