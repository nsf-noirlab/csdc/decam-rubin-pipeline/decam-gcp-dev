import argparse
import lsst.daf.butler as dafButler

def print_raw_exposure_ids(repo: str):
    butler = dafButler.Butler(repo)
    queryData = butler.registry.queryDatasets
    where = "exposure.observation_type='science' AND detector=1"
    exps = list(queryData(
        "raw",
        collections="DECam/raw/all",
        instrument="DECam",
        where=where,
    ))
    expids = tuple(x.dataId["exposure"] for x in exps)
    print(expids)

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description="Prints science exposure IDs querying a Butler repository"
    )
    argparser.add_argument(
        "--repo",
        help="Butler repository to query from",
        required=True,
    )
    args = argparser.parse_args()
    print_raw_exposure_ids(args.repo)
