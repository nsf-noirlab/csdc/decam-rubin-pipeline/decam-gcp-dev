### Configuration for task `measure'

# Mapping of camera filter name: reference catalog filter name; each reference filter must exist in the refcat. Note that this does not perform any bandpass corrections: it is just a lookup.
config.match.refObjLoader.filterMap={'g':'gmag',}

# Name of the ingested reference dataset
#config.match.refObjLoader.ref_dataset_name='ps1_dr1_GW170817'

# name for connection refCat
config.connections.refCat='ps1_dr1_GW170817'
