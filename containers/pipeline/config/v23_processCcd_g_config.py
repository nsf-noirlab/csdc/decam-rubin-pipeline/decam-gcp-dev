## Calibrate astrometry and photometry with reference catalogs
#from lsst.meas.algorithms import LoadIndexedReferenceObjectsTask
#config.calibrate.astromRefObjLoader.retarget(LoadIndexedReferenceObjectsTask)
#config.calibrate.photoRefObjLoader.retarget(LoadIndexedReferenceObjectsTask)
#
# This is the Gen2 configuration option
config.astromRefObjLoader.ref_dataset_name='gaia_dr2_GW170817'
config.photoRefObjLoader.ref_dataset_name='ps1_dr1_GW170817'
# These are the Gen3 configuration options for reference catalog name
config.connections.astromRefCat = "gaia_dr2_GW170817"
config.connections.photoRefCat = "ps1_dr1_GW170817"

config.astromRefObjLoader.filterMap = {
#    'u':'phot_g_mean_mag',
    'g':'phot_g_mean_mag',
#    'r':'phot_g_mean_mag',
#    'i':'phot_g_mean_mag',
#    'z':'phot_g_mean_mag',
#    'r':'r_psf',
}

config.photoRefObjLoader.filterMap = {
#    'u':'v_psf',
#    'g':'g_psf',
    'g':'gmag',
#    'r':'r_psf',
#    'r':'wavg_mag_psf_r',
#    'i':'wavg_mag_psf_i',
#    'z':'wavg_mag_psf_z',
}

## Use PSFEx instead of PCA for better PSF measurement
#import lsst.meas.extensions.psfex.psfexPsfDeterminer
#config.charImage.measurePsf.psfDeterminer.name='psfex'
#
## maximum number of contaminated pixels
#config.charImage.repair.cosmicray.nCrPixelMax=10000000
