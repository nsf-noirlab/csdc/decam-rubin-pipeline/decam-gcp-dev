import argparse
import lsst.daf.butler as dafButler

def print_bias_certified_date_ranges(repo: str):
    butler = dafButler.Butler(repo)
    qda = butler.registry.queryDatasetAssociations
    coll = "DECam/calib"
    biases = [x for x in qda("bias", collections=coll) if x.ref.dataId["detector"] == 1]
    print(biases)
    print(f"{biases[0].timespan.begin.value = }")
    print(f"{biases[0].timespan.end.value = }")

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description="Prints bias certified date ranges querying a Butler repository"
    )
    argparser.add_argument(
        "--repo",
        help="Butler repository to query from",
        required=True,
    )
    args = argparser.parse_args()
    print_bias_certified_date_ranges(args.repo)
