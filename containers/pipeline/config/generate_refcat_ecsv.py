import argparse
import os
import glob
from typing import List
import astropy.table

def generate_refcat_ecsv_files(refcat_dirs: List[str], outdir: str):
    # loop over each FITS file in all refcats
    # note: this constructs a series of .ecsv files, each containing two columns:
    # 1) the FITS filename, and 2) the htm7 pixel index
    for refcat_dir in refcat_dirs:

        outfile = f"{outdir}/{os.path.basename(refcat_dir)}.ecsv"
        print(f"Saving to: {outfile}")

        table = astropy.table.Table(names=("filename", "htm7"), dtype=("str", "int"))
        files = glob.glob(f"{outdir}/data/refcats/{refcat_dir}/[0-9]*.fits")

        for ii, file in enumerate(files):
            print(f"{ii}/{len(files)} ({100*ii/len(files):0.1f}%)", end="\r")
            # try/except to catch extra .fits files which may be in this dir
            try:
                file_index = int(os.path.basename(os.path.splitext(file)[0]))
            except ValueError:
                continue
            else:
                table.add_row((file, file_index))

        table.write(outfile)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser(
        description="Generate ecsv files for reference catalogs in LSST format"
    )
    argparser.add_argument(
        "--refcats",
        help="Reference catalogs directories separated by blank space",
        nargs='+',
        required=True,
    )
    argparser.add_argument(
        "--outdir",
        help="Output directory where ecsv files will be written to",
        required=True,
    )
    args = argparser.parse_args()
    generate_refcat_ecsv_files(args.refcats, args.outdir)
