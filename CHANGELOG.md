# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- New `config.yaml` file inside the `containers/pipeline/stages` module, that specifies
differences in LSST instructions for each supported `LSST_VERSION`. The stage classes
now make use of this config file to issue proper commands
- New `LSST_VERSION` environment variable that allows to specify which version of the
LSST Science Pipelines package the platform will use. Currently there are two supported
versions: `v26_0_2` (the default) and `v23_0_1`
- Application logs are now configured in the root logger, using a `StreamHandler`
  instance that has a filter attached that allows logging levels of warning and higher,
  as well as records with lower levels that include any of the stages in their messages
- Integrate `ruff` linter and formatter package to both `pipeline` and `tutorial`
  containers, using default rules
- New optional request payload parameter for the `PUT /pipelines/{pipeline_name}`
  endpoint named `stage`, that allows to specify from which stage a pipeline execution
  should continue.

### Changed

- Refactor `pipeline` stages unit tests using new fixtures defined in `conftest.py`,
which allowed to remove lines for mock definitions in almost every test case
- Update documentation to be more descriptive for a first-time user. Root README now
contains general information of the platform, with proper links to more detailed
documentation of modules (including containers and infrastructure)
- Increase number of minimum instances of the `pipeline-service` Cloud Run resource to
avoid containers shutting down after 15 minutes of being idle
- Refactor stage classes now inheriting from the new intermediate `BaseStage` class,
  which performs all the heavy lifting of configurations as well as abstracting calls
  to the LSST Science Pipelines connector
- Gitlab CI/CD jobs for linting (`lint_flake8`) and formatting (`lint_black`) Python
  code were replaced by the new `check_ruff` and `format_ruff` CI/CD jobs respectively

### Fixed

- Fix packages repository issues in `pipeline`'s Dockerfile, after CentOS 7 reached EOL
- Pin `poetry` version to `1.8.5` inside `pipeline`'s Dockerfile, after the install of
dependencies started to fail with newer versions
- Pipeline no longer logging duplicated lines to its status log file

## [0.1.0] - 2024-11-25

### Added

- Gitlab CI/CD jobs for building, testing and deploying both the `tutorial` and
  `pipeline` containers. These jobs are triggered when new Merge Requests are created
  and when new commits or tags are pushed to the `main` branch. Deployment jobs are
  manual and use the `gcloud` SDK client
- Stages classes a pipeline can go through: `Preparation` (for pre-processing tasks
  needed before actually triggering the DECam pipeline), `Ingestion` (for ingesting raw
  images), `Calibration` (for calibrating images), `DRP` (for running the Data Release
  Production pipeline) and `AP` (for running the Alert/Prompt Production pipeline)
- The `pipeline` container folder which has the main codebase for the DECam Pipeline
  Platform. It includes a FastAPI application for triggering pipeline executions, as
  well as adapters for the LSST Science Pipelines package (`v23.0.1`), PostgreSQL
  databases, Kafka clusters, Cloud Storage buckets and Astro Data Archive API. It also
  includes a package with the logic for executing pipelines in different stages
- The `tutorial` container folder which includes a Flask application that
  analyzes Cloud Storage images in response to Pub/Sub events. This application serves
  as a proof of concept for processing incoming images automatically
- Terraform `pipeline` module used in the `dev` directory. It contains the necessary
  GCP resources to run the DECam Pipeline Platform as a Google Cloud Run service, using
  a PostgreSQL database on Google Cloud SQL and an NFS file server on Google Filestore
- Terraform `image_processing` module used in the `dev` directory. It contains the
  necessary GCP resources to process images that are uploaded to a Google Cloud Storage
  bucket
- Terraform `oidc` module used in the `dev` directory, for the integration between
  Google Cloud and Gitlab CI, which allows to change infrastructure from Gitlab
- Terraform `dev` working directory to provision infrastructure on GCP

[unreleased]: https://gitlab.com/nsf-noirlab/csdc/decam-rubin-pipeline/decam-gcp-dev/-/compare/v0.1.0...main
[0.1.0]: https://gitlab.com/nsf-noirlab/csdc/decam-rubin-pipeline/decam-gcp-dev/-/tags/v0.1.0
